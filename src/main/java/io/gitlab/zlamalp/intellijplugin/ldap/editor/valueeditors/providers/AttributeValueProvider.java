package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers;

import com.intellij.ui.ColoredTableCellRenderer;
import com.intellij.ui.SimpleTextAttributes;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.LdapPasswordValueEditorDialog;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.StringValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.jetbrains.annotations.NotNull;

import java.awt.*;

/**
 * Shared interface for all implementations of Attribute Value Providers.
 *
 * These classes are used to provide "display" value of Attribute,
 * get/set methods for raw and string representation of a value.
 *
 * There are also conversion methods between byte[] and String.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public interface AttributeValueProvider {

	@NotNull
	LdapEntryTreeNode getEntry();

	@NotNull
	Attribute getAttribute();

	/**
	 * Render display value of attribute in passed renderer.
	 *
	 * @param renderer renderer to use to append content to
	 * @param style default style (usually plain, bold or italic from table cell renderer logic)
	 * @param raw TRUE if value should be rendered raw / FALSE if to render decorated/modified value
	 */
	void renderDisplayValue(ColoredTableCellRenderer renderer, SimpleTextAttributes style, boolean raw);

	Value getRawValue();

	void setRawValue(Value rawValue);

	String getStringValue();

	Value constructNewValue(String stringValue) throws LdapInvalidAttributeValueException;

	Value constructNewValue(byte[] byteValue) throws LdapInvalidAttributeValueException;

	String convertToString(byte[] bytes);

	byte[] convertToBytes(String string);

	/**
	 * Return new instance of DialogWrapper. Returning class is actually extending it and also implements LdapValueEditor.
	 * Dialog window is used to edit attribute value.
	 *
	 * @see AttributeValueEditor
	 * @see StringValueEditor
	 * @see LdapPasswordValueEditorDialog
	 *
	 * @param component Root component this dialog wrapper should be associated with
	 * @param addingNewValue TRUE if we want to use editor to add new value for same attribute | FALSE edit current value)
	 *
	 * @return Instance of DialogWrapper used to edit Attribute value.
	 */
	AttributeValueEditor getValueEditor(Component component, boolean addingNewValue);

}
