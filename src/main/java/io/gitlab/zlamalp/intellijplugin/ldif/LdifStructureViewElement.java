package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.ide.structureView.StructureViewTreeElement;
import com.intellij.ide.util.treeView.smartTree.*;
import com.intellij.navigation.*;
import com.intellij.psi.*;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.*;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.*;

/**
 * Implementation of Structure view elements of LDIF file.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifStructureViewElement implements StructureViewTreeElement, SortableTreeElement {

	private PsiElement element;

	public LdifStructureViewElement(PsiElement element) {
		this.element = element;
	}

	@Override
	public Object getValue() {
		return element;
	}

	@Override
	public void navigate(boolean requestFocus) {
		if (element instanceof NavigationItem) {
			((NavigationItem) element).navigate(requestFocus);
		}
	}

	@Override
	public boolean canNavigate() {
		return element instanceof NavigationItem &&
				((NavigationItem) element).canNavigate();
	}

	@Override
	public boolean canNavigateToSource() {
		return element instanceof NavigationItem &&
				((NavigationItem) element).canNavigateToSource();
	}

	@Override
	@NotNull
	public String getAlphaSortKey() {
		if (element instanceof PsiNamedElement) {
			return StringUtils.trimToEmpty(((PsiNamedElement) element).getName());
		}
		return element.getText();
	}

	@Override
	@NotNull
	public ItemPresentation getPresentation() {
		ItemPresentation presentation = null;
		if (element instanceof NavigationItem) {
			presentation = ((NavigationItem) element).getPresentation();
		}
		if (presentation == null) {
			presentation = new ItemPresentation() {

				@Override
				public String getPresentableText() {
					return "Unknown element: "+element.getText();
				}

				@Nullable
				@Override
				public String getLocationString() {
					return null;
				}

				@Nullable
				@Override
				public Icon getIcon(boolean unused) {
					return null;
				}
			};
		}
		return presentation;
	}

	@Override
	@NotNull
	public TreeElement[] getChildren() {

		if (element instanceof LdifFile) {
			List<TreeElement> treeElements = new ArrayList<>();
			List<LdifEntry> items = LdifUtil.findEntriesInFile((LdifFile)element);
			for (LdifEntry item : items) {
				treeElements.add(new LdifStructureViewElement(item.getDn()));
			}
			return treeElements.toArray(new TreeElement[0]);
		} else {
			return EMPTY_ARRAY;
		}

	}

}
