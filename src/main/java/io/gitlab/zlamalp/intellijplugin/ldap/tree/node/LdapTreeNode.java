package io.gitlab.zlamalp.intellijplugin.ldap.tree.node;

import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.tree.MutableTreeNode;

/**
 * Interface marking all possible TreeNodes for our LDAP plugin.
 * Each of them must implement necessary methods of this interface.
 *
 * @author Attila Majoros
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public interface LdapTreeNode extends MutableTreeNode {

	/**
	 * Return presentation of single TreeItem
	 *
	 * @return String
	 */
	@NotNull String getTreeNodePresentation();

	/**
	 * Get Icon related to TreeNode type
	 *
	 * @return Icon or NULL
	 */
	@Nullable
	Icon getIcon();

	/**
	 * Return connection associated with displayed tree node
	 *
	 * @return
	 */
	LdapServer getLdapServer();

	/**
	 * Return VirtualFile associated with the LDAP Entry or Server or LDAP ROOT NODE
	 *
	 * @return VirtualFile associated with tree node
	 */
	@Nullable
	LdapNodeVirtualFile getVirtualFile();

}
