package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers;

import com.intellij.ui.ColoredTableCellRenderer;
import com.intellij.ui.SimpleTextAttributes;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.LdapPasswordValueEditorDialog;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.constants.LdapSecurityConstants;
import org.apache.directory.api.ldap.model.constants.SchemaConstants;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.apache.directory.api.ldap.model.password.PasswordDetails;
import org.apache.directory.api.ldap.model.password.PasswordUtil;
import org.apache.directory.api.util.Strings;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

import java.awt.*;

/**
 * Basic Binary value editor for LDAP Entry attribute values.
 * Only binary attributes can be used with this provider.
 *
 * Since we can't edit binary value directly, it's encoded to base64 string and expect same on input to save value.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class PasswordValueProvider extends AbstractValueProvider {

	private static Logger log = LoggerFactory.getLogger(PasswordValueProvider.class);

	private LdapSecurityConstants algorithm = null;
	private byte[] salt = null;

	public PasswordValueProvider(@NotNull LdapEntryTreeNode node, @NotNull Attribute attribute, Value value) {
		super(node, attribute, value);
		if (!SchemaConstants.USER_PASSWORD_AT.equalsIgnoreCase(attribute.getUpId())) {
			throw new IllegalArgumentException("You can't use PasswordValueProvider to edit value of "+attribute.getUpId()+" attribute.");
		}
		if (value != null && value.isHumanReadable()) {
			throw new IllegalArgumentException(attribute.getUpId() + " is HumanReadable. You can't use PasswordValueEditor to edit its value.");
		}

	}

	public PasswordValueProvider(@NotNull LdapEntryTreeNode node, @NotNull Attribute attribute, Value value, LdapSecurityConstants algorithm) {
		super(node, attribute, value);
		if (!SchemaConstants.USER_PASSWORD_AT.equalsIgnoreCase(attribute.getUpId())) {
			throw new IllegalArgumentException("You can't use PasswordValueProvider to edit value of "+attribute.getUpId()+" attribute.");
		}
		if (value != null && value.isHumanReadable()) {
			throw new IllegalArgumentException(attribute.getUpId() + " is HumanReadable. You can't use PasswordValueEditor to edit its value.");
		}
		this.algorithm = algorithm;

	}

	@Override
	public void renderDisplayValue(ColoredTableCellRenderer renderer, SimpleTextAttributes style, boolean raw) {
		if (raw) {
			renderer.append(getStringValue(), style, true);
		} else {
			SimpleTextAttributes localStyle = style.derive(SimpleTextAttributes.REGULAR_ITALIC_ATTRIBUTES.getStyle() | style.getStyle(), null, null, null);
			if (getAlgorithm() == null) {
				if (getStringValue() != null && getStringValue().startsWith("{SASL}")) {
					renderer.append("SASL hashed password", localStyle, true);
				} else if (getStringValue() != null && getStringValue().startsWith("{ARGON2}")) {
					renderer.append("ARGON2 hashed password", localStyle, true);
				} else {
					renderer.append("Plain text password", localStyle, true);
				}
			} else {
				renderer.append(getAlgorithm().getName() + " hashed password", localStyle, true);
			}
		}
	}

	@Override
	public String getStringValue() {

		if (getRawValue() == null) return null;
		if (getRawValue().isNull()) return null;

		return convertToString(getRawValue().getBytes());

	}

	@Override
	public void setRawValue(Value rawValue) {
		super.setRawValue(rawValue);
		// update salt and algorithm of new value
		if (getRawValue() != null && !getRawValue().isNull()) {
			PasswordDetails passwordDetails = PasswordUtil.splitCredentials(getRawValue().getBytes());
			this.algorithm = passwordDetails.getAlgorithm();
			this.salt = passwordDetails.getSalt();
		}
	}

	@Override
	public Value constructNewValue(String stringValue) throws LdapInvalidAttributeValueException {
		return constructNewValue(convertToBytes(stringValue));
	}

	@Override
	public String convertToString(byte[] bytes) {
		return Strings.utf8ToString(bytes);
	}

	@Override
	public byte[] convertToBytes(String string) {
		return PasswordUtil.createStoragePassword(string, this.algorithm);
	}

	@Override
	public AttributeValueEditor getValueEditor(Component component, boolean addingNewValue) {
		return new LdapPasswordValueEditorDialog(component, getEntry(), this, addingNewValue);
	}

	public LdapSecurityConstants getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(LdapSecurityConstants algorithm) {
		this.algorithm = algorithm;
	}

	public byte[] getSalt() {
		return salt;
	}

	public void setSalt(byte[] salt) {
		this.salt = salt;
	}

	public boolean isSASLvalue() {
		if (getStringValue() != null) return getStringValue().startsWith("{SASL}");
		return false;
	}

	public boolean isArgon2value() {
		if (getStringValue() != null) return getStringValue().startsWith("{ARGON2}");
		return false;
	}

	/**
	 * Returns "argon2_hashed_password" value constructed from the string plaintext password
	 *
	 * @param password Plain text password from user input
	 * @return Password hashed with ARGON2
	 */
	public String stringToArgon2(String password) {
		// FIXME - Where and how should be memory and iterations configurable
		Argon2PasswordEncoder arg2SpringSecurity = new Argon2PasswordEncoder(16, 32, 1, 4096, 3);
		return arg2SpringSecurity.encode(password);
	}

}
