package io.gitlab.zlamalp.intellijplugin.ldap.settings;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.plugin.LdapPluginSettings;
import org.jetbrains.annotations.NotNull;

/**
 * LdapPluginSettingsService service is used to store IDE wide settings for LDAP plugin
 * as well as IDE wide LDAP server connections.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
@State(name = "LdapPluginSettings", storages = {@Storage("LdapPluginSettings.xml")})
public class LdapPluginSettingsService implements PersistentStateComponent<LdapPluginSettings> {

	/**
	 * Plugin settings
	 */
	private LdapPluginSettings settings;

	/**
	 * Return instance of LdapPluginSettingsService IDE Component
	 *
	 * @return LdapPluginSettingsService
	 */
	public static LdapPluginSettingsService getInstance() {
		return ApplicationManager.getApplication().getService(LdapPluginSettingsService.class);
	}

	@Override
	public void initializeComponent() {
		if (settings == null) {
			settings = new LdapPluginSettings();
		}
	}

	@NotNull
	@Override
	public LdapPluginSettings getState() {
		return settings;
	}

	@Override
	public void loadState(@NotNull LdapPluginSettings state) {
		this.settings = state;
	}

}
