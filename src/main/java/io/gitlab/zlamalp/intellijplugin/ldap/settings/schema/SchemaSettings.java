package io.gitlab.zlamalp.intellijplugin.ldap.settings.schema;

import java.io.Serializable;

/**
 * Data holder for LDAP plugin settings related to the LDAP Schema.
 *
 * @author Pavel Zlámal
 */
public class SchemaSettings implements Serializable {

	/**
	 * Determine, what component is used by LDAP plugin to handle/provide schema information.
	 */
	public enum SchemaProvider {

		/**
		 * LDAP schema is not used at all within the plugin (no schema validation,
		 * no syntax validation, unknown must/may attributes and object classes).
		 */
		NONE("None"),

		/**
		 * ApacheDirectory API - SchemaManager is used for schema handling.
		 * Schema is loaded from locally known templates or LDAP itself (configurable).
		 * Schema loaded from LDAP must be in RFC valid format. It is sadly no case for OpenLDAP or MS AD.
		 *
		 * Assistance with syntax, comparison, MUST/MAY attributes and objectClasses is provided.
		 *
		 * Properties from superior entries are not part of subordinate entries -> MUST attributes of superior
		 * are not part of subordinate attribute type. In order to correctly resolve such properties, methods
		 * from our SchemaUtils must be used.
		 */
		APACHE_DIRECTORY_API("ApacheDirectory API"),

		/**
		 * Our implementation is used for schema handling. Schema is loaded primarily from LDAP itself.
		 * We use ApacheDirectory API SchemaManager as a fallback and patch it with missing elements
		 * for OpenLDAP and MS ActiveDirectory.
		 *
		 * Assistance with syntax, comparison, MUST/MAY attributes and objectClasses is provided.
		 *
		 * Properties from superior entries are filled to subordinate entries (eg. attributeTypes and objectClasses.
		 * Using SchemaUtils methods to get proper schema information might not be necessary, but still preferred.
		 */
		PLUGIN("Plugin built-in");

		private String name;

		SchemaProvider(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}

	}

	// default values
	SchemaProvider schemaProvider = SchemaProvider.PLUGIN;
	boolean strictSchemaValidation = false;

	public SchemaProvider getSchemaProvider() {
		return schemaProvider;
	}

	public void setSchemaProvider(SchemaProvider schemaProvider) {
		this.schemaProvider = schemaProvider;
	}

	public boolean isStrictSchemaValidation() {
		return strictSchemaValidation;
	}

	public void setStrictSchemaValidation(boolean strictSchemaValidation) {
		this.strictSchemaValidation = strictSchemaValidation;
	}

}
