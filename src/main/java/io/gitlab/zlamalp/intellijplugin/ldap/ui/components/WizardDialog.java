package io.gitlab.zlamalp.intellijplugin.ldap.ui.components;

import com.intellij.LdapBundle;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.openapi.ui.ex.MultiLineLabel;
import com.intellij.ui.JBCardLayout;
import com.intellij.ui.JBColor;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBPanelWithEmptyText;
import com.intellij.util.ui.JBDimension;
import com.intellij.util.ui.StatusText;
import com.intellij.util.ui.UIUtil;
import com.intellij.util.ui.components.BorderLayoutPanel;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Wizard dialog automatically handles walk-trough through steps of WizardModel.
 * WizardModel should be your "data holder" for the whole DataWizard life-cycle
 * and provider of all wizard steps.
 *
 * @param <T> Class extending WizardModel.
 */
public class WizardDialog<T extends WizardModel> extends DialogWrapper {

	// model and steps
	private final T wizardModel;
	Map<WizardStep, String> stepNames = new HashMap<>();

	// UI
	JBLabel label = new JBLabel("");
	MultiLineLabel comment = new MultiLineLabel("");
	BorderLayoutPanel header = new BorderLayoutPanel();
	JPanel panel = new JBPanelWithEmptyText(new JBCardLayout()) {
		@Override
		public Dimension getPreferredSize() {
			Dimension custom = getWindowPreferredSize();
			Dimension superSize = super.getPreferredSize();
			if (custom != null) {
				custom.width = custom.width > 0 ? custom.width : superSize.width;
				custom.height = custom.height > 0 ? custom.height : superSize.height;
			} else {
				custom = superSize;
			}
			return custom;
		}

		@NotNull
		@Override
		public StatusText getEmptyText() {
			withEmptyText("No steps present in wizard");
			return super.getEmptyText();
		}
	};

	// dialog actions
	Action prevAction = new DialogWrapperAction(LdapBundle.message("ldap.addEntry.buttons.prev.text")){
		@Override
		protected void doAction(ActionEvent e) {
			WizardStep previousStep = wizardModel.previous();
			if (previousStep != null) {
				initAndShowCurrentStep(JBCardLayout.SwipeDirection.BACKWARD);
			}
		}
	};
	Action nextAction = new DialogWrapperAction(LdapBundle.message("ldap.addEntry.buttons.next.text")){
		@Override
		protected void doAction(ActionEvent e) {
			WizardStep nextStep = wizardModel.next();
			if (nextStep != null) {
				initAndShowCurrentStep(JBCardLayout.SwipeDirection.FORWARD);
			}
		}
	};
	Action finishAction = new DialogWrapperAction(LdapBundle.message("ldap.addEntry.buttons.finish.text")){
		@Override
		protected void doAction(ActionEvent e) {
			if (wizardModel.finish()) {
				// We can perform action as Ok or within wizard step as onFinish()
				// depending if we want wizard to be closed before/after performing the action (update LDAP).
				doOKAction();
			}
		}
	};
	Action cancelAction = new DialogWrapperAction(LdapBundle.message("ldap.addEntry.buttons.cancel.text")){
		@Override
		protected void doAction(ActionEvent e) {
			wizardModel.cancel();
			doCancelAction(e);
		}
	};

	/**
	 * Creates new WizardDialog for specified project and model.
	 *
	 * @see WizardModel
	 *
	 * @param project associated project
	 * @param canBeParent can be parent dialog
	 * @param wizardModel model to use for building a wizard
	 */
	public WizardDialog(@Nullable Project project, boolean canBeParent, @NotNull T wizardModel) {
		super(project, canBeParent);
		this.wizardModel = wizardModel;
		this.wizardModel.setDialog(this);
		init();
		initValidation();
	}

	/**
	 * Get current wizard model
	 *
	 * @return current model
	 */
	@NotNull
	public T getWizardModel() {
		return wizardModel;
	}

	@Override
	protected void init() {
		setTitle(wizardModel.getTitle());
		super.init();
		initKeyBindings();
		initAndShowCurrentStep(JBCardLayout.SwipeDirection.AUTO);
	}

	/**
	 * Advance dialog to the "current step" of the provided WizardModel.
	 *
	 * @param swipe direction
	 */
	private void initAndShowCurrentStep(JBCardLayout.SwipeDirection swipe) {

		WizardStep currentStep = wizardModel.getCurrentStep();

		// handle header
		header.setVisible(StringUtils.isNotBlank(currentStep.getName()) || StringUtils.isNotBlank(currentStep.getDescription()));
		label.setText(currentStep.getName());
		label.setVisible(StringUtils.isNotBlank(currentStep.getName()));
		comment.setText(currentStep.getDescription());
		comment.setVisible(StringUtils.isNotBlank(currentStep.getDescription()));

		if (label.isVisible() && !comment.isVisible()) {
			label.setBorder(BorderFactory.createEmptyBorder(10, 15, 10, 15));
		} else {
			label.setBorder(BorderFactory.createEmptyBorder(10, 15, 0, 15));
		}

		// handle step content
		String stepName = stepNames.get(currentStep);
		if (stepName == null) {
			stepName = "Step "+stepNames.size();
			stepNames.put(currentStep, stepName);
			panel.add(currentStep.getComponent(), stepName);
		}
		((JBCardLayout)panel.getLayout()).swipe(panel, stepName, swipe);

		// handle wizard buttons
		finishAction.setEnabled(wizardModel.isLast(currentStep) || wizardModel.isGoalAchieved());
		nextAction.setEnabled(!wizardModel.isLast(currentStep));
		prevAction.setEnabled(!wizardModel.isFirst(currentStep));
		getHelpAction().setEnabled(currentStep.getHelpId() != null);

		if (nextAction.isEnabled()) {
			getRootPane().setDefaultButton(getButton(nextAction));
		}
		else if (finishAction.isEnabled()) {
			getRootPane().setDefaultButton(getButton(finishAction));
		}
		else if (cancelAction.isEnabled()) {
			getRootPane().setDefaultButton(getButton(cancelAction));
		}
		else {
			getRootPane().setDefaultButton(null);
		}

		// focus
		JComponent focusComponent = currentStep.getPreferredFocusedComponent();
		if (focusComponent != null) {
			focusComponent.requestFocusInWindow();
		}

		// immediately trigger validation to update button state based on it
		doValidateAll();

	}

	@Nullable
	@Override
	protected JComponent createNorthPanel() {

		header.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, JBColor.border()));
		header.setBackground(UIUtil.SIDE_PANEL_BACKGROUND);

		label.setFont(UIUtil.getLabelFont().deriveFont(Font.BOLD));
		comment.setFont(UIUtil.getLabelFont().deriveFont(Font.ITALIC));

		label.setBorder(BorderFactory.createEmptyBorder(10, 15, 0, 15));
		comment.setBorder(BorderFactory.createEmptyBorder(10, 15, 10, 15));

		header.addToTop(label);
		header.addToCenter(comment);

		return header;

	}

	@Nullable
	@Override
	protected JComponent createCenterPanel() {
		panel.setPreferredSize(new JBDimension(600, 400));
		panel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
		return panel;
	}

	@NotNull
	@Override
	protected DialogStyle getStyle() {
		return DialogStyle.COMPACT;
	}

	@Override
	public JComponent getPreferredFocusedComponent() {
		return wizardModel.getCurrentStep().getPreferredFocusedComponent();
	}

	// fixme naco ?
	public Dimension getWindowPreferredSize() {
		return null;
	}

	@NotNull
	protected Action[] createActions() {

		Action helpAction = getHelpAction();
		return helpAction == myHelpAction && getHelpId() == null ?
				new Action[]{prevAction, nextAction, finishAction, cancelAction} :
				new Action[]{prevAction, nextAction, finishAction, cancelAction, helpAction};

	}

	@Nullable
	@Override
	protected String getHelpId() {
		return wizardModel.getCurrentStep().getHelpId();
	}

	/**
	 * Initialize key-bindings for help action
	 */
	private void initKeyBindings() {

		getRootPane().registerKeyboardAction(
				new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						doHelpAction();
					}
				},
				KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0),
				JComponent.WHEN_IN_FOCUSED_WINDOW
		);

		getRootPane().registerKeyboardAction(
				new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						doHelpAction();
					}
				},
				KeyStroke.getKeyStroke(KeyEvent.VK_HELP, 0),
				JComponent.WHEN_IN_FOCUSED_WINDOW
		);

	}

	@Nullable
	@Override
	protected String getDimensionServiceKey() {
		return "io.github.zlamalp.intellijplugin.ldap.ui.components."+wizardModel.getTitle();
	}

	@NotNull
	@Override
	protected List<ValidationInfo> doValidateAll() {

		// Delegate validation
		List<ValidationInfo> list = wizardModel.getCurrentStep().doValidateAll();

		// Update Next/Finish buttons based on validation results
		for (ValidationInfo info : list) {
			if (!info.okEnabled) {
				if (nextAction.isEnabled()) {
					getButton(nextAction).setEnabled(false);
				}
				if (finishAction.isEnabled()) {
					getButton(finishAction).setEnabled(false);
				}
				break;
			}
		}
		if (list.isEmpty()) {
			if (nextAction.isEnabled()) {
				getButton(nextAction).setEnabled(true);
			}
			if (finishAction.isEnabled()) {
				getButton(finishAction).setEnabled(true);
			}
		}

		return list;

	}

}
