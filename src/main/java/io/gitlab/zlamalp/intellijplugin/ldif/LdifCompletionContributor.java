package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.codeInsight.completion.*;


/**
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifCompletionContributor extends CompletionContributor {

	public LdifCompletionContributor() {

		/*
		extend(CompletionType.BASIC,
				PlatformPatterns.psiElement(LdifTypes.KEY_DN).withLanguage(LdifLanguage.INSTANCE),
				new CompletionProvider<CompletionParameters>() {
					public void addCompletions(@NotNull CompletionParameters parameters,
					                           ProcessingContext context,
					                           @NotNull CompletionResultSet resultSet) {
						resultSet.addElement(LookupElementBuilder.create("dn: "));
						resultSet.addElement(LookupElementBuilder.create("dn:: "));
					}
				}
		);
		extend(CompletionType.BASIC,
				PlatformPatterns.psiElement(LdifTypes.ITEM_).withLanguage(LdifLanguage.INSTANCE),
				new CompletionProvider<CompletionParameters>() {
					public void addCompletions(@NotNull CompletionParameters parameters,
					                           ProcessingContext context,
					                           @NotNull CompletionResultSet resultSet) {
						resultSet.addElement(LookupElementBuilder.create("dn: "));
						resultSet.addElement(LookupElementBuilder.create("dn:: "));
					}
				}
		);

		extend(CompletionType.BASIC,
				PlatformPatterns.psiElement(LdifTypes.VERSION).withLanguage(LdifLanguage.INSTANCE),
				new CompletionProvider<CompletionParameters>() {
					public void addCompletions(@NotNull CompletionParameters parameters,
					                           ProcessingContext context,
					                           @NotNull CompletionResultSet resultSet) {
						resultSet.addElement(LookupElementBuilder.create("version: 1"));
					}
				}
		);

		extend(CompletionType.BASIC,
					PlatformPatterns.psiElement(LdifTypes.PROPERTY).withLanguage(LdifLanguage.INSTANCE),
				new CompletionProvider<CompletionParameters>() {
					public void addCompletions(@NotNull CompletionParameters parameters,
					                           ProcessingContext context,
					                           @NotNull CompletionResultSet resultSet) {
						resultSet.addElement(LookupElementBuilder.create("cn: "));
						resultSet.addElement(LookupElementBuilder.create("sn: "));
						resultSet.addElement(LookupElementBuilder.create("objectClass: "));
					}
				}
		);

		extend(CompletionType.BASIC,
				PlatformPatterns.psiElement(LdifTypes.SEPARATOR).withLanguage(LdifLanguage.INSTANCE),
				new CompletionProvider<CompletionParameters>() {
					public void addCompletions(@NotNull CompletionParameters parameters,
					                           ProcessingContext context,
					                           @NotNull CompletionResultSet resultSet) {
						resultSet.addElement(LookupElementBuilder.create(":"));
						resultSet.addElement(LookupElementBuilder.create("::"));
						resultSet.addElement(LookupElementBuilder.create(":<"));

					}
				}
		);
		*/

	}

}
