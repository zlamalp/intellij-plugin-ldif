package io.gitlab.zlamalp.intellijplugin.ldap;

import com.intellij.credentialStore.CredentialAttributes;
import com.intellij.credentialStore.Credentials;
import com.intellij.ide.passwordSafe.PasswordSafe;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.progress.PerformInBackgroundOption;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.util.messages.Topic;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.AttributeValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.BinaryValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.DnValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.GeneralizedTimeValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.IA5StringValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.IntegerValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.JpegValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.NumberValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.ObjectClassValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.ObjectGuidValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.ObjectSidValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.OctetStringValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.OidValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.PasswordValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.StringValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeType;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.LdapSchema;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapPluginSettingsService;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapProjectSettingsService;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapServersStore;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.plugin.LdapPluginSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.schema.SchemaSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNodeWrapper;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.LdapNotificationHandler;
import org.apache.commons.lang.StringUtils;
import org.apache.directory.api.ldap.codec.api.DefaultConfigurableBinaryAttributeDetector;
import org.apache.directory.api.ldap.codec.controls.search.pagedSearch.PagedResultsFactory;
import org.apache.directory.api.ldap.codec.standalone.StandaloneLdapApiService;
import org.apache.directory.api.ldap.model.constants.LdapConstants;
import org.apache.directory.api.ldap.model.constants.SchemaConstants;
import org.apache.directory.api.ldap.model.cursor.CursorException;
import org.apache.directory.api.ldap.model.cursor.EntryCursor;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.DefaultAttribute;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.apache.directory.api.ldap.model.exception.LdapInvalidDnException;
import org.apache.directory.api.ldap.model.ldif.LdifUtils;
import org.apache.directory.api.ldap.model.message.AliasDerefMode;
import org.apache.directory.api.ldap.model.message.ResultCodeEnum;
import org.apache.directory.api.ldap.model.message.SearchRequest;
import org.apache.directory.api.ldap.model.message.SearchRequestImpl;
import org.apache.directory.api.ldap.model.message.SearchResultDone;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.apache.directory.api.ldap.model.message.controls.PagedResults;
import org.apache.directory.api.ldap.model.name.Dn;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.apache.directory.api.util.Base64;
import org.apache.directory.api.util.Strings;
import org.apache.directory.ldap.client.api.EntryCursorImpl;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventListener;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Utility methods for this plugin processing LDAP entries and schema
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapUtils {

	private static Logger log = LoggerFactory.getLogger(LdapUtils.class);

	public static DefaultConfigurableBinaryAttributeDetector binaryAttributeDetector = new DefaultConfigurableBinaryAttributeDetector();

	public enum RefreshType {
		ENTRY,
		WITH_CHILDREN,
		ONLY_CHILDREN
	}

	// FIXME - this will be configurable
	private static Set<String> objectClassesForNodesWithoutChildren = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
	// fallback OID attrs
	private static Set<String> oidAttrs = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

	static {
		objectClassesForNodesWithoutChildren.addAll(Arrays.asList("user", "person","inetorgperson","organizationalrole","attributeschema"));
		oidAttrs.addAll(Arrays.asList(SchemaConstants.SUPPORTED_CONTROL_AT, SchemaConstants.SUPPORTED_EXTENSION_AT, SchemaConstants.SUPPORTED_FEATURES_AT, "supportedcapabilities"));
	}

	/**
	 * Get available BASEs of LDAP (namingContexts) by querying ROOT_DSE node.
	 *
	 * @param ldap LDAP server
	 * @return List of BASE DNs or empty list
	 */
	public static List<String> queryRootDSEForBase(@NotNull LdapServer ldap, LdapConnection sourceConnection) throws LdapException {

		log.debug("Query RootDSE of {} for namingContexts.", ldap.getUrl());

		LdapConnection connection = null;

		String[] requestedAttributes = { SchemaConstants.NAMING_CONTEXTS_AT };
		List<String> bases = new ArrayList<>();
		try {

			connection = (sourceConnection == null) ? ldap.getConnection() : sourceConnection;

			Entry rootDse = queryRootDSE(ldap, connection, requestedAttributes);
			if (rootDse != null) {
				Attribute attribute = rootDse.get(SchemaConstants.NAMING_CONTEXTS_AT);
				for (Value val : attribute) {
					bases.add(val.getString());
				}
			}

			return bases;

		} finally {
			// close locally created connection
			if (sourceConnection == null && connection != null) {
				ldap.releaseConnection(connection);
			}
		}

	}

	/**
	 * Query LDAP for Root DSE entry with specified set of attributes.
	 * If requestedAttributes are null or empty, all user and operational attributes are retrieved ("*","+").
	 *
	 * In case of connection problems we might not be able to fetch entry from LDAP, in such case NULL is returned.
	 *
	 * @param ldap LDAP to get attributes from
	 * @param requestedAttributes Set of requested attributes
	 * @return ROOT_DSE Entry or null
	 */
	@Nullable
	private static Entry queryRootDSE(@NotNull LdapServer ldap, LdapConnection sourceConnection, @Nullable String[] requestedAttributes) throws LdapException {

		if (requestedAttributes == null || requestedAttributes.length == 0) {
			// if not defined, get all user and operational attributes
			requestedAttributes = SchemaConstants.ALL_ATTRIBUTES_ARRAY;
		}
		log.debug("Query RootDSE of {} for {}.", ldap.getUrl(), requestedAttributes);

		LdapConnection connection = null;
		try {

			connection = (sourceConnection == null) ? ldap.getConnection() : sourceConnection;
			return connection.getRootDse(requestedAttributes);

		} finally {
			// close locally created connection
			if (sourceConnection == null && connection != null) {
				ldap.releaseConnection(connection);
			}
		}

	}

	/**
	 * Fetch Entry from LDAP for specified LdapTreeNode. Might return NULL if Entry doesn't exists in LDAP anymore
	 * or when we are unable to fetch it for other reasons.
	 *
	 * Different lookup behavior is provided for each LdapNodeTypes (ROOT_DSE, BASE, NODE).
	 *
	 * @see LdapEntryTreeNode
	 * @see Entry
	 * @see Attribute
	 *
	 * @param ldapNode LdapNode (Entry) to get attributes for
	 * @return Entry object with attributes or NULL in case of Exception
	 */
	@Nullable
	public static Entry getEntry(@NotNull LdapEntryTreeNode ldapNode, LdapConnection sourceConnection) throws LdapException {

		// Get requested attributes to get with entry
		Set<String> requestedAttributes = LdapUtils.getRequestedAttributes(ldapNode);
		return getEntry(ldapNode, sourceConnection, requestedAttributes);

	}

	/**
	 * Fetch Entry from LDAP for specified LdapTreeNode. Might return NULL if Entry doesn't exists in LDAP anymore
	 * or when we are unable to fetch it for other reasons.
	 *
	 * Different lookup behavior is provided for each LdapNodeTypes (ROOT_DSE, BASE, NODE).
	 *
	 * @see LdapEntryTreeNode
	 * @see Entry
	 * @see Attribute
	 *
	 * @param ldapNode LdapNode (Entry) to get attributes for
	 * @param sourceConnection Connection to use to get Entry
	 * @param requestedAttributes set of requested attributes
	 * @return Entry object with attributes or NULL in case of Exception
	 */
	@Nullable
	public static Entry getEntry(@NotNull LdapEntryTreeNode ldapNode, LdapConnection sourceConnection, Set<String> requestedAttributes) throws LdapException {

		LdapNodeType nodeType = ldapNode.getNodeType();
		String dn = ldapNode.getDn().toString();
		LdapServer ldap = ldapNode.getLdapServer();
		Entry entry = null;

		// we always retrieve objectClass and hasSubordinates
		requestedAttributes.add(SchemaConstants.OBJECT_CLASS_AT);
		requestedAttributes.add(SchemaConstants.HAS_SUBORDINATES_AT);
		String[] attrs = requestedAttributes.toArray(new String[0]);

		log.info("Refreshing: " + dn + " with attrs: {}", requestedAttributes);

		LdapConnection connection = null;

		try {

			connection = (sourceConnection == null) ? ldap.getConnection() : sourceConnection;

			if (Objects.equals(nodeType, LdapNodeType.ROOT_DSE)) {

				entry = io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils.queryRootDSE(ldap, connection, null);

			} else if (Objects.equals(nodeType, LdapNodeType.BASE)) {

				entry = connection.lookup(dn, attrs);
				// TODO - perform incremental lookup for ranged attributes

			} else if (Objects.equals(nodeType, LdapNodeType.NODE)) {

				entry = connection.lookup(dn, attrs);
				// TODO - perform incremental lookup for ranged attributes
			}

			if (entry != null) {
				// FIXME - build known nodes
				ldapNode.getLdapServer().getKnownNodes().add(entry.getDn().toString());
			}

			return entry;

		} finally {
			// close locally created connection
			if (sourceConnection == null && connection != null) {
				ldap.releaseConnection(connection);
			}
		}

	}

	/**
	 * Search in LDAP for child entries of specified LdapTreeNode.
	 *
	 * Only "objectClass" and "hasSubordinates" attributes are retrieved with each child Entry.
	 * This is a speed and preferred way of getting new entries to be displayed in the LDAP tree (LdapTreePanel).
	 *
	 * Different search behavior is provided for all parent LdapTreeNode types (ROOT_DSE, BASE, NODE).
	 *
	 * Before showing or editing LdapTreeNode in any Editor, all Entry Attributes must be retrieved explicitly
	 * using LdapUtils.refresh(LdapTeeNode).
	 *
	 * @see #refresh(LdapEntryTreeNode)
	 * @see LdapEntryTreeNode
	 * @see Entry
	 * @see Attribute
	 *
	 * @param parent LdapTreeNode (LDAP Entry) to get children for
	 * @return List of child LdapTreeNodes (Entries) or empty list if no children found or Exception happened
	 * @throws LdapException when search fails
	 */
	public static List<LdapEntryTreeNode> getChildren(@NotNull LdapEntryTreeNode parent, LdapConnection sourceConnection) throws LdapException {

		List<LdapEntryTreeNode> children = new ArrayList<>();

		LdapNodeType nodeType = parent.getNodeType();
		Dn dn = parent.getDn();
		LdapServer ldap = parent.getLdapServer();
		String[] attributesToGet = { SchemaConstants.OBJECT_CLASS_AT, SchemaConstants.HAS_SUBORDINATES_AT };

		String filter = parent.getFilter();
		if (StringUtils.isBlank(filter)) {
			filter = ldap.getFilter();
		}
		// make sure we have valid filter on bad input
		if (StringUtils.isBlank(filter)) {
			filter = LdapConstants.OBJECT_CLASS_STAR;
		}

		LdapConnection connection = null;

		try {

			connection = (sourceConnection == null) ? ldap.getConnection() : sourceConnection;

			if (Objects.equals(nodeType, LdapNodeType.ROOT_DSE)) {

				List<String> bases = new ArrayList<>();
				if (ldap.getBase().isEmpty()) {
					// read all LDAP bases
					bases = io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils.queryRootDSEForBase(ldap, connection);
				} else {
					// single defined base
					bases.add(ldap.getBase());
				}

				// FIXME - build known nodes
				parent.getLdapServer().getKnownNodes().addAll(bases);

				for (String base : bases) {

					List<LdapEntryTreeNode> partialChildren = new ArrayList<>();

					List<Entry> cursor = search(ldap, connection, base, filter, SearchScope.OBJECT, attributesToGet);
					for (Entry entry : cursor) {
						partialChildren.add(new LdapEntryTreeNode(ldap, LdapNodeType.BASE, entry.getDn(), entry));
					}

					if (partialChildren.isEmpty()) {
						// if we are not allowed to read base, fake it for us
						// FIXME - should we also fake "Entry" with Base DN ? - currently NULL
						try {
							Dn localDn = new Dn(base);
							partialChildren = Collections.singletonList(new LdapEntryTreeNode(ldap, LdapNodeType.BASE, localDn, null));
						} catch (LdapInvalidDnException ex) {
							log.error("We couldn't create BASE node from LdapServerConnection settings and none was provided by LDAP server itself.", ex);
						}
					}

					children.addAll(partialChildren);

				}

			} else if (Objects.equals(nodeType, LdapNodeType.BASE)) {

				List<Entry> cursor = search(ldap, connection, dn.toString(), filter, SearchScope.ONELEVEL, attributesToGet);
				for (Entry entry : cursor) {
					// FIXME - build known nodes
					parent.getLdapServer().getKnownNodes().add(entry.getDn().toString());
					children.add(new LdapEntryTreeNode(ldap, LdapNodeType.NODE, entry.getDn(), entry));
				}

			} else if (Objects.equals(nodeType, LdapNodeType.NODE)) {

				List<Entry> cursor = search(ldap, connection, dn.toString(), filter, SearchScope.ONELEVEL, attributesToGet);
				for (Entry entry : cursor) {
					// FIXME - build known nodes
					parent.getLdapServer().getKnownNodes().add(entry.getDn().toString());
					children.add(new LdapEntryTreeNode(ldap, LdapNodeType.NODE, entry.getDn(), entry));
				}

			}

			return children;

		} finally {
			// close locally created connection
			if (sourceConnection == null && connection != null) {
				ldap.releaseConnection(connection);
			}
		}

	}

	/**
	 * Query LDAP/AD with specified parameters like baseDn, filter, scope etc.
	 * Most of actual search parameters are taken from IDE or LdapServerConnection settings.
	 *
	 * This can use already existing connection passed in "sourceConection" param. If not provided, then
	 * active connection is taken from "ldap" (LDAP server config) param and returned once search is done.
	 *
	 * @param ldap LDAP/AD server instance (connection config)
	 * @param sourceConnection Prepared LDAP connection or NULL (will be taken from LDAP server instance)
	 * @param baseDn Base DN for search
	 * @param filter Filter applied to the query
	 * @param scope SearchScope limiting what is searched/returned
	 * @param attributes Attribute IDs to retrieve with each Entry
	 * @return List of found Entries
	 * @throws LdapException When search fails
	 */
	public static List<Entry> search(@NotNull LdapServer ldap,
	                                 LdapConnection sourceConnection,
	                                 @NotNull String baseDn,
	                                 @NotNull String filter,
	                                 @NotNull SearchScope scope,
	                                 @NotNull String... attributes ) throws LdapException {

		// load configuration
		LdapSettings ldapSettings = LdapUtils.getPreferredLdapSettings(ldap.getSettings());
		int pageSize = ldapSettings.getPageSize();
		int requestTimeout = ldapSettings.getRequestTimeout();
		int responseLimit = ldapSettings.getRequestLimit();
		AliasDerefMode aliasDerefMode = ldapSettings.getAliasDerefMode();

		LdapConnection connection = null;
		try {

			connection = (sourceConnection == null) ? ldap.getConnection() : sourceConnection;

			//Without this you get a class Cast Exception:
			//java.lang.ClassCastException: org.apache.directory.api.ldap.codec.BasicControlDecorator cannot be cast to org.apache.directory.api.ldap.model.message.controls.PagedResults
			System.setProperty(StandaloneLdapApiService.REQUEST_CONTROLS_LIST, PagedResultsFactory.class.getName());

			PagedResults pagedSearchControl = new PagedResultsFactory(connection.getCodecService()).newControl();
			pagedSearchControl.setSize(pageSize);

			// Loop over all the elements
			List<Entry> results = new ArrayList<>();
			boolean hasUnwillingToPerform = false;

			while (true) {
				EntryCursor cursor = null;

				try {
					SearchRequest searchRequest = new SearchRequestImpl();
					searchRequest.setBase(new Dn(baseDn));
					searchRequest.setFilter(filter);
					searchRequest.setScope(scope);
					searchRequest.addAttributes(attributes);
					if (pageSize > 0) searchRequest.addControl(pagedSearchControl);
					searchRequest.setDerefAliases(aliasDerefMode);
					searchRequest.setTimeLimit(requestTimeout);
					if (responseLimit > 0) searchRequest.setSizeLimit(responseLimit);

					cursor = new EntryCursorImpl(connection.search(searchRequest));

					while (cursor.next()) {
						try {
							if (cursor.available()) {
								Entry result = cursor.get();
								results.add(result);
							}
						} catch (org.apache.directory.api.ldap.model.cursor.CursorLdapReferralException ex) {
							// we follow referrals, so we ignore exception
							log.warn("Entry is referral.", ex);
						}
					}

					SearchResultDone result = cursor.getSearchResultDone();
					pagedSearchControl = (PagedResults) result.getControl(PagedResults.OID);

					if (result.getLdapResult().getResultCode() == ResultCodeEnum.UNWILLING_TO_PERFORM) {
						hasUnwillingToPerform = true;
						break;
					}
				} catch (CursorException ex) {
					log.error("Cursor exception when resolving LDAP query result.", ex);
				} finally {
					if (cursor != null) {
						try {
							cursor.close();
						} catch (IOException e) {
							log.error("IO exception when querying LDAP.", e);
						}
					}
				}

				// if we read node itself without rights we might not get paged control from result.
				if (pagedSearchControl == null) {
					break;
				}

				// check if this is over
				byte[] cookie = pagedSearchControl.getCookie();

				if (Strings.isEmpty(cookie)) {
					// If so, exit the loop
					break;
				}

				// Prepare the next iteration
				pagedSearchControl.setSize(pageSize);
			}

			if (hasUnwillingToPerform) {
				throw new IllegalStateException("AD can't handle paging");
			}

			return results;

		} finally {
			// close connection if it was locally retrieved from the pool
			if (sourceConnection == null && connection != null) {
				ldap.releaseConnection(connection);
			}
		}

	}

	/**
	 * Export LDAP Schema to LDIF
	 *
	 * @param ldap LDAP to read schema from
	 * @param connection LdapConnection to use for it
	 * @return Schema exported in LDIF format
	 */
	public static String exportSchemaToLdif(@NotNull LdapServer ldap, @NotNull LdapConnection connection) throws LdapException {

		Entry schemaContent = exportSchema(ldap, connection);
		// since we manually parse schema from input, do not wrap any lines
		return printToLDIFSorted(schemaContent, 1000000)+"\n";

	}

	/**
	 * Export LDAP Schema to Entry instance.
	 *
	 * @param ldap LDAP to read schema from
	 * @return LDAP Schema as Entry
	 */
	public static Entry exportSchema(@NotNull LdapServer ldap, LdapConnection sourceConnection) throws LdapException {

		LdapConnection connection = null;
		try {

			connection = (sourceConnection != null) ? sourceConnection : ldap.getConnection();

			String[] subschemaSubentryName = {SchemaConstants.SUBSCHEMA_SUBENTRY_AT};
			Entry subschemaSubentry = queryRootDSE(ldap, connection, subschemaSubentryName);
			if (subschemaSubentry == null) {
				return null;
			}
			String schemaLocation = null;
			try {
				// single attribute value is expected
				schemaLocation = subschemaSubentry.get(SchemaConstants.SUBSCHEMA_SUBENTRY_AT).getString();
			} catch (LdapInvalidAttributeValueException ex) {
				log.error("Unable to get SUBSCHEMA_SUBENTRY.", ex);
			}

			if (schemaLocation == null) {
				return null;
			} else {
				return connection.lookup(schemaLocation,
						SchemaConstants.ATTRIBUTE_TYPES_AT,
						SchemaConstants.COMPARATORS_AT,
						SchemaConstants.DIT_CONTENT_RULES_AT,
						SchemaConstants.DIT_STRUCTURE_RULES_AT,
						SchemaConstants.LDAP_SYNTAXES_AT,
						SchemaConstants.MATCHING_RULES_AT,
						SchemaConstants.MATCHING_RULE_USE_AT,
						SchemaConstants.NAME_FORMS_AT,
						SchemaConstants.NORMALIZERS_AT,
						SchemaConstants.OBJECT_CLASSES_AT,
						SchemaConstants.SYNTAX_CHECKERS_AT
				);
			}
		} finally {
			// close locally created connection
			if (sourceConnection == null && connection != null) ldap.releaseConnection(connection);
		}

	}

	/**
	 * Returns a string representation of the object in LDIF format.
	 * Object attributes are alphabetically sorted before print.
	 *
	 * Each output line is wrapped on 80th column.
	 *
	 * @see #printToLDIFSorted(Entry, int)
	 *
	 * @return {@link java.lang.String} formatted to RFC2849 LDIF specifications.
	 * @param entry Entry to print
	 */
	public static String printToLDIFSorted(Entry entry) {
		return printToLDIFSorted(entry, 80);
	}

	/**
	 * Returns a string representation of the object in LDIF format.
	 * Object attributes are alphabetically sorted before print
	 *
	 * @return {@link java.lang.String} formatted to RFC2849 LDIF specifications.
	 * @param entry Entry to print
	 * @param wrapOnColumn Number of columns in one row
	 */
	public static String printToLDIFSorted(Entry entry, int wrapOnColumn) {

		if (entry == null) return "\n";
		return convertToLdifSorted(entry, wrapOnColumn, true);

	}

	/**
	 * Convert an Entry as LDIF
	 *
	 * @param entry the Entry to convert
	 * @param length the expected line length
	 * @return the corresponding LDIF code as a String
	 */
	public static String convertToLdifSorted( Entry entry, int length, boolean sorted ) {

		StringBuilder sb = new StringBuilder();

		if (entry.getDn() != null) {
			// First, dump the Dn
			if (LdifUtils.isLDIFSafe(entry.getDn().getName())) {
				sb.append(LdifUtils.stripLineToNChars("dn: " + entry.getDn().getName(), length));
			} else {
				// force encoding using UTF-8 charset, as required in RFC2849 note 7
				sb.append(LdifUtils.stripLineToNChars("dn:: " + new String(Base64.encode(Strings.getBytesUtf8(entry.getDn().getName()))), length));
			}

			sb.append('\n');
		}

		// Then all the attributes
		if (sorted) {

			List<Attribute> sortedList = new ArrayList<>(entry.getAttributes());
			sortedList.sort(new Comparator<Attribute>() {
				@Override
				public int compare(Attribute o1, Attribute o2) {
					return o1.getId().compareTo(o2.getId());
				}
			});

			for (Attribute attribute : sortedList) {
				sb.append( LdifUtils.convertToLdif( attribute, length ) );
			}

		} else {
			for ( Attribute attribute : entry ) {
				sb.append( LdifUtils.convertToLdif( attribute, length ) );
			}
		}

		return sb.toString();
	}

	/**
	 * Check if Attribute has OID syntax for its value by schema.
	 *
	 * @param attribute Attribute to check
	 * @return TRUE if has OID syntax, FALSE otherwise
	 */
	public static boolean hasOidSyntax(@NotNull Attribute attribute, LdapSchema schema) {

		if (schema != null && schema.getAttributeTypes().get(attribute.getId()) != null && schema.getAttributeTypes().get(attribute.getId()).getSyntax() != null) {
			if (Objects.equals(SchemaConstants.OID_SYNTAX, schema.getAttributeTypes().get(attribute.getId()).getSyntax().getOid())) {
				return true;
			}
		}
		if (oidAttrs.contains(attribute.getId())) {
			return true;
		} else {
			if (attribute.getAttributeType() != null && attribute.getAttributeType().getSyntax() != null) {
				// TODO - find out if this might work for strict checks
				return Objects.equals(SchemaConstants.OID_SYNTAX, attribute.getAttributeType().getSyntax().getOid());
			}
		}
		return false;
	}

	/**
	 * Return proper ValueProvider for Attribute by its name. This perform best effort way to construct proper
	 * value provider based on LdapNode (by connection - schema) and provided attribute name.
	 *
	 * Returned ValueProvider has always EMPTY value. If you construct if for editing purpose, always set back raw value
	 *
	 * @param node LdapNode with which will Attribute be associated
	 * @param attributeName Name of attribute to get Value provider for
	 *
	 * @return Value provider for Attribute of LdapNode (Entry) specified by name
	 */
	public static AttributeValueProvider getLdapValueProvider(@NotNull LdapEntryTreeNode node, @NotNull String attributeName) {

		SchemaSettings schemaSettings = LdapUtils.getPreferredLdapSettings(node.getLdapServer().getSettings()).getSchemaSettings();

		// construct attribute object
		Attribute attribute;
		AttributeType type = null;

		if (!SchemaSettings.SchemaProvider.NONE.equals(schemaSettings.getSchemaProvider())) {
			type = SchemaUtils.getAttributeType(node, new DefaultAttribute(attributeName));
		}

		if (node.getAttributeByName(attributeName) != null) {
			// attribute already exists within entry
			attribute = node.getAttributeByName(attributeName);
		} else {
			// attribute is not yet present in the entry
			attribute = new DefaultAttribute(attributeName);

			// on strict schema validation, apply also known attribute type
			if (!SchemaSettings.SchemaProvider.NONE.equals(schemaSettings.getSchemaProvider())) {
				if (type != null) {
					if (schemaSettings.isStrictSchemaValidation()) {
						attribute = new DefaultAttribute(attributeName, type);
					}
				}
			}

		}

		String attrName = attribute.getUpId();
		int optPos = attrName.indexOf( ';' );
		if (optPos != -1) {
			attrName = attrName.substring(0, optPos);
		}

		if (type != null) {
			// schema provider is set and attribute type was found

			if (!type.isHR() || binaryAttributeDetector.isBinary(attrName)) {

				if (SchemaConstants.USER_PASSWORD_AT.equalsIgnoreCase(attrName) ||
						SchemaConstants.USER_PASSWORD_AT_OID.equals(attrName)) {
					return new PasswordValueProvider(node, attribute, null);
				}
				if ("objectguid".equalsIgnoreCase(attrName)) {
					return new ObjectGuidValueProvider(node, attribute, null);
				}
				if ("objectsid".equalsIgnoreCase(attrName)) {
					return new ObjectSidValueProvider(node, attribute, null);
				}
				if (SchemaConstants.OCTET_STRING_SYNTAX.equals(type.getSyntaxOid())) {
					return new OctetStringValueProvider(node, attribute, null);
				}
				if (SchemaConstants.JPEG_SYNTAX.equals(type.getSyntaxOid())) {
					return new JpegValueProvider(node, attribute, null);
				}

				// default to binary value provider
				return new BinaryValueProvider(node, attribute, null);

			} else {

				if (SchemaConstants.OBJECT_CLASS_AT.equalsIgnoreCase(attrName) ||
						SchemaConstants.OBJECT_CLASS_AT_OID.equals(attrName)) {
					return new ObjectClassValueProvider(node, attribute, null);
				}
				if (LdapUtils.hasOidSyntax(attribute, node.getLdapServer().getSchema())) {
					return new OidValueProvider(node, attribute, null);
				}
				if (SchemaConstants.INTEGER_SYNTAX.equals(type.getSyntaxOid())) {
					return new IntegerValueProvider(node, attribute, null);
				}
				if (SchemaConstants.NUMBER_SYNTAX.equals(type.getSyntaxOid())) {
					return new NumberValueProvider(node, attribute, null);
				}
				if (SchemaConstants.DN_SYNTAX.equals(type.getSyntaxOid())) {
					return new DnValueProvider(node, attribute, null);
				}
				if (SchemaConstants.IA5_STRING_SYNTAX.equals(type.getSyntaxOid())) {
					return new IA5StringValueProvider(node, attribute, null);
				}
				if (SchemaConstants.GENERALIZED_TIME_SYNTAX.equals(type.getSyntaxOid())) {
					return new GeneralizedTimeValueProvider(node, attribute, null);
				}

				// default to string value provider
				return new StringValueProvider(node, attribute, null);

			}

		} else {

			// no schema provider or attribute type not found -- best guess fallback on provider/editors
			if (binaryAttributeDetector.isBinary(attrName)) {

				if (SchemaConstants.USER_PASSWORD_AT.equalsIgnoreCase(attrName) ||
						SchemaConstants.USER_PASSWORD_AT_OID.equals(attrName)) {
					return new PasswordValueProvider(node, attribute, null);
				}
				if ("objectguid".equalsIgnoreCase(attrName)) {
					return new ObjectGuidValueProvider(node, attribute, null);
				}
				if ("objectsid".equalsIgnoreCase(attrName)) {
					return new ObjectSidValueProvider(node, attribute, null);
				}
				if ("jpegphoto".equalsIgnoreCase(attrName)) {
					return new JpegValueProvider(node, attribute, null);
				}

				// TODO - OctetString editor

				// default to binary value provider
				return new BinaryValueProvider(node, attribute, null);

			} else {

				if (SchemaConstants.OBJECT_CLASS_AT.equalsIgnoreCase(attrName) ||
						SchemaConstants.OBJECT_CLASS_AT_OID.equals(attrName)) {
					return new ObjectClassValueProvider(node, attribute, null);
				}
				if (LdapUtils.hasOidSyntax(attribute, node.getLdapServer().getSchema())) {
					return new OidValueProvider(node, attribute, null);
				}

				// default to string value provider
				return new StringValueProvider(node, attribute, null);

			}

		}

	}

	/**
	 * Return true if we should load children for entry with following object classes
	 *
	 * @param objectClasses object classes of Entry
	 * @return TRUE if skip loading | FALSE otherwise
	 */
	public static boolean skipLoadingChildren(List<String> objectClasses) {
		for (String objectClass : objectClasses) {
			if (objectClassesForNodesWithoutChildren.contains(objectClass)) return true;
		}
		return false;
	}

	/**
	 * Refresh specified LdapTreeNode from the LDAP.
	 * This call waits for actual loading if not called from DispatcherThread, otherwise loading is non-blocking.
	 *
	 * @param node LdapTreeNode to get attributes for
	 */
	public static void refresh(@NotNull LdapEntryTreeNode node) {
		assert ApplicationManager.getApplication().isDispatchThread();
		refresh(node, RefreshType.ENTRY);
	}

	/**
	 * Refresh specified LdapTreeNode from the LDAP.
	 *
	 * @param node LdapTreeNode to get attributes and children for
	 * @param refreshType what kind of data we want to refresh - entry, entry with children etc.
	 */
	public static void refresh(@NotNull LdapEntryTreeNode node, @NotNull RefreshType refreshType) {
		assert ApplicationManager.getApplication().isDispatchThread();

		// FIXME - we must somehow allow specifying search options - filter, requestedAttributes etc.
		// TODO - we probably want this function to get project from input params, so we wouldn't have to guess it while getting LdapTreePanel

		final LdapEntryTreeNode treeNode = node;

		boolean fetchEntry = Arrays.asList(RefreshType.ENTRY, RefreshType.WITH_CHILDREN).contains(refreshType);
		boolean withChildren = Arrays.asList(RefreshType.ONLY_CHILDREN, RefreshType.WITH_CHILDREN).contains(refreshType);

		// update UI if Entry has opened Editor
		if (fetchEntry && treeNode.getEditor() != null) {
			LdapEntryEditor editor = treeNode.getEditor();
			// clear selection otherwise UI refresh race-condition fuck-up the IDE
			if (editor.getTable().isEditing()) editor.getTable().getCellEditor().cancelCellEditing();
			editor.getTable().getSelectionModel().clearSelection();
			// update loading UI
			editor.getModel().clearItems();
			editor.getTable().getRowSorter().allRowsChanged();
			editor.setLoading(true);
		}

		final LdapTreePanel treePanel = LdapTreePanel.getInstance();
		if (treePanel == null) {
			log.warn("We couldn't get LdapTreePanel from IDE by guessing project from active window.");
			return;
		}

		if (withChildren && treeNode.getAllowsChildren()) {
			treePanel.getTree().setPaintBusy(true);
		}

		Task searchTask = new Task.Backgroundable(treePanel.getProject(),
				"Refreshing "+treeNode.getDn(),
				true, PerformInBackgroundOption.ALWAYS_BACKGROUND) {

			Exception exception = null;
			boolean nodeChanged = false;
			List<LdapEntryTreeNode> children = null;
			LdapConnection connection = null;
			boolean notExists = false;

			@Override
			public void run(@NotNull ProgressIndicator indicator) {

				// Fetch Entry
				final Entry entry;

				try {

					setTitle("Contacting LDAP...");
					connection = treeNode.getLdapServer().getConnection();

					if (fetchEntry) {

						setTitle("Fetching Entry: "+treeNode.getDn());
						indicator.checkCanceled();
						entry = LdapUtils.getEntry(treeNode, connection);
						if (entry == null) {
							LdapNotificationHandler.handleError(null, treeNode.getLdapServer().getName(), "Entry with DN '"+treeNode.getDn()+"' doesn't exists!");
							notExists = true;
							return;
						}
						treeNode.setEntry(entry);
					}

					if (withChildren) {

						// Fetch Children
						if (LdapNodeType.ROOT_DSE.equals(treeNode.getNodeType())) {
							log.info("Searching for children of Root DSE");
						} else {
							log.info("Searching for children of {}", treeNode.getDn());
						}

						if (treeNode.getAllowsChildren()) {
							setTitle("Fetching children of Entry: "+treeNode.getDn());
							indicator.checkCanceled();
							children = LdapUtils.getChildren(treeNode, connection);
						}

						indicator.checkCanceled();
						treeNode.setChildrenLoaded(true);

						if (children != null) {

							// node may have and has children
							Collections.sort(children);
							treeNode.removeAllChildren();

							// resolve if children should be wrapped by X
							LdapSettings ldapSettings = LdapUtils.getPreferredLdapSettings(treeNode.getLdapServer().getSettings());
							int wrapLimit = ldapSettings.getWrapChildrenBy();

							if (wrapLimit <= 0 || children.size() < wrapLimit) {
								for (LdapEntryTreeNode node : children) {
									// update model
									treeNode.add(node);
								}
							} else {
								// wrap all children in middle nodes
								boolean notComplete = true;
								int fromIndex = 1;
								int toIndex = wrapLimit + 1;
								while (notComplete) {

									if (toIndex > children.size()) {
										// end of the list
										toIndex = children.size();
										notComplete = false;
									}
									List<LdapEntryTreeNode> sublist = children.subList(fromIndex - 1, toIndex);

									LdapTreeNodeWrapper wrapper = new LdapTreeNodeWrapper(treeNode.getLdapServer(), fromIndex, (toIndex - ((notComplete) ? 1 : 0)));
									// add children
									for (LdapEntryTreeNode childNode : sublist) {
										wrapper.add(childNode);
									}
									// add wrapper
									treeNode.add(wrapper);

									// move index
									fromIndex = toIndex;
									toIndex = toIndex + wrapLimit;

								}

							}

							nodeChanged = true;

						}

					}

				} catch (LdapException e) {

					exception = e;

				}

			}

			@Override
			public void onFinished() {

				// close connection even if user canceled operation
				if (connection != null) {
					try {
						node.getLdapServer().releaseConnection(connection);
					} catch (LdapException e) {
						LdapNotificationHandler.handleError(e, node.getLdapServer().getName(), "Can't release connection.");
					}
				}

			}

			@Override
			public void onSuccess() {

				if (withChildren && treeNode.getAllowsChildren()) {
					treePanel.getTree().setPaintBusy(false);
				}

				if (fetchEntry && treeNode.getEditor() != null) {
					treeNode.getEditor().setLastState(treeNode.getEntry());
					treeNode.getEditor().getModel().refresh();
					treeNode.getEditor().setLoading(false);
				}

				if (exception != null) {

					if (LdapNodeType.ROOT_DSE.equals(treeNode.getNodeType())) {
						LdapNotificationHandler.handleError(exception, treeNode.getLdapServer().getName(), "Can't retrieve 'ROOT_DSE' Entry from LDAP server.");
					} else {
						LdapNotificationHandler.handleError(exception, treeNode.getLdapServer().getName(), "Can't retrieve '"+treeNode.getDn().toString()+"' Entry from LDAP server.");
					}

				} else {

					if (notExists) {

						//((DefaultTreeModel)treePanel.getTree().getModel()).removeNodeFromParent(treeNode);
						LdapUtils.refresh((LdapEntryTreeNode)treeNode.getParent(), RefreshType.WITH_CHILDREN);
						treePanel.getModel().nodeStructureChanged(treeNode.getParent());

					} else if (withChildren && treeNode.getAllowsChildren() && nodeChanged) {

						// make sure label is refreshed when children are changed
						treePanel.getModel().nodeChanged(treeNode);

						// expand node in tree if search for children was triggered by expanding the node
						if (treeNode.getPath() != null &&
								treePanel.getTree().isCollapsed(new TreePath(treeNode.getPath()))) {

							if (!treeNode.isLeaf()) {
								// invoke toggleExpandState() on tree path to make sure nodes are visible on expand
								LdapUIUtils.toggleExpandStateOnTree(treePanel.getTree(), new TreePath(treeNode.getPath()));
							} else {
								// node is or has changed to leaf (has no children). Fire event anyway to trigger our logic for opening file !!
								try {
									treePanel.getTree().fireTreeWillExpand(new TreePath(treeNode.getPath()));
								} catch (ExpandVetoException e) {
									// shouldn't expand, doesn't matter, its expected
								}
							}
						}

					} else {
						// make sure tree is updated since setting Entry might fail to update the node
						treePanel.getModel().nodeChanged(treeNode);
					}

				}
			}
		};

		ProgressManager.getInstance().run(searchTask);

	}

	/**
	 * Return proper instance of LdapSettings based on the fact, if connection settings override default or not.
	 *
	 * @see LdapSettings
	 * @see LdapServerSettings
	 * @see LdapPluginSettings
	 *
	 * @param localSettings Local settings retrieved from connection
	 * @return Instance of settings any logic should use (either global or from connection)
	 */
	public static LdapSettings getPreferredLdapSettings(LdapServerSettings localSettings) {

		LdapSettings ldapSettings;
		if (localSettings != null && localSettings.isOverrideIDESettings()) {
			ldapSettings = localSettings;
		} else {
			ldapSettings = LdapPluginSettingsService.getInstance().getState();
		}
		return ldapSettings;

	}

	/**
	 * Return standard set of requested attributes for entry configured either on IDE or Connection settings.
	 *
	 * @param node Node to get requested attributes for
	 * @return Set of attribute names or placeholders like *, + (all user and operational attributes).
	 */
	public static Set<String> getRequestedAttributes(@NotNull LdapEntryTreeNode node) {

		// Get requested attributes to get with entry
		LdapSettings settings = LdapUtils.getPreferredLdapSettings(node.getLdapServer().getSettings());

		Set<String> requestedAttributes = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
		requestedAttributes.addAll(settings.getFetchOnlyAttributes());
		if (requestedAttributes.isEmpty()) {
			if (settings.isFetchUserAttributes()) {
				requestedAttributes.add(SchemaConstants.ALL_USER_ATTRIBUTES);
			}
			if (settings.isFetchOperationAttributes()) {
				requestedAttributes.add(SchemaConstants.ALL_OPERATIONAL_ATTRIBUTES);
			}
		}
		return requestedAttributes;

	}

	/**
	 * Return list of {@link LdapServer} stored within current {@link Project} and IDE.
	 * Passwords are not part of stored objects. You must retrieve them using {@link LdapUtils#loadLdapServerCredentials}
	 * whenever they are needed (e.g. changing settings / connecting to the server).
	 *
	 * @return List of all connections.
	 */
	public static List<LdapServer> getLdapServers(@NotNull Project project) {

		synchronized (LdapUtils.class) {

			List<LdapServer> ldapServers = new ArrayList<>();

			// IDE
			ldapServers.addAll(LdapPluginSettingsService.getInstance().getState().getAllLdapServers().values());
			// Project
			ldapServers.addAll(LdapProjectSettingsService.getInstance(project).getState().getAllLdapServers().values());

			return ldapServers;

		}

	}

	/**
	 * Load LDAP server passwords from the {@link PasswordSafe} and stores them within each passed {@link LdapServer} object.
	 *
	 * @param ldapServers List of LDAP servers to load credentials for
	 */
	public static void loadLdapServerCredentials(@NotNull List<LdapServer> ldapServers) {

		synchronized (LdapUtils.class) {

			for (LdapServer conn : ldapServers) {

				// Retrieving Password
				// if password shouldn't be remembered, it will be used from "memoryOnly"
				CredentialAttributes attributes = new CredentialAttributes("IntelliJ Platform LDAP — " + conn.getUuid(), conn.getUserName(), LdapTreePanel.class, !conn.isRememberPassword());
				String password = PasswordSafe.getInstance().getPassword(attributes);

				// Overwrite only if run-time password is empty (initial load of connections)
				if (StringUtils.isBlank(conn.getPassword())) {
					if (!StringUtils.isBlank(password)) {
						// set only valid passwords
						conn.setPassword(password);
					} else {
						conn.setPassword(null);
					}
				}

			}

		}

	}

	/**
	 * Store list of {@link LdapServer}s within current Project or IDE.
	 *
	 * Performed operation is determined based on LdapServer state.
	 * See {@link LdapServer#isGlobal()} and {@link LdapServer#isToBeRemoved()}
	 *
	 * @param project current Project performing change operation
	 * @param modifiedConnections Modified connections to be updated
	 */
	public static void storeLdapServers(@NotNull Project project, @NotNull List<LdapServer> modifiedConnections) {

		synchronized (LdapUtils.class) {

			List<LdapServer> currentServers = getLdapServers(project);

			// disconnect before saving, since we might be connected internally to the LDAP server
			modifiedConnections.stream().filter(LdapServer::isConnected).forEach(LdapServer::disconnect);

			for (LdapServer ldapServer : modifiedConnections) {

				String newPassword = "";
				if (ldapServer.getPassword() != null) {
					newPassword = ldapServer.getPassword();
				}

				// store current password to credential store
				// if password shouldn't be remembered, it will be "memoryOnly"
				CredentialAttributes attributes = new CredentialAttributes("IntelliJ Platform LDAP — "+ldapServer.getUuid(), ldapServer.getUserName(), LdapTreePanel.class, !ldapServer.isRememberPassword());
				Credentials saveCredentials = new Credentials(attributes.getUserName(), newPassword);
				PasswordSafe.getInstance().set(attributes, saveCredentials);

			}

			LdapServersStore ideStorage = LdapPluginSettingsService.getInstance().getState();
			LdapServersStore projectStorage = LdapProjectSettingsService.getInstance(project).getState();

			for (LdapServer ldapServer : modifiedConnections) {

				// clear if is to be removed
				if (ldapServer.isToBeRemoved()) {
					ideStorage.removeLdapServerByUUID(ldapServer);
					projectStorage.removeLdapServerByUUID(ldapServer);
					continue;
				}

				// set to proper storage if new or updated
				if (ldapServer.isGlobal()) {
					ideStorage.setLdapServerByUUID(ldapServer);
					// remove from project if it was previously there
					projectStorage.removeLdapServerByUUID(ldapServer);
				} else {
					projectStorage.setLdapServerByUUID(ldapServer);
					// remove from IDE if it was previously there
					ideStorage.removeLdapServerByUUID(ldapServer);
				}
			}

			// change event
			List<LdapServer> added = new ArrayList<>(modifiedConnections);
			List<LdapServer> removed = modifiedConnections.stream().filter(LdapServer::isToBeRemoved).collect(Collectors.toList());
			List<LdapServer> changed = new ArrayList<>(modifiedConnections);

			added.removeAll(currentServers);
			changed.removeAll(added);
			changed.removeAll(removed);

			// submit change events
			ApplicationManager.getApplication().getMessageBus().syncPublisher(LdapConnectionsListener.TOPIC).added(project, added);
			ApplicationManager.getApplication().getMessageBus().syncPublisher(LdapConnectionsListener.TOPIC).removed(project, removed);
			ApplicationManager.getApplication().getMessageBus().syncPublisher(LdapConnectionsListener.TOPIC).changed(project, changed);

		}

	}

	/**
	 * Return {@link LdapServer} by its UUID or null if it doesn't exists.
	 * LdapServer is searched within both Project and IDE storage.
	 *
	 * @param project Current project
	 * @param uuid UUID
	 * @return LdapServer with specified UUID or NULL
	 */
	@Nullable
	public static LdapServer getLdapServerByUUID(@NotNull Project project, @NotNull String uuid) {

		// try IDE
		LdapServer foundConnection = LdapPluginSettingsService.getInstance().getState().getLdapServerByUUID(uuid);

		if (foundConnection == null) {
			// try Project
			foundConnection = LdapProjectSettingsService.getInstance(project).getState().getLdapServerByUUID(uuid);
		}

		return foundConnection;

	}

	/**
	 * A listener for LDAP connections changes.
	 */
	public interface LdapConnectionsListener extends EventListener {

		Topic<LdapConnectionsListener> TOPIC = Topic.create("LDAP connection settings", LdapConnectionsListener.class);

		/**
		 * Called when some LDAP connections were added.
		 *
		 * @param project related project
		 * @param added added connections
		 */
		void added(@NotNull Project project, @NotNull List<LdapServer> added);

		/**
		 * Called when some LDAP connections were removed.
		 *
		 * @param project related project
		 * @param removed removed connections
		 */
		void removed(@NotNull Project project, @NotNull List<LdapServer> removed);

		/**
		 * Called when some LDAP connections were changed (their settings/properties).
		 *
		 * @param project related project
		 * @param changed changed connections
		 */
		void changed(@NotNull Project project, @NotNull List<LdapServer> changed);

	}

}
