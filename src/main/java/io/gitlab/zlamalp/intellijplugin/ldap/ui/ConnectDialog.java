package io.gitlab.zlamalp.intellijplugin.ldap.ui;

import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.ui.JBColor;
import com.intellij.ui.components.JBCheckBox;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBPasswordField;
import com.intellij.ui.components.JBTextField;
import com.intellij.util.ui.JBDimension;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * Provide dialog for entering password when connecting to the LDAP server.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class ConnectDialog extends DialogWrapper {

	private JBTextField bindDN;
	private JBPasswordField password;
	private JBCheckBox rememberPassword;
	private JPanel panel;
	private JBLabel ldapURL;
	private JBLabel previousErrorLabel;
	private LdapServer ldapServer;
	private boolean initialized = false;
	private String previousFailure;

	public ConnectDialog(boolean canBeParent, @NotNull LdapServer ldapServer, String previousFailure) {
		super(canBeParent);
		this.ldapServer = ldapServer;
		this.previousFailure = previousFailure;
		init();
		validate();
	}

	private void initialize() {
		setTitle("Connect to LDAP: " + ldapServer.getName());
		ldapURL.setText(ldapServer.getUrl());
		panel.setPreferredSize(new JBDimension(400, 100));
		bindDN.setText(ldapServer.getUserName());
		rememberPassword.setSelected(ldapServer.isRememberPassword());
		setOKButtonText("Connect");

		if (previousFailure != null && !previousFailure.isEmpty()) {
			previousErrorLabel.setText(previousFailure);
			previousErrorLabel.setForeground(JBColor.RED);
		} else {
			previousErrorLabel.setVisible(false);
		}

		// if we fail connecting, we might retry this -> prefill password
		if (ldapServer.getPassword() != null) {
			password.setText(ldapServer.getPassword());
		}
		initialized = true;
	}

	@Nullable
	@Override
	public JComponent getPreferredFocusedComponent() {

		if (ldapServer.getUserName() == null || ldapServer.getUserName().isEmpty()) {
			return bindDN;
		} else {
			return password;
		}

	}

	@Nullable
	@Override
	protected JComponent createCenterPanel() {
		if (!initialized) {
			initialize();
		}
		return panel;
	}

	/**
	 * Return used password value
	 *
	 * @return password
	 */
	public String getPassword() {
		if (password.getPassword().length > 0) {
			return new String(password.getPassword());
		} else {
			return null;
		}
	}

	/**
	 * Return TRUE if ldapServer should remember provided password
	 *
	 * @return TRUE if provided password should be remembered
	 */
	public boolean getRememberPassword() {
		return rememberPassword.isSelected();
	}

	/**
	 * Return used user name (bind DN).
	 *
	 * @return bind DN
	 */
	public String getUserName() {
		return (bindDN.getText().trim().isEmpty()) ? null : bindDN.getText();
	}

}
