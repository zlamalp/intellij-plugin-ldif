package io.gitlab.zlamalp.intellijplugin.ldap.tree;

import com.intellij.ide.DataManager;
import com.intellij.openapi.Disposable;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.ActionToolbar;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.CommonShortcuts;
import com.intellij.openapi.actionSystem.CustomShortcutSet;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.actionSystem.DefaultActionGroup;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.ui.SimpleToolWindowPanel;
import com.intellij.openapi.util.Disposer;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.WindowManager;
import com.intellij.ui.ColoredTreeCellRenderer;
import com.intellij.ui.SimpleTextAttributes;
import com.intellij.ui.TreeUIHelper;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.treeStructure.Tree;
import com.intellij.util.ui.tree.TreeUtil;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActions;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUIUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.AddConnectionAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.AddEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.BrowseSchemaAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ChangeObjectClassAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ConnectAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.CopyDnAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.DeleteEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.DisconnectAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ConnectionSettingsAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.EditEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ExportToLDIFAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ExportSchemaAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ImportFromLDIFAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.LocateEntryInTreeAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.MoveEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.RefreshEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.RenameEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.SearchAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.SetFilterAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.SynchronizeSchemaAction;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeType;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapProjectSettingsService;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapPluginSettingsService;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.connection.ManageLdapServersDialog;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapSearchResultTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapSearchTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapServerTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNodeWrapper;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeRootNode;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.CustomSearchDialog;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static com.intellij.ui.SimpleTextAttributes.GRAYED_ITALIC_ATTRIBUTES;
import static com.intellij.ui.SimpleTextAttributes.REGULAR_ATTRIBUTES;
import static com.intellij.ui.SimpleTextAttributes.REGULAR_BOLD_ATTRIBUTES;
import static io.gitlab.zlamalp.intellijplugin.ldap.actions.LdapPluginActionPlaces.LDAP_TREE_POPUP;
import static io.gitlab.zlamalp.intellijplugin.ldap.actions.LdapPluginActionPlaces.LDAP_TREE_TOOLBAR;

/**
 * Content of main LDAP plugin {@link ToolWindow}, which is initialized by {@link LdapToolWindowFactory}.
 * It serves as LDAP tree browser and is considered to be entry point for most user actions.
 *
 * It's project wide service and is responsible for loading stored {@link LdapServer} connections within the project
 * and globally within the IDE (see {@link LdapPluginSettingsService} and {@link LdapProjectSettingsService}.
 *
 * LDAP servers are then organized / displayed within {@link LdapTree} using {@link LdapTreeModel} with
 * {@link LdapTreeNode} representing tree nodes.
 *
 * @author Attila Majoros
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapTreePanel extends SimpleToolWindowPanel implements Disposable {

	private static final Logger log = LoggerFactory.getLogger(LdapTreePanel.class);

	public final static String LDAP_TOO_WINDOW_NAME = "LDAP";

	private LdapTree tree;
	private final Project project;
	private LdapTreeModel model;
	private final LdapTreePanel ldapTreePanelRef = this;

	// panel/tree actions
	private final AnAction myConnectionSettingsAction = LdapActions.getWrappedAction(ConnectionSettingsAction.ID);
	private final AnAction myAddConnectionAction = LdapActions.getWrappedAction(AddConnectionAction.ID);
	private final AnAction myConnectAction = LdapActions.getWrappedAction(ConnectAction.ID);
	private final AnAction myDisconnectAction = LdapActions.getWrappedAction(DisconnectAction.ID);

	private final AnAction myEditEntryAction = LdapActions.getWrappedAction(EditEntryAction.ID);
	private final AnAction myRefreshEntryAction = LdapActions.getWrappedAction(RefreshEntryAction.ID);
	private final AnAction myCopyDnAction = LdapActions.getWrappedAction(CopyDnAction.ID);
	private final AnAction myAddEntryAction = LdapActions.getWrappedAction(AddEntryAction.ID);
	private final AnAction myDeleteEntryAction = LdapActions.getWrappedAction(DeleteEntryAction.ID);
	private final AnAction myRenameEntryAction = LdapActions.getWrappedAction(RenameEntryAction.ID);
	private final AnAction myMoveEntryAction = LdapActions.getWrappedAction(MoveEntryAction.ID);
	private final AnAction myChangeObjectClassAction = LdapActions.getWrappedAction(ChangeObjectClassAction.ID);
	private final AnAction myFilterAction = LdapActions.getWrappedAction(SetFilterAction.ID);
	private final AnAction mySearchAction = LdapActions.getWrappedAction(SearchAction.ID);
	private final AnAction myExportLDIFAction = LdapActions.getWrappedAction(ExportToLDIFAction.ID);
	private final AnAction myImportLDIFAction = LdapActions.getWrappedAction(ImportFromLDIFAction.ID);

	private final AnAction mySynchronizeSchemaAction = LdapActions.getWrappedAction(SynchronizeSchemaAction.ID);
	private final AnAction myBrowseSchemaAction = LdapActions.getWrappedAction(BrowseSchemaAction.ID);
	private final AnAction myExportSchemaAction = LdapActions.getWrappedAction(ExportSchemaAction.ID);

	private final AnAction myLocateEntryInTreeAction = LdapActions.getWrappedAction(LocateEntryInTreeAction.ID);

	/**
	 * Return instance of LdapTreePanel IDE Component related to the passed project.
	 * Returns NULL if project is already disposed.
	 *
	 * @return LdapTreePanel
	 */
	public static LdapTreePanel getInstance(@NotNull Project project) {
		if (!project.isDisposed()) return project.getService(LdapTreePanel.class);
		return null;
	}

	/**
	 * Return instance of LdapTreePanel IDE Component for currently active project
	 * (based on active / focused IDE window). Use this if you have no option to get "project"
	 * from the sourcing event.
	 *
	 * @see LdapTreePanel#getInstance(Project)
	 * @return LdapTreePanel
	 */
	public static LdapTreePanel getInstance() {

		// determine active project from focus
		DataContext dataContext = null;
		try {
			dataContext = DataManager.getInstance().getDataContextFromFocusAsync().blockingGet(2000);
		} catch (TimeoutException | ExecutionException e) {
			throw new RuntimeException(e);
		}
		if (dataContext != null) {
			Project project = dataContext.getData(PlatformDataKeys.PROJECT);
			if (project != null) return getInstance(project);
		}
		// old way - from active window
		Project[] projects = ProjectManager.getInstance().getOpenProjects();
		Project activeProject = null;
		for (Project project : projects) {
			Window window = WindowManager.getInstance().suggestParentWindow(project);
			if (window != null && window.isActive()) {
				activeProject = project;
			}
		}
		return (activeProject != null) ? getInstance(activeProject) : null;

	}


	/**
	 * Create default TreePanel for LDAP connections
	 */
	public LdapTreePanel(Project project) {

		super(true, true);
		this.project = project;

		//Disposer.register(project, this);

		registerKeyboardShortcuts();

		buildToolbar();

		// build model and tree
		// FIXME - model should remember previous state
		model = new LdapTreeModel(project, generateTree());
		tree = new LdapTree(project, model);

		// empty state
		tree.getEmptyText().setText("No LDAP connections added.").appendSecondaryText("Add connection", SimpleTextAttributes.LINK_ATTRIBUTES, e -> {
			new ManageLdapServersDialog(getProject(), LdapTreePanel.this, new LdapServer()).show();
		});

		tree.setRootVisible(false);

		// prevent standard expand/collapse on double-click, its handled by our own mouse adapter in order to load child nodes !!
		tree.setToggleClickCount(10);

		// this is a default, but to be sure
		tree.setScrollsOnExpand(true);

		// perform own changes when nodes are updated/inserted/removed/child changed
		// eg. when node is removed, then we close all associated files
		model.addTreeModelListener(getTreeModelListener());

		// handle TreeNode cell rendering
		tree.setCellRenderer(new ColoredTreeCellRenderer() {
			@Override
			public void customizeCellRenderer(@NotNull JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {

				SimpleTextAttributes style = REGULAR_ATTRIBUTES;

				if (value instanceof LdapServerTreeNode) {
					if (((LdapServerTreeNode) value).getLdapServer().isConnected()) {
						style = REGULAR_BOLD_ATTRIBUTES;
					}
				}

				if (value instanceof LdapTreeNode) {

					LdapTreeNode node = (LdapTreeNode) value;
					setIcon(node.getIcon());

					/* TODO - display modified state in tree
					if (node instanceof LdapEntryTreeNode) {
						if (((LdapEntryTreeNode) node).getEditor() instanceof LdapEntryEditor) {
							if (((LdapEntryTreeNode) node).getEditor().isModified()) {
								style = style.derive(style.getStyle(), JBColor.BLUE, null, null);
							}
						}
					}
					*/

					append(node.getTreeNodePresentation(), style, true);

					if (node instanceof LdapEntryTreeNode) {

						// all wrappers, count sub-entries
						// not wrapper - count self
						if (node.getAllowsChildren() && !Objects.equals(((LdapEntryTreeNode) node).getNodeType(), LdapNodeType.ROOT_DSE)) {
							List<LdapTreeNode> children = ((LdapEntryTreeNode) node).getChildren();
							int size = children.size();
							if (size > 0 && children.get(0) instanceof LdapTreeNodeWrapper) {
								for (LdapTreeNode child : children) {
									if (child instanceof LdapTreeNodeWrapper) {
										// all wrappers, count sub-entries
										size = size + ((LdapTreeNodeWrapper) child).getChildren().size();
									} else {
										// not wrapper - count self
										size++;
									}
								}
							}
							if (size > 0) {
								super.append(" (" + NumberFormat.getIntegerInstance().format(size) + ")", GRAYED_ITALIC_ATTRIBUTES, false);
							}
						}

					}

				} else {
					append("Not LdapTreeNode item", style);
				}

			}
		});

		// if node can have children, search for them before node expansion
		// -> it will update the node and expand it if necessary once children are loaded
		tree.addTreeWillExpandListener(new TreeWillExpandListener() {
			@Override
			public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException {

				if (Disposer.isDisposed(ldapTreePanelRef)) {
					throw new ExpandVetoException(event);
				}

				if (event.getPath().getLastPathComponent() instanceof LdapEntryTreeNode) {
					LdapEntryTreeNode node = (LdapEntryTreeNode)event.getPath().getLastPathComponent();
					if (node.getAllowsChildren() && !node.isChildrenLoaded()) {
						// get children
						LdapUtils.refresh(node, LdapUtils.RefreshType.ONLY_CHILDREN);
						throw new ExpandVetoException(event);
					} else if (node.isLeaf()) {
						// open editor if entry was found to be without children after all
						boolean previouslyOpened = FileEditorManager.getInstance(project).isFileOpen(node.getVirtualFile());
						FileEditorManager.getInstance(project).openFile(node.getVirtualFile(), true, true);
						if (!previouslyOpened) {
							// do not refresh entry if file is already opened
							LdapUtils.refresh(node, LdapUtils.RefreshType.ENTRY);
						}
					}
				} else if (event.getPath().getLastPathComponent() instanceof LdapServerTreeNode) {
					LdapServerTreeNode node = (LdapServerTreeNode)event.getPath().getLastPathComponent();
					if (node.isLeaf()) throw new ExpandVetoException(event);
				} else if (event.getPath().getLastPathComponent() instanceof LdapSearchTreeNode) {
					LdapSearchTreeNode node = (LdapSearchTreeNode)event.getPath().getLastPathComponent();
					if (node.isLeaf()) throw new ExpandVetoException(event);
				}
			}

			@Override
			public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {

			}
		});

		// This captures keys before standard registered actions are performed
		tree.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				if (Disposer.isDisposed(ldapTreePanelRef)) {
					e.consume();
					return;
				}

				if (e.getKeyCode() == KeyEvent.VK_ENTER) {

					// edit entry if not expandable, otherwise standard tree "toggle" action happens
					LdapEntryTreeNode[] selectedNodes = tree.getSelectedNodes(LdapEntryTreeNode.class, null);
					if (selectedNodes.length > 0) {
						if (!selectedNodes[0].getAllowsChildren()) {
							e.consume();
							boolean previouslyOpened = FileEditorManager.getInstance(project).isFileOpen(selectedNodes[0].getVirtualFile());
							FileEditorManager.getInstance(project).openFile(selectedNodes[0].getVirtualFile(), true, true);
							if (!previouslyOpened) {
								// do not refresh entry if file is already opened
								LdapUtils.refresh(selectedNodes[0], LdapUtils.RefreshType.ENTRY);
							}
							return;
						}
					}

					// start new custom search
					LdapSearchTreeNode[] selectedNodes2 = tree.getSelectedNodes(LdapSearchTreeNode.class, null);
					if (selectedNodes2.length > 0) {
						e.consume();
						CustomSearchDialog dialog = new CustomSearchDialog(project, selectedNodes2[0].getLdapServer(), selectedNodes2[0]);
						dialog.show();
						return;
					}

					// edit custom search
					LdapSearchResultTreeNode[] selectedNodes3 = tree.getSelectedNodes(LdapSearchResultTreeNode.class, null);
					if (selectedNodes3.length > 0) {
						e.consume();
						CustomSearchDialog dialog = new CustomSearchDialog(project, selectedNodes3[0].getLdapServer(), selectedNodes3[0]);
						dialog.show();
					}

				} else if (e.getKeyCode() == KeyEvent.VK_RIGHT || e.getKeyCode() == KeyEvent.VK_KP_RIGHT) {

					// start new custom search on key-right
					LdapSearchTreeNode[] selectedNodes3 = tree.getSelectedNodes(LdapSearchTreeNode.class, null);
					if (selectedNodes3.length > 0) {
						e.consume();
						CustomSearchDialog dialog = new CustomSearchDialog(project, selectedNodes3[0].getLdapServer(), selectedNodes3[0]);
						dialog.show();
					}

				}
			}
		});

		// we must explicitly handle mouse clicks and nodes expansion !!
		tree.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (Disposer.isDisposed(ldapTreePanelRef)) {
					e.consume();
					return;
				}

				if (SwingUtilities.isRightMouseButton(e)) {

					// handle right click menu
					int closestRowForLocation = tree.getClosestRowForLocation(e.getX(), e.getY());
					if (closestRowForLocation != -1) {
						TreePath pathForRow = tree.getPathForRow(closestRowForLocation);
						if (pathForRow != null) {
							Object lastPathComponent = pathForRow.getLastPathComponent();
							if (lastPathComponent instanceof LdapTreeNode) {
								openTreePopupMenu((LdapTreeNode)lastPathComponent, e.getX(), e.getY(), tree.getSelectedNodes(LdapTreeNode.class, null));
							}
						}
					}

				} else if (e.getClickCount() > 1) {

					Object comp = null;
					TreePath path = null;

					// get closest row for click location
					int closestRowForLocation = tree.getClosestRowForLocation(e.getX(), e.getY());
					if (closestRowForLocation != -1) {
						path = tree.getPathForRow(closestRowForLocation);
						comp = path.getLastPathComponent();
					}

					// no row is even close
					if (path == null || comp == null) return;

					// did user click on the row or was it just close ?
					int rowBottom = (int)tree.getRowBounds(closestRowForLocation).getY();
					int rowTop = rowBottom + (int)tree.getRowBounds(closestRowForLocation).getHeight();
					int clickedY = (int)e.getPoint().getY();
					if (clickedY < rowBottom || clickedY > rowTop) {
						log.trace("User clicked outside of the 'closest row' (vertically). Canceling");
						return;
					}

					// init root node
					if (comp instanceof LdapServerTreeNode) {
						((LdapServerTreeNode) comp).init();
					}

					// open entry node
					if (comp instanceof LdapEntryTreeNode) {
						LdapEntryTreeNode treeNode = (LdapEntryTreeNode) comp;
						// open entry if node is leaf (has no children or can't have children)
						if (treeNode.isLeaf()) {
							boolean previouslyOpened = FileEditorManager.getInstance(project).isFileOpen(treeNode.getVirtualFile());
							FileEditorManager.getInstance(project).openFile(treeNode.getVirtualFile(), true, true);
							if (!previouslyOpened) {
								// do not refresh entry if file is already opened
								LdapUtils.refresh(treeNode, LdapUtils.RefreshType.ENTRY);
							}
							return;
						}
					}

					// start new custom search
					if (comp instanceof LdapSearchTreeNode) {
						if (((LdapSearchTreeNode) comp).isLeaf()) {
							CustomSearchDialog dialog = new CustomSearchDialog(project, ((LdapSearchTreeNode) comp).getLdapServer(), (LdapSearchTreeNode)comp);
							dialog.show();
							return;
						}
					}

					// expand/collapse tree nodes manually - since we override default toggle click count of 2 to 10.
					LdapUIUtils.toggleExpandStateOnTree(tree, path);

				}
			}
		});

		// is this anyhow useful ??
		tree.setLargeModel(true);

		// allow IntelliJ SpeedSearch
		TreeUIHelper.getInstance().installTreeSpeedSearch(tree);

		// put tree to scroll and UI
		JBScrollPane treeScroll = new JBScrollPane(tree);
		super.setContent(treeScroll);

		// our tree panel/model must listen to changes in LDAP connections
		ApplicationManager.getApplication().getMessageBus().connect().subscribe(LdapUtils.LdapConnectionsListener.TOPIC, new LdapUtils.LdapConnectionsListener() {

			@Override
			public void added(@NotNull Project project, @NotNull List<LdapServer> added) {
				if (added.isEmpty()) return;
				for (LdapServer add : added) {
					// store only global or own project changes
					if (add.isGlobal() || project.equals(LdapTreePanel.this.project)) {
						getModel().add((DefaultMutableTreeNode)getModel().getRoot(), new LdapServerTreeNode(add));
					}
				}
				// make sure LDAP servers are sorted
				getModel().sortChildren((LdapTreeRootNode) getModel().getRoot(), (o1, o2) -> {
					String name1 = ((LdapServerTreeNode)o1).getLdapServer().getName();
					String name2 = ((LdapServerTreeNode)o2).getLdapServer().getName();
					return name1.compareTo(name2);
				});
			}

			@Override
			public void removed(@NotNull Project project, @NotNull List<LdapServer> removed) {
				// disregarding ide/project state, try to remove deleted connections
				for (LdapServer remove : removed) {
					DefaultMutableTreeNode foundNode = TreeUtil.findNodeWithObject((DefaultMutableTreeNode)getModel().getRoot(), remove);
					DefaultMutableTreeNode parent = null;
					if (foundNode != null) {
						parent = (DefaultMutableTreeNode)foundNode.getParent();
						getModel().removeNodeFromParent(foundNode);
					}
					if (parent != null) {
						getModel().nodeChanged(parent);
					}
				}
			}

			@Override
			public void changed(@NotNull Project project, @NotNull List<LdapServer> changed) {
				for (LdapServer change : changed) {
					// store only global or all changes
					if (change.isGlobal() || project.equals(LdapTreePanel.this.project)) {
						LdapServerTreeNode foundNode = (LdapServerTreeNode)TreeUtil.findNodeWithObject((DefaultMutableTreeNode)getModel().getRoot(), change);
						if (foundNode != null) {
							// update LDAP within tree node - FIXME - but does it update LDAP within all instances of editors etc.?
							foundNode.setUserObject(change);
							getModel().nodeChanged(foundNode);
						} else {
							// not yet present in the tree (moved from projectX => IDE and we are updating projectY)
							added(project, Collections.singletonList(change));
						}
					} else {
						// item shouldn't be present in our tree (moved from IDE => projectX and we are updating projectY)
						removed(project, Collections.singletonList(change));
					}
				}
			}

		});
	}

	/**
	 * Generates Tree from saved LDAP connections
	 *
	 * @return Return LDAP Connections ROOT node with available connections as children
	 */
	private LdapTreeRootNode generateTree() {

		LdapTreeRootNode root = new LdapTreeRootNode();
		List<LdapServer> connections = LdapUtils.getLdapServers(project);
		connections.sort(Comparator.comparing(LdapServer::getName));
		for (LdapServer c : connections) {
			root.add(new LdapServerTreeNode(c));
		}
		return root;

	}

	/**
	 * Build ActionToolbar above tree browser component
	 *
	 * - allow managing existing connections
	 * - allow to add/remove entries in LDAPs
	 */
	private void buildToolbar() {

		DefaultActionGroup actionGroup = new DefaultActionGroup();
		actionGroup.addAction(myAddConnectionAction);
		actionGroup.addAction(myConnectionSettingsAction);

		actionGroup.addAction(myDisconnectAction);
		actionGroup.addSeparator();
		actionGroup.addAction(mySynchronizeSchemaAction);
		actionGroup.addAction(myBrowseSchemaAction);
		actionGroup.addAction(myExportSchemaAction);

		/*
		actionGroup.addSeparator();
		actionGroup.addAction(myEditEntryAction);
		actionGroup.addAction(myRefreshEntryAction);
		actionGroup.addAction(myAddEntryAction);
		actionGroup.addAction(myDeleteEntryAction);
		actionGroup.addAction(myExportLDIFAction);
		*/

		ActionToolbar actionToolbar = ActionManager.getInstance().createActionToolbar(LDAP_TREE_TOOLBAR, actionGroup, false);
		actionToolbar.setTargetComponent(this);
		actionToolbar.setOrientation(JToolBar.HORIZONTAL);
		Box toolbarBox = Box.createHorizontalBox();
		toolbarBox.add(actionToolbar.getComponent());

		super.setToolbar(toolbarBox);

	}

	/**
	 * Get Tree
	 *
	 * @return Tree
	 */
	public Tree getTree() {
		return tree;
	}

	/**
	 * Get Tree
	 *
	 * @return Tree
	 */
	public LdapTreeModel getModel() {
		return model;
	}

	/**
	 * Get project this panel is associated with
	 *
	 * @return associated project
	 */
	public Project getProject() {
		return project;
	}

	/**
	 * Open popup menu for all LDAP tree nodes
	 *
	 * @param ldapTreeNode TreeNode nearest to the right-click action (should be also between selected nodes).
	 * @param x X coordinate
	 * @param y Y coordinate
	 * @param allSelectedNodes All nodes currently selected in the tree
	 */
	private void openTreePopupMenu(LdapTreeNode ldapTreeNode, int x, int y, LdapTreeNode[] allSelectedNodes) {

		// STANDARD ENTRY SECTION

		DefaultActionGroup actionGroup = new DefaultActionGroup();
		actionGroup.addAction(myEditEntryAction);
		actionGroup.addAction(myRefreshEntryAction);
		actionGroup.addAction(myCopyDnAction);
		actionGroup.addAction(myAddEntryAction);
		actionGroup.addAction(myDeleteEntryAction);
		actionGroup.addAction(myRenameEntryAction);
		actionGroup.addAction(myMoveEntryAction);
		actionGroup.addAction(myChangeObjectClassAction);
		actionGroup.addSeparator();
		actionGroup.addAction(myFilterAction);
		actionGroup.addAction(mySearchAction);
		actionGroup.addSeparator();
		actionGroup.addAction(myExportLDIFAction);
		actionGroup.addAction(myImportLDIFAction);

		actionGroup.addSeparator();

		// LDAP / CONNECTION SECTION

		boolean someConnected = false;
		boolean someDisconnected = false;
		for (LdapTreeNode node : allSelectedNodes) {
			if (node.getLdapServer().isConnected()) {
				someConnected = true;
			}
			if (!node.getLdapServer().isConnected()) {
				someDisconnected = true;
			}
			if (someConnected && someDisconnected) break; // we know everything
		}
		if (someConnected) {
			actionGroup.addAction(myDisconnectAction);
		}
		if (someDisconnected) {
			actionGroup.addAction(myConnectAction);
		}

		DefaultActionGroup schemaGroup = new DefaultActionGroup();
		schemaGroup.getTemplatePresentation().setText("Schema");
		schemaGroup.setPopup(true);
		schemaGroup.addAction(mySynchronizeSchemaAction);
		schemaGroup.addAction(myBrowseSchemaAction);
		schemaGroup.addAction(myExportSchemaAction);
		actionGroup.add(schemaGroup);
		actionGroup.addAction(myConnectionSettingsAction);

		ActionManager.getInstance().createActionPopupMenu(LDAP_TREE_POPUP, actionGroup).getComponent().show(tree, x, y);

	}

	/**
	 * Create new TreeModelListener
	 *
	 * Updates nodes in Tree when model is modified
	 */
	private TreeModelListener getTreeModelListener() {

		return new TreeModelListener() {
			@Override
			public void treeNodesChanged(TreeModelEvent e) {
			}

			@Override
			public void treeNodesInserted(TreeModelEvent e) {
			}

			@Override
			public void treeNodesRemoved(TreeModelEvent e) {

				Object comp = e.getTreePath().getLastPathComponent();

				// node which had children removed
				if (comp instanceof LdapEntryTreeNode) {

					LdapEntryTreeNode node = (LdapEntryTreeNode)comp;

					// close opened files of removed child nodes
					Object[] children = e.getChildren();
					if (children.length > 0) {
						for (Object child : children) {
							if (child instanceof LdapEntryTreeNode) {
								// remove all children editors
								LdapUIUtils.closeAllFilesForSubtree(getProject(), (LdapEntryTreeNode) child, true);
							}
						}
					} else {
						// remove self if changed node had no children
						LdapUIUtils.closeAllFilesForSubtree(getProject(), node, true);
					}

					// close empty tree
					if (node.getChildren().isEmpty()) {
						if (tree.isExpanded(e.getTreePath())) {
							LdapUIUtils.toggleExpandStateOnTree(tree, e.getTreePath());
						}
					}

				}

			}

			@Override
			public void treeStructureChanged(TreeModelEvent e) {

				Object parent = e.getTreePath().getLastPathComponent();

				// manually collapse node that have been completely changed
				if (tree.isExpanded(e.getTreePath())) {
					LdapUIUtils.toggleExpandStateOnTree(tree, e.getTreePath());
				}

				// Close all Editors of child nodes !!
				if (parent instanceof LdapEntryTreeNode) {
					LdapUIUtils.closeAllFilesForSubtree(getProject(), (LdapEntryTreeNode) parent, false);
				}

			}
		};

	}

	public List<AnAction> getTabActions() {

		return Collections.singletonList(myLocateEntryInTreeAction);

	}

	@Override
	public void dispose() {
		this.model = null;
		this.tree = null;
	}

	private void registerKeyboardShortcuts() {

		// FIXME / TODO - utilize "New" popup to distinguish between AddConnection and AddEntry !!
		myAddConnectionAction.registerCustomShortcutSet(CommonShortcuts.getNew(), this, this);

		myEditEntryAction.registerCustomShortcutSet(CommonShortcuts.getEditSource(), this, this);
		myRenameEntryAction.registerCustomShortcutSet(CommonShortcuts.getRename(), this, this);
		myCopyDnAction.registerCustomShortcutSet(CommonShortcuts.getCopy(), this, this);
		myDeleteEntryAction.registerCustomShortcutSet(CommonShortcuts.getDelete(), this, this);

		myRefreshEntryAction.registerCustomShortcutSet(
				new CustomShortcutSet(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0)),
				this, this);

		myDisconnectAction.registerCustomShortcutSet(
				new CustomShortcutSet(KeyStroke.getKeyStroke(KeyEvent.VK_F2, InputEvent.CTRL_DOWN_MASK)),
				this, this);

		myConnectionSettingsAction.registerCustomShortcutSet(
				new CustomShortcutSet(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, InputEvent.SHIFT_DOWN_MASK)),
				this, this);

		myLocateEntryInTreeAction.registerCustomShortcutSet(
				new CustomShortcutSet(KeyStroke.getKeyStroke(KeyEvent.VK_F1, InputEvent.ALT_DOWN_MASK)),
				this, this);

	}

}

