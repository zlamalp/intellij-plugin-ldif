package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.LdapBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.progress.PerformInBackgroundOption;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.testFramework.LightVirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUIUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.LdapNotificationHandler;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldif.LdifFileType;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.HashSet;
import java.util.Set;


/**
 * Export LDAP server schema to LDIF and open it in LdifVirtualFile.
 *
 * @see LdapTreePanel
 * @see LdapEntryTreeNode
 *
 * @author Pavel Zlámal
 */
public class ExportSchemaAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.exportSchema";
	private static Logger log = LoggerFactory.getLogger(ExportSchemaAction.class);

	public ExportSchemaAction() {
	}

	public ExportSchemaAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		if (LdapActionUtils.isFromTree(e)) {

			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapTreeNode.class, null);
			Set<LdapServer> foundConnections = new HashSet<>();
			for (LdapTreeNode selectedNode : selectedTreeNodes) {
				foundConnections.add(selectedNode.getLdapServer());
			}
			for (LdapServer conn : foundConnections) {
				performAction(treePanel.getProject(), conn);
			}

		} else if (LdapActionUtils.isFromEditor(e)) {

			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null) {
				LdapServer conn = editor.getLdapNode().getLdapServer();
				Project project = PlatformDataKeys.PROJECT.getData(e.getDataContext());
				if (project != null) {
					performAction(project, conn);
				}
			}

		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if SynchronizeSchema action should be active.
	 *
	 * @param e triggered event
	 * @return TRUE if action should be active
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapTreeNode.class, null);
			return selectedTreeNodes.length > 0;
		} else if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			return editor != null && !editor.isLoading();
		}
		return false;

	}

	/**
	 * Perform ExportSchema action on connection.
	 *
	 * @param project sourcing project
	 * @param ldap Connection to export schema for
	 */
	private void performAction(@NotNull Project project, @NotNull LdapServer ldap) {

		Task task = new Task.Backgroundable(project,
				LdapBundle.message("ldap.update.modal.exportSchema"),
				true,
				PerformInBackgroundOption.ALWAYS_BACKGROUND) {

			Exception exception = null;
			String content = null;
			LdapConnection connection = null;

			@Override
			public void run(@NotNull ProgressIndicator indicator) {

				try {

					indicator.setIndeterminate(false);
					indicator.setText(LdapBundle.message("ldap.update.modal.connecting"));
					indicator.setFraction(0.1);
					LdapUIUtils.waitIfUIDebug();
					indicator.checkCanceled();
					connection = ldap.getConnection();

					// connection is handled in method call
					indicator.setFraction(0.5);
					indicator.setText(LdapBundle.message("ldap.update.modal.exportingSchema"));
					LdapUIUtils.waitIfUIDebug();
					indicator.checkCanceled();
					content = LdapUtils.exportSchemaToLdif(ldap, connection);
					indicator.setFraction(1.0);

				} catch (LdapException ex) {
					exception = ex;
				}

			}

			@Override
			public void onFinished() {

				// close connection even if user canceled operation
				if (connection != null) {
					try {
						ldap.releaseConnection(connection);
					} catch (LdapException e) {
						LdapNotificationHandler.handleError(e, ldap.getName(), "Can't release connection.");
					}
				}

			}

			@Override
			public void onSuccess() {

				if (exception != null) {

					LdapNotificationHandler.handleError(exception, ldap.getName(), "Schema was not exported!");

				} else {

					LightVirtualFile vf = new LightVirtualFile("Schema export of " + ldap.getName(), LdifFileType.INSTANCE, content);
					FileEditorManager.getInstance(project).openFile(vf, true, true);
					LdapUIUtils.showEditorNotificationPanelForExport(project, vf);
					log.debug("Schema was exported for {}.", ldap.getName());
					LdapNotificationHandler.notify(ldap.getName(), "Schema was exported.");

				}

			}
		};
		ProgressManager.getInstance().run(task);

	}

}
