package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.DumbAware;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapServerTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.HashSet;
import java.util.Set;

import static com.intellij.openapi.progress.PerformInBackgroundOption.ALWAYS_BACKGROUND;

/**
 * Explicitly disconnect from all selected LdapServerConnections.
 *
 * Keeps the attribute tree for performance and later usage.
 *
 * @see LdapTreePanel
 * @see LdapServer
 * @see LdapServerTreeNode
 * @see LdapEntryTreeNode
 *
 * @author Attila Majoros
 * @author Pavel Zlámal
 */
public class DisconnectAction extends AnAction implements DumbAware {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.disconnect";
	private static Logger log = LoggerFactory.getLogger(DisconnectAction.class);

	public DisconnectAction() {
	}

	public DisconnectAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		if (LdapActionUtils.isFromTree(e)) {

			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapTreeNode.class, null);
			Set<LdapServer> foundLdapServers = new HashSet<>();
			for (LdapTreeNode selectedNode : selectedTreeNodes) {
				foundLdapServers.add(selectedNode.getLdapServer());
			}
			for (LdapServer conn : foundLdapServers) {
				if (conn.isConnected()) {
					Task task = new Task.Backgroundable(e.getProject(),
							"Disconnecting from LDAP "+conn.getName(),
							false,
							ALWAYS_BACKGROUND) {
						@Override
						public void run(@NotNull ProgressIndicator indicator) {
							conn.disconnect();
						}
					};
					ProgressManager.getInstance().run(task);
				}
			}
		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if Disconnect action is allowed for selected LdapNodes.
	 *
	 * @param e Event triggering this action
	 * @return TRUE if Action is active or FALSE
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (e.getProject() == null || e.getProject().isDisposed()) return false;
		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapTreeNode.class, null);
			for (LdapTreeNode node : selectedTreeNodes) {
				if (node.getLdapServer().isConnected()) return true;
			}
		}
		return false;
	}

}
