package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors;

import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.AttributeValueProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;

/**
 * LDAP Attribute Value editor for String (Directory String) syntax.
 *
 * It provides visual UI for editing attribute value and all necessary data (related attribute / new value)
 * when editing is done. It provides value validation (syntax check).
 *
 * @author Attila Majoros
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class StringValueEditor extends DefaultValueEditor {

	private static Logger log = LoggerFactory.getLogger(StringValueEditor.class);

	/**
	 * Create default Attribute value editor with specified action ADD / EDIT.
	 *
	 * @param parent Parent Component for modal dialog
	 * @param valueProvider Value provider for edited Attribute and its Value
	 * @param addingNewValue TRUE if dialog should add new value for same attribute instead of using the one from value provider
	 */
	public StringValueEditor(@NotNull Component parent, @NotNull AttributeValueProvider valueProvider, boolean addingNewValue) {
		super(parent, valueProvider, addingNewValue);
		init();
	}

	@Nullable
	@Override
	protected String getDimensionServiceKey() {
		return DefaultValueEditor.class.getCanonicalName();
	}

}
