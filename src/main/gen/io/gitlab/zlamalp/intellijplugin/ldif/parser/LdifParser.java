// This is a generated file. Not intended for manual editing.
package io.gitlab.zlamalp.intellijplugin.ldif.parser;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifTypes.*;
import static io.gitlab.zlamalp.intellijplugin.ldif.parser.LdifParserUtil.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.lang.LightPsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class LdifParser implements PsiParser, LightPsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, null);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    r = parse_root_(t, b);
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b) {
    return parse_root_(t, b, 0);
  }

  static boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return ldifFile(b, l + 1);
  }

  /* ********************************************************** */
  // attribute-null|(attribute-key SEPARATOR value-normal CRLF)|(attribute-key SEPARATOR_64 value-base64 CRLF)|(attribute-key SEPARATOR_FILE value-url CRLF)
  public static boolean attribute(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "attribute")) return false;
    if (!nextTokenIs(b, KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = attribute_null(b, l + 1);
    if (!r) r = attribute_1(b, l + 1);
    if (!r) r = attribute_2(b, l + 1);
    if (!r) r = attribute_3(b, l + 1);
    exit_section_(b, m, ATTRIBUTE, r);
    return r;
  }

  // attribute-key SEPARATOR value-normal CRLF
  private static boolean attribute_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "attribute_1")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = attribute_key(b, l + 1);
    r = r && consumeToken(b, SEPARATOR);
    p = r; // pin = 2
    r = r && report_error_(b, value_normal(b, l + 1));
    r = p && consumeToken(b, CRLF) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // attribute-key SEPARATOR_64 value-base64 CRLF
  private static boolean attribute_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "attribute_2")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = attribute_key(b, l + 1);
    r = r && consumeToken(b, SEPARATOR_64);
    p = r; // pin = 2
    r = r && report_error_(b, value_base64(b, l + 1));
    r = p && consumeToken(b, CRLF) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // attribute-key SEPARATOR_FILE value-url CRLF
  private static boolean attribute_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "attribute_3")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = attribute_key(b, l + 1);
    r = r && consumeToken(b, SEPARATOR_FILE);
    p = r; // pin = 2
    r = r && report_error_(b, value_url(b, l + 1));
    r = p && consumeToken(b, CRLF) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // KEY option*
  public static boolean attribute_key(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "attribute_key")) return false;
    if (!nextTokenIs(b, KEY)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, ATTRIBUTE_KEY, null);
    r = consumeToken(b, KEY);
    p = r; // pin = 1
    r = r && attribute_key_1(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // option*
  private static boolean attribute_key_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "attribute_key_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!option(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "attribute_key_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // attribute-key SEPARATOR CRLF
  static boolean attribute_null(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "attribute_null")) return false;
    if (!nextTokenIs(b, KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = attribute_key(b, l + 1);
    r = r && consumeTokens(b, 2, SEPARATOR, CRLF);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // ADD CRLF (attribute)+
  public static boolean change_add(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_add")) return false;
    if (!nextTokenIs(b, ADD)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CHANGE_ADD, null);
    r = consumeTokens(b, 1, ADD, CRLF);
    p = r; // pin = 1
    r = r && change_add_2(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // (attribute)+
  private static boolean change_add_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_add_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = change_add_2_0(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!change_add_2_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "change_add_2", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // (attribute)
  private static boolean change_add_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_add_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = attribute(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // DELETE CRLF
  public static boolean change_delete(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_delete")) return false;
    if (!nextTokenIs(b, DELETE)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CHANGE_DELETE, null);
    r = consumeTokens(b, 1, DELETE, CRLF);
    p = r; // pin = 1
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // DELOLDRDN SEPARATOR VALUE_DIGIT CRLF
  public static boolean change_deloldrdn(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_deloldrdn")) return false;
    if (!nextTokenIs(b, DELOLDRDN)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CHANGE_DELOLDRDN, null);
    r = consumeTokens(b, 2, DELOLDRDN, SEPARATOR, VALUE_DIGIT, CRLF);
    p = r; // pin = 2
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // (MODDN | MODRDN) CRLF change-newrdn change-deloldrdn change-newsup?
  public static boolean change_moddn(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_moddn")) return false;
    if (!nextTokenIs(b, "<change moddn>", MODDN, MODRDN)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CHANGE_MODDN, "<change moddn>");
    r = change_moddn_0(b, l + 1);
    p = r; // pin = 1
    r = r && report_error_(b, consumeToken(b, CRLF));
    r = p && report_error_(b, change_newrdn(b, l + 1)) && r;
    r = p && report_error_(b, change_deloldrdn(b, l + 1)) && r;
    r = p && change_moddn_4(b, l + 1) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // MODDN | MODRDN
  private static boolean change_moddn_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_moddn_0")) return false;
    boolean r;
    r = consumeToken(b, MODDN);
    if (!r) r = consumeToken(b, MODRDN);
    return r;
  }

  // change-newsup?
  private static boolean change_moddn_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_moddn_4")) return false;
    change_newsup(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // MODIFY CRLF (change-modify-add|change-modify-delete|change-modify-replace)*
  public static boolean change_modify(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_modify")) return false;
    if (!nextTokenIs(b, MODIFY)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CHANGE_MODIFY, null);
    r = consumeTokens(b, 1, MODIFY, CRLF);
    p = r; // pin = 1
    r = r && change_modify_2(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // (change-modify-add|change-modify-delete|change-modify-replace)*
  private static boolean change_modify_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_modify_2")) return false;
    while (true) {
      int c = current_position_(b);
      if (!change_modify_2_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "change_modify_2", c)) break;
    }
    return true;
  }

  // change-modify-add|change-modify-delete|change-modify-replace
  private static boolean change_modify_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_modify_2_0")) return false;
    boolean r;
    r = change_modify_add(b, l + 1);
    if (!r) r = change_modify_delete(b, l + 1);
    if (!r) r = change_modify_replace(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // KEY_ADD SEPARATOR attribute-key CRLF attribute+ DASH CRLF
  public static boolean change_modify_add(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_modify_add")) return false;
    if (!nextTokenIs(b, KEY_ADD)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CHANGE_MODIFY_ADD, null);
    r = consumeTokens(b, 2, KEY_ADD, SEPARATOR);
    p = r; // pin = 2
    r = r && report_error_(b, attribute_key(b, l + 1));
    r = p && report_error_(b, consumeToken(b, CRLF)) && r;
    r = p && report_error_(b, change_modify_add_4(b, l + 1)) && r;
    r = p && report_error_(b, consumeTokens(b, -1, DASH, CRLF)) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // attribute+
  private static boolean change_modify_add_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_modify_add_4")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = attribute(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!attribute(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "change_modify_add_4", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // KEY_DELETE SEPARATOR attribute-key CRLF DASH CRLF
  public static boolean change_modify_delete(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_modify_delete")) return false;
    if (!nextTokenIs(b, KEY_DELETE)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CHANGE_MODIFY_DELETE, null);
    r = consumeTokens(b, 2, KEY_DELETE, SEPARATOR);
    p = r; // pin = 2
    r = r && report_error_(b, attribute_key(b, l + 1));
    r = p && report_error_(b, consumeTokens(b, -1, CRLF, DASH, CRLF)) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // KEY_REPLACE SEPARATOR attribute-key CRLF attribute+ DASH CRLF
  public static boolean change_modify_replace(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_modify_replace")) return false;
    if (!nextTokenIs(b, KEY_REPLACE)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CHANGE_MODIFY_REPLACE, null);
    r = consumeTokens(b, 2, KEY_REPLACE, SEPARATOR);
    p = r; // pin = 2
    r = r && report_error_(b, attribute_key(b, l + 1));
    r = p && report_error_(b, consumeToken(b, CRLF)) && r;
    r = p && report_error_(b, change_modify_replace_4(b, l + 1)) && r;
    r = p && report_error_(b, consumeTokens(b, -1, DASH, CRLF)) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // attribute+
  private static boolean change_modify_replace_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_modify_replace_4")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = attribute(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!attribute(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "change_modify_replace_4", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // NEWRDN ({SEPARATOR value-normal} | (SEPARATOR_64 value-base64)) CRLF
  public static boolean change_newrdn(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_newrdn")) return false;
    if (!nextTokenIs(b, NEWRDN)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CHANGE_NEWRDN, null);
    r = consumeToken(b, NEWRDN);
    p = r; // pin = 1
    r = r && report_error_(b, change_newrdn_1(b, l + 1));
    r = p && consumeToken(b, CRLF) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // {SEPARATOR value-normal} | (SEPARATOR_64 value-base64)
  private static boolean change_newrdn_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_newrdn_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = change_newrdn_1_0(b, l + 1);
    if (!r) r = change_newrdn_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // SEPARATOR value-normal
  private static boolean change_newrdn_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_newrdn_1_0")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = consumeToken(b, SEPARATOR);
    p = r; // pin = 1
    r = r && value_normal(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // SEPARATOR_64 value-base64
  private static boolean change_newrdn_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_newrdn_1_1")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = consumeToken(b, SEPARATOR_64);
    p = r; // pin = 1
    r = r && value_base64(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // NEWSUP ((SEPARATOR value-normal) | (SEPARATOR_64 value-base64)) CRLF
  public static boolean change_newsup(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_newsup")) return false;
    if (!nextTokenIs(b, NEWSUP)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CHANGE_NEWSUP, null);
    r = consumeToken(b, NEWSUP);
    p = r; // pin = 1
    r = r && report_error_(b, change_newsup_1(b, l + 1));
    r = p && consumeToken(b, CRLF) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // (SEPARATOR value-normal) | (SEPARATOR_64 value-base64)
  private static boolean change_newsup_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_newsup_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = change_newsup_1_0(b, l + 1);
    if (!r) r = change_newsup_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // SEPARATOR value-normal
  private static boolean change_newsup_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_newsup_1_0")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = consumeToken(b, SEPARATOR);
    p = r; // pin = 1
    r = r && value_normal(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // SEPARATOR_64 value-base64
  private static boolean change_newsup_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "change_newsup_1_1")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = consumeToken(b, SEPARATOR_64);
    p = r; // pin = 1
    r = r && value_base64(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // KEY_CHANGE SEPARATOR (change-add|change-delete|change-modify|change-moddn)
  public static boolean changetype(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "changetype")) return false;
    if (!nextTokenIs(b, KEY_CHANGE)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CHANGETYPE, null);
    r = consumeTokens(b, 2, KEY_CHANGE, SEPARATOR);
    p = r; // pin = 2
    r = r && changetype_2(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // change-add|change-delete|change-modify|change-moddn
  private static boolean changetype_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "changetype_2")) return false;
    boolean r;
    r = change_add(b, l + 1);
    if (!r) r = change_delete(b, l + 1);
    if (!r) r = change_modify(b, l + 1);
    if (!r) r = change_moddn(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // KEY_CONTROL SEPARATOR VALUE_OID criticality? control-value? CRLF
  public static boolean control(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "control")) return false;
    if (!nextTokenIs(b, KEY_CONTROL)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, CONTROL, null);
    r = consumeTokens(b, 2, KEY_CONTROL, SEPARATOR, VALUE_OID);
    p = r; // pin = 2
    r = r && report_error_(b, control_3(b, l + 1));
    r = p && report_error_(b, control_4(b, l + 1)) && r;
    r = p && consumeToken(b, CRLF) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // criticality?
  private static boolean control_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "control_3")) return false;
    criticality(b, l + 1);
    return true;
  }

  // control-value?
  private static boolean control_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "control_4")) return false;
    control_value(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // (SEPARATOR value-normal) | (SEPARATOR_64 value-base64) | (SEPARATOR_FILE value-url)
  static boolean control_value(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "control_value")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = control_value_0(b, l + 1);
    if (!r) r = control_value_1(b, l + 1);
    if (!r) r = control_value_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // SEPARATOR value-normal
  private static boolean control_value_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "control_value_0")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = consumeToken(b, SEPARATOR);
    p = r; // pin = 1
    r = r && value_normal(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // SEPARATOR_64 value-base64
  private static boolean control_value_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "control_value_1")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = consumeToken(b, SEPARATOR_64);
    p = r; // pin = 1
    r = r && value_base64(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // SEPARATOR_FILE value-url
  private static boolean control_value_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "control_value_2")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = consumeToken(b, SEPARATOR_FILE);
    p = r; // pin = 1
    r = r && value_url(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // WHITE_SPACE CRITICALITY
  static boolean criticality(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "criticality")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = consumeTokens(b, 1, WHITE_SPACE, CRITICALITY);
    p = r; // pin = 1
    exit_section_(b, l, m, r, p, LdifParser::rule_recover);
    return r || p;
  }

  /* ********************************************************** */
  // KEY_DN ((SEPARATOR value-normal) | (SEPARATOR_64 value-base64)) CRLF
  public static boolean dn(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "dn")) return false;
    if (!nextTokenIs(b, KEY_DN)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, DN, null);
    r = consumeToken(b, KEY_DN);
    p = r; // pin = 1
    r = r && report_error_(b, dn_1(b, l + 1));
    r = p && consumeToken(b, CRLF) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // (SEPARATOR value-normal) | (SEPARATOR_64 value-base64)
  private static boolean dn_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "dn_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = dn_1_0(b, l + 1);
    if (!r) r = dn_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // SEPARATOR value-normal
  private static boolean dn_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "dn_1_0")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = consumeToken(b, SEPARATOR);
    p = r; // pin = 1
    r = r && value_normal(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // SEPARATOR_64 value-base64
  private static boolean dn_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "dn_1_1")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = consumeToken(b, SEPARATOR_64);
    p = r; // pin = 1
    r = r && value_base64(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // ldif-content | ldif-change {
  // }
  public static boolean entry(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "entry")) return false;
    if (!nextTokenIs(b, KEY_DN)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = ldif_content(b, l + 1);
    if (!r) r = entry_1(b, l + 1);
    exit_section_(b, m, ENTRY, r);
    return r;
  }

  // ldif-change {
  // }
  private static boolean entry_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "entry_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = ldif_change(b, l + 1);
    r = r && entry_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // {
  // }
  private static boolean entry_1_1(PsiBuilder b, int l) {
    return true;
  }

  /* ********************************************************** */
  // dn control? changetype CRLF
  static boolean ldif_change(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ldif_change")) return false;
    if (!nextTokenIs(b, KEY_DN)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = dn(b, l + 1);
    r = r && ldif_change_1(b, l + 1);
    r = r && changetype(b, l + 1);
    p = r; // pin = 3
    r = r && consumeToken(b, CRLF);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // control?
  private static boolean ldif_change_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ldif_change_1")) return false;
    control(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // dn attribute+ CRLF
  static boolean ldif_content(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ldif_content")) return false;
    if (!nextTokenIs(b, KEY_DN)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = dn(b, l + 1);
    r = r && ldif_content_1(b, l + 1);
    p = r; // pin = 2
    r = r && consumeToken(b, CRLF);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // attribute+
  private static boolean ldif_content_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ldif_content_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = attribute(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!attribute(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "ldif_content_1", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // !<<eof>> { version? entry+ }
  static boolean ldifFile(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ldifFile")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = ldifFile_0(b, l + 1);
    r = r && ldifFile_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // !<<eof>>
  private static boolean ldifFile_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ldifFile_0")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NOT_);
    r = !eof(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // version? entry+
  private static boolean ldifFile_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ldifFile_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = ldifFile_1_0(b, l + 1);
    r = r && ldifFile_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // version?
  private static boolean ldifFile_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ldifFile_1_0")) return false;
    version(b, l + 1);
    return true;
  }

  // entry+
  private static boolean ldifFile_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ldifFile_1_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = entry(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!entry(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "ldifFile_1_1", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // OPT_SEP OPT
  public static boolean option(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "option")) return false;
    if (!nextTokenIs(b, OPT_SEP)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, OPT_SEP, OPT);
    exit_section_(b, m, OPTION, r);
    return r;
  }

  /* ********************************************************** */
  // !(CRLF | WHITE_SPACE | SEPARATOR)
  static boolean rule_recover(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "rule_recover")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NOT_);
    r = !rule_recover_0(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // CRLF | WHITE_SPACE | SEPARATOR
  private static boolean rule_recover_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "rule_recover_0")) return false;
    boolean r;
    r = consumeToken(b, CRLF);
    if (!r) r = consumeToken(b, WHITE_SPACE);
    if (!r) r = consumeToken(b, SEPARATOR);
    return r;
  }

  /* ********************************************************** */
  // VALUE_64
  public static boolean value_base64(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "value_base64")) return false;
    if (!nextTokenIs(b, VALUE_64)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, VALUE_64);
    exit_section_(b, m, VALUE_BASE_64, r);
    return r;
  }

  /* ********************************************************** */
  // VALUE | VALUE_OID | VALUE_DIGIT
  public static boolean value_normal(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "value_normal")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, VALUE_NORMAL, "<value normal>");
    r = consumeToken(b, VALUE);
    if (!r) r = consumeToken(b, VALUE_OID);
    if (!r) r = consumeToken(b, VALUE_DIGIT);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // VALUE_FILE
  public static boolean value_url(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "value_url")) return false;
    if (!nextTokenIs(b, VALUE_FILE)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, VALUE_FILE);
    exit_section_(b, m, VALUE_URL, r);
    return r;
  }

  /* ********************************************************** */
  // KEY_VERSION WHITE_SPACE? VALUE_DIGIT CRLF
  public static boolean version(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "version")) return false;
    if (!nextTokenIs(b, KEY_VERSION)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, KEY_VERSION);
    r = r && version_1(b, l + 1);
    r = r && consumeTokens(b, 0, VALUE_DIGIT, CRLF);
    exit_section_(b, m, VERSION, r);
    return r;
  }

  // WHITE_SPACE?
  private static boolean version_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "version_1")) return false;
    consumeToken(b, WHITE_SPACE);
    return true;
  }

}
