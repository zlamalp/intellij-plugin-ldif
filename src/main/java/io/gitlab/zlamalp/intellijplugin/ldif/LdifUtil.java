package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.*;
import com.intellij.psi.util.PsiTreeUtil;
import java.util.Base64;
import com.intellij.util.indexing.FileBasedIndex;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.*;

import java.util.*;

/**
 * Utility methods for working with LDIF files.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifUtil {

	/**
	 * Find all LDIF entries in all files in a project
	 *
	 * @param project Project to search for Ldif files
	 * @return List of all entries in all ldif files
	 */
	public static List<LdifEntry> findEntriesInProject(Project project) {

		List<LdifEntry> result = new ArrayList<LdifEntry>();
		Collection<VirtualFile> virtualFiles = FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, LdifFileType.INSTANCE, GlobalSearchScope.allScope(project));

		for (VirtualFile virtualFile : virtualFiles) {
			LdifFile ldifFile = (LdifFile) PsiManager.getInstance(project).findFile(virtualFile);
			if (ldifFile != null) {
				result.addAll(findEntriesInFile(ldifFile));
			}
		}
		return result;
	}

	/**
	 * Find all LDIF entries in a file
	 *
	 * @param file File to be searched for entries
	 * @return List of LdifItem entries
	 */
	public static List<LdifEntry> findEntriesInFile(PsiFile file) {
		List<LdifEntry> result = new ArrayList<LdifEntry>();
		if (file instanceof LdifFile) {
			LdifEntry[] items = PsiTreeUtil.getChildrenOfType(file, LdifEntry.class);
			if (items != null) {
				Collections.addAll(result, items);
			}
		}
		return result;
	}

	/**
	 * Find all LDIF entries in all files in a project with specified DN value (base64 values must be decoded!!)
	 *
	 * @param project Project to search for Ldif files
	 * @param dnValue DN value to search for (if was base64, must be decoded first !!)
	 * @return List of all entries in all ldif files with specified DN value
	 */
	public static List<LdifEntry> findEntriesInProject(Project project, String dnValue) {

		List<LdifEntry> result = new ArrayList<LdifEntry>();
		Collection<VirtualFile> virtualFiles = FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, LdifFileType.INSTANCE, GlobalSearchScope.allScope(project));

		for (VirtualFile virtualFile : virtualFiles) {
			LdifFile ldifFile = (LdifFile) PsiManager.getInstance(project).findFile(virtualFile);
			if (ldifFile != null) {
				result.addAll(findEntriesInFile(ldifFile, dnValue));
			}
		}
		return result;
	}

	/**
	 * Find all LDIF entries in a file with specified DN value (base64 values must be decoded!!)
	 *
	 * @param file File to be searched for entries
	 * @param dnValue DN value to search for (if was base64, must be decoded first !!)
	 * @return List of LdifItem entries with specified DN value
	 */
	public static List<LdifEntry> findEntriesInFile(PsiFile file, String dnValue) {
		List<LdifEntry> result = new ArrayList<LdifEntry>();
		if (dnValue == null) return result;
		if (file instanceof LdifFile) {
			LdifEntry[] items = PsiTreeUtil.getChildrenOfType(file, LdifEntry.class);
			if (items != null) {
				for (LdifEntry entry : items) {
					if (entry.getDn().getValueBase64() != null) {
						String decodedValue = new String(Base64.getDecoder().decode(entry.getDn().getValueBase64().getText().replaceAll("\n ","")));
						if (Objects.equals(dnValue, decodedValue)) {
							result.add(entry);
						}
					} else if (entry.getDn().getValueNormal() != null) {
						if (Objects.equals(dnValue, entry.getDn().getValueNormal().getText().replaceAll("\n ",""))) {
							result.add(entry);
						}
					}
				}
			}
		}
		return result;
	}

}
