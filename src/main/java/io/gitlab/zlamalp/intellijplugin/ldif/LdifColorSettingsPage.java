package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.*;
import org.jetbrains.annotations.*;

import javax.swing.*;
import java.util.Map;

/**
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifColorSettingsPage implements ColorSettingsPage{

	private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
			new AttributesDescriptor("Attribute", LdifSyntaxHighlighter.KEY),
			new AttributesDescriptor("Separator", LdifSyntaxHighlighter.SEPARATOR),
			new AttributesDescriptor("Value", LdifSyntaxHighlighter.VALUE),
			new AttributesDescriptor("Value (base64)", LdifSyntaxHighlighter.BASE64VALUE),
			new AttributesDescriptor("Digits, OID, version", LdifSyntaxHighlighter.NUMBERS),
			new AttributesDescriptor("Options, Changetype, criticality", LdifSyntaxHighlighter.CONSTANT),
			new AttributesDescriptor("Comment", LdifSyntaxHighlighter.COMMENT),
	};

	@Nullable
	@Override
	public Icon getIcon() {
		return LdifIcons.FILE;
	}

	@NotNull
	@Override
	public SyntaxHighlighter getHighlighter() {
		return new LdifSyntaxHighlighter();
	}

	@NotNull
	@Override
	public String getDemoText() {
		return "version: 1\n" +
				"## ADD a single entry to people level\n" +
				"\n" +
				"dn: cn=John Smith,ou=people,dc=example,dc=com\n" +
				"control: 1.2.3.23.252 false: some_value\n" +
				"objectclass: inetOrgPerson\n" +
				"cn: John Smith\n" +
				"cn: John J Smith\n" +
				"cn;lang-cs-CZ: Jan Kovář\n" +
				"sn: Smith\n" +
				"uid: jsmith\n" +
				"userpassword: jSmitH\n" +
				"carlicense: HISCAR 124\n" +
				"homephone: 555-111-2223\n" +
				"mail: j.smith@example.com\n" +
				"mail: jsmith@example.com\n" +
				"mail: john.smith@example.com\n" +
				"ou: Sales\n" +
				"jpegPhoto:: /9j/4AAQSkZJRgABAAAAAQABAAD/2wBDABALD\n" +
				" A4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQ\n" +
				" ERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVG\n" +
				"\n" +
				"## MODIFY the Robert Smith entry\n" +
				"\n" +
				"dn: cn=Robert Smith,ou=people,dc=example,dc=com\n" +
				"changetype: modify\n" +
				"add: telephonenumber\n" +
				"telephonenumber: 555-555-1212\n" +
				"telephonenumber: 212\n" +
				"-\n" +
				"replace: uid\n" +
				"uid: rjosmith\n" +
				"-\n" +
				"replace: mail\n" +
				"mail: robert.smith@example.com\n" +
				"mail: bob.smith@example.com\n" +
				"-\n" +
				"# adds using URL format\n" +
				"add: jpegphoto\n" +
				"jpegphoto:< file://path/to/jpeg/file.jpg\n" +
				"-\n" +
				"delete: description";
	}

	@Nullable
	@Override
	public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
		return null;
	}

	@NotNull
	@Override
	public AttributesDescriptor[] getAttributeDescriptors() {
		return DESCRIPTORS;
	}

	@NotNull
	@Override
	public ColorDescriptor[] getColorDescriptors() {
		return ColorDescriptor.EMPTY_ARRAY;
	}

	@NotNull
	@Override
	public String getDisplayName() {
		return "LDIF";
	}
}
