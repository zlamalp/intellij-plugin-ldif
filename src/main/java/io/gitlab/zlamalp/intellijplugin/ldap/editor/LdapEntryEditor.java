package io.gitlab.zlamalp.intellijplugin.ldap.editor;

import com.intellij.LdapBundle;
import com.intellij.codeHighlighting.BackgroundEditorHighlighter;
import com.intellij.ide.structureView.StructureViewBuilder;
import com.intellij.openapi.actionSystem.ActionGroup;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.ActionToolbar;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonShortcuts;
import com.intellij.openapi.actionSystem.CustomShortcutSet;
import com.intellij.openapi.actionSystem.DefaultActionGroup;
import com.intellij.openapi.actionSystem.Separator;
import com.intellij.openapi.actionSystem.ToggleAction;
import com.intellij.openapi.actionSystem.ex.CheckboxAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.fileEditor.FileEditorLocation;
import com.intellij.openapi.fileEditor.FileEditorState;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.UserDataHolderBase;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.EditorNotificationPanel;
import com.intellij.ui.JBColor;
import com.intellij.ui.components.JBLoadingPanel;
import com.intellij.ui.components.JBLoadingPanelListener;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.util.Alarm;
import com.intellij.util.ui.components.BorderLayoutPanel;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActions;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUIUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.AddEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.BrowseSchemaAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ConnectionSettingsAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.CopyDnAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.DeleteEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ExportSchemaAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ExportToLDIFAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ImportFromLDIFAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.LocateEntryInTreeAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.MoveEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.RenameEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.SearchAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.SetFilterAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.SynchronizeSchemaAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.AddAttributeAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.AddAttributeValueAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.CollapseAllValuesAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.CopyAttributeValueAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.DiscardEditorChangesAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.EditAttributeValueAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.ExpandAllValuesAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.RefreshEditorAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.RemoveAttributeAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.RemoveAttributeValueAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.SaveEditorChangesAction;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.struct.LdapNodeStructureViewBuilderProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeFileType;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.LdapNotificationHandler;
import org.apache.directory.api.ldap.model.constants.SchemaConstants;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.DefaultModification;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Modification;
import org.apache.directory.api.ldap.model.entry.ModificationOperation;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import static io.gitlab.zlamalp.intellijplugin.ldap.actions.LdapPluginActionPlaces.LDAP_EDITOR_POPUP;
import static io.gitlab.zlamalp.intellijplugin.ldap.actions.LdapPluginActionPlaces.LDAP_EDITOR_TOOLBAR;

/**
 * Implementation of {@link FileEditor} for editing {@link LdapNodeVirtualFile}, which represents
 * single LDAP {@link Entry}. Editor is also associated with single {@link LdapEntryTreeNode}, which represents
 * the same in the {@link LdapTreePanel}.
 *
 * Editor UI consists of {@link ActionToolbar} at the top and {@link AttributeTable} as main editor component.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapEntryEditor extends UserDataHolderBase implements FileEditor {

	private static Logger log = LoggerFactory.getLogger(LdapEntryEditor.class);

	// UI
	private JBLoadingPanel panel = new JBLoadingPanel(new BorderLayout(), this);
	private BorderLayoutPanel northPanel = new BorderLayoutPanel();

	// properties
	private final Project project;
	private final LdapNodeVirtualFile file;
	private final PropertyChangeSupport myChangeSupport;
	private StructureViewBuilder structureViewBuilder;
	private AttributeTable table;
	private boolean previousModifiedState;
	private Alarm myAlarm = new Alarm(this);
	private AtomicBoolean isLoading = new AtomicBoolean(false);

	// settings
	private boolean isLocallyReadOnly;
	private boolean directEdit;
	private boolean showDecoratedValues;
	private Set<String> locallyRequestedAttributes;
	private boolean useCompactTable;
	private boolean isShortMenu;

	// Toolbar and Table (popup) Actions
	protected AnAction myEditAttributeValueAction = LdapActions.getWrappedAction(EditAttributeValueAction.ID);
	protected AnAction myAddAttributeValueAction = LdapActions.getWrappedAction(AddAttributeValueAction.ID);
	protected AnAction myRemoveAttributeValueAction = LdapActions.getWrappedAction(RemoveAttributeValueAction.ID);
	protected AnAction myCopyAttributeValueAction = LdapActions.getWrappedAction(CopyAttributeValueAction.ID);
	protected AnAction myRefreshEditorAction = LdapActions.getWrappedAction(RefreshEditorAction.ID);
	protected AnAction myAddAttributeAction = LdapActions.getWrappedAction(AddAttributeAction.ID);
	protected AnAction myRemoveAttributeAction = LdapActions.getWrappedAction(RemoveAttributeAction.ID);

	protected AnAction mySaveChangesAction = LdapActions.getWrappedAction(SaveEditorChangesAction.ID);
	protected AnAction myDiscardChangesAction = LdapActions.getWrappedAction(DiscardEditorChangesAction.ID);

	protected AnAction myCopyDnAction = LdapActions.getWrappedAction(CopyDnAction.ID);
	protected AnAction myAddEntryAction = LdapActions.getWrappedAction(AddEntryAction.ID);
	protected AnAction myDeleteEntryAction = LdapActions.getWrappedAction(DeleteEntryAction.ID);
	protected AnAction myRenameEntryAction = LdapActions.getWrappedAction(RenameEntryAction.ID);
	protected AnAction myMoveEntryAction = LdapActions.getWrappedAction(MoveEntryAction.ID);
	protected AnAction myFilterAction = LdapActions.getWrappedAction(SetFilterAction.ID);
	protected AnAction mySearchAction = LdapActions.getWrappedAction(SearchAction.ID);
	protected AnAction myExportToLDIFAction = LdapActions.getWrappedAction(ExportToLDIFAction.ID);
	protected AnAction myImportFromLDIFAction = LdapActions.getWrappedAction(ImportFromLDIFAction.ID);
	protected AnAction mySynchronizeSchemaAction = LdapActions.getWrappedAction(SynchronizeSchemaAction.ID);
	protected AnAction myBrowseSchemaAction = LdapActions.getWrappedAction(BrowseSchemaAction.ID);
	protected AnAction myExportSchemaAction = LdapActions.getWrappedAction(ExportSchemaAction.ID);
	protected AnAction myConnectionSettingsAction = LdapActions.getWrappedAction(ConnectionSettingsAction.ID);

	private AnAction myLocateEntryAction = LdapActions.getWrappedAction(LocateEntryInTreeAction.ID);

	private ActionGroup myPoupMenu;
	private ActionGroup myShortPoupMenu;

	// last editor state
	private Entry clonedEntry = null;

	private MouseAdapter myMouseAdapter = new MouseAdapter() {
		@Override
		public void mouseReleased(MouseEvent e) {

			if (SwingUtilities.isRightMouseButton(e) && !isLoading()) {
				ActionManager.getInstance().createActionPopupMenu(LDAP_EDITOR_POPUP, isShortMenu ? myShortPoupMenu : myPoupMenu)
						.getComponent().show(table, e.getX(), e.getY());
			}

			int row = table.rowAtPoint(e.getPoint());
			int col = table.columnAtPoint(e.getPoint());
			if (row != -1 && col != -1 && SwingUtilities.isLeftMouseButton(e) && !isLoading()) {
				// Left click on collapse column
				AttributeModelItem modelItem = ((AttributeTable)table).getItem(row, col);
				if (modelItem == null) return;
				try {
					int modelColumn = table.convertColumnIndexToModel(col);
					if (modelColumn == 0) {
						if (modelItem.isCollapsible()) {
							if (modelItem.isCollapsed()) {
								getModel().getExpandedItems().add(modelItem.getAttribute().getId());
							} else {
								getModel().getExpandedItems().remove(modelItem.getAttribute().getId());
							}
							getModel().refresh();
						}
					}
				} catch (IndexOutOfBoundsException ex) {
					// proper column can't be resolved
				}
			}

		}

	};

	/**
	 * Represents current file editor location as "model index" for table.
	 */
	public class MyFileEditorLocation implements FileEditorLocation {

		private LdapEntryEditor editor;
		private int modelIndex = -1;

		public MyFileEditorLocation(@NotNull LdapEntryEditor editor) {
			this.editor = editor;
			if (editor.getTable().getSelectedRow() >= 0) {
				modelIndex = table.convertRowIndexToModel(table.getSelectedRow());
			}
		}

		public int getModelIndex() {
			return this.modelIndex;
		}

		@NotNull
		@Override
		public FileEditor getEditor() {
			return this.editor;
		}

		@Override
		public int compareTo(@NotNull FileEditorLocation o) {
			return this.modelIndex - ((MyFileEditorLocation)o).getModelIndex();
		}

	}

	/**
	 * Create instance of IDEs {@link FileEditor} for editing LDAP entries.
	 * It expect only {@link LdapNodeVirtualFile} type as source data.
	 *
	 * Entry attributes are edited in table (see {@link AttributeTable}).
	 *
	 * @param project Related project
	 * @param file File to be opened in editor
	 */
	public LdapEntryEditor(@NotNull Project project, @NotNull LdapNodeVirtualFile file) {

		this.project = project;
		this.file = file;

		// associated tree node with
		getLdapNode().setEditor(this);

		// FIXME - when in CreateEntry dialog, we don't have Entry object yet, prevent setting such value to last state
		// set last state if exists
		if (getLdapNode().getEntry() != null) {
			setLastState(getLdapNode().getEntry());
		}

		this.myChangeSupport = new PropertyChangeSupport(this);
		this.previousModifiedState = isModified();

		this.table = new AttributeTable(this);

		// TODO - better way of determining changed state ?
		this.table.getModel().addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				markModified(isModified());
			}
		});

		locallyRequestedAttributes = LdapUtils.getRequestedAttributes(getLdapNode());
		LdapSettings settings = LdapUtils.getPreferredLdapSettings(getLdapNode().getLdapServer().getSettings());
		this.directEdit = settings.isUpdateLdap();
		this.showDecoratedValues = settings.isShowDecoratedValues();
		this.isLocallyReadOnly = getLdapNode().getLdapServer().isReadOnly();
		this.useCompactTable = settings.isUseCompactTable();

		// init popup menu groups
		this.myPoupMenu = buildActionGroup(false);
		this.myShortPoupMenu = buildActionGroup(true);

		// configure right-click menu for rows and expandable logic for 1st column
		table.addMouseListener(myMouseAdapter);

		// enable AttributeType popup panel
		table.addMouseMotionListener(LdapUIUtils.createAttributeTypeMouseMotionListener(this, myAlarm));

		// setup CellEditor for value column
		AttributeTableCellEditor attributeTableCellEditor = new AttributeTableCellEditor(this);
		attributeTableCellEditor.addCellEditorListener(new CellEditorListener() {
			@Override
			public void editingStopped(ChangeEvent e) {

				LdapEntryTreeNode ldapNode = getLdapNode();
				AttributeTableCellEditor localEditor = (AttributeTableCellEditor) e.getSource();

				// FIXME - null pointer on table, when editing table and closing editor tab
				if (table == null) return;

				int modelRow = -1;
				try {
					modelRow = table.convertRowIndexToModel(table.getSelectedRow());
				} catch (IndexOutOfBoundsException ex) {
					// ignore
				}

				// cross-check, that somebody doesn't call this manually on non-editable Item
				if (!((AttributeTableModel)table.getModel()).isRowEditable(modelRow)) {
					return;
				}

				if (!localEditor.isEditingStoppedFromInside()) {
					// editing was stopped by external action (focus loss) -> sadly shared logic doesn't call
					// editingCanceled() but editingStopped() instead, so our editor must tell the difference.
					return;
				}

				// get selected model item (column doesn't matter for out model)
				AttributeModelItem selectedItem = table.getSelectedItem();
				if (selectedItem == null) return;

				Value newValue = (Value)localEditor.getCellEditorValue();
				Value oldValue = selectedItem.getRawValue();
				Attribute attribute = selectedItem.getAttribute();

				// We must check, that attribute is present in model and wasn't discarded -> newly created by value provider
				// since somebody could change our model during edit action by mistake
				if (ldapNode.getAttributeByName(attribute.getId()) == null) {
					log.error("Local representation of '{}' doesn't have Attribute we want to edit: {}", ldapNode.getDn(), attribute);
					return;
				}

				// test value change by client schema validation on "clone" of source attribute
				final Attribute clonedAttribute = attribute.clone();
				if (checkAndChangeAttribute(ldapNode, ldapNode.getEntry().clone(), clonedAttribute, oldValue, newValue)) {

					if (isDirectEdit()) {

						// MODIFY LDAP DIRECTLY AND RELOAD ENTRY
						AttributeType type = SchemaUtils.getAttributeType(ldapNode, clonedAttribute);
						// TODO - we probably need to find transitive equality ??
						final boolean hasEquality = (type != null && type.getEquality() != null);

						Task task = new Task.Modal(getProject(), LdapBundle.message("ldap.update.modal.modifyEntry", ldapNode.getDn()), true) {

							Exception exception = null;
							LdapConnection connection = null;

							@Override
							public void run(@NotNull ProgressIndicator indicator) {

								indicator.setIndeterminate(false);
								try {

									indicator.setText(LdapBundle.message("ldap.update.modal.connecting"));
									indicator.setFraction(0.1);
									LdapUIUtils.waitIfUIDebug();
									indicator.checkCanceled();
									connection = ldapNode.getLdapServer().getConnection();

									List<Modification> mods = new ArrayList<>();
									String realModText = LdapBundle.message("ldap.update.modal.changingAttributeValue", clonedAttribute.getUpId());

									if (hasEquality) {
										mods.add(new DefaultModification(ModificationOperation.REMOVE_ATTRIBUTE, clonedAttribute.getId(), oldValue));
										if (newValue != null) {
											mods.add(new DefaultModification(ModificationOperation.ADD_ATTRIBUTE, clonedAttribute.getId(), newValue));
										} else {
											realModText = LdapBundle.message("ldap.update.modal.removingAttributeValue", clonedAttribute.getUpId());
										}
									} else {
										mods.add(new DefaultModification(ModificationOperation.REPLACE_ATTRIBUTE, clonedAttribute));
										realModText = LdapBundle.message("ldap.update.modal.changingAttributeValueUsingReplace", clonedAttribute.getUpId());
									}

									indicator.setFraction(0.5);
									indicator.setText(realModText);

									LdapUIUtils.waitIfUIDebug();
									indicator.checkCanceled();
									log.info("Updating attribute: {}", clonedAttribute.getUpId());
									connection.modify(ldapNode.getDn(), mods.toArray(new Modification[0]));

									indicator.setFraction(1.0);

								} catch (LdapException ex) {
									exception = ex;
								}

							}

							@Override
							public void onFinished() {

								// close connection even if user canceled operation
								if (connection != null) {
									try {
										ldapNode.getLdapServer().releaseConnection(connection);
									} catch (LdapException e) {
										LdapNotificationHandler.handleError(e, ldapNode.getLdapServer().getName(), "Can't release connection.");
									}
								}

							}

							@Override
							public void onSuccess() {

								if (exception != null) {

									// local model was not modified, so just show error
									LdapNotificationHandler.handleError(exception, ldapNode.getLdapServer().getName(), "Entry "+ ldapNode.getDn() +" was not updated.");

								} else {

									// update local model representation
									checkAndChangeAttribute(ldapNode, ldapNode.getEntry(), attribute, oldValue, newValue);
									// we modified LDAP, make current state the last known
									setLastState(getLdapNode().getEntry());

								}

								getModel().refresh();

							}

						};

						ProgressManager.getInstance().run(task);

					} else {

						// MODIFY MODEL AND REFRESH VIEW

						// we don't have to update value provider, since model was changed by modifying source attribute
						// and it will disappeared with this dialog and after refreshing model
						//selectedItem.getValueProvider().setRawValue(valueEditorDialog.getValue());

						// actually change local attribute
						checkAndChangeAttribute(ldapNode, ldapNode.getEntry(), attribute, oldValue, newValue);

						// we had to modify local model before actually deciding "directEdit", so now just refresh
						getModel().refresh();

					}


				}

				// values were equals -> don't modify model or LDAP.

			}

			/**
			 * Return TRUE if newValue is valid for the attribute and is different than oldValue.
			 * During the process, sourced Attribute object values are modified.
			 *
			 * Return FALSE if either values are equals (sourcing Attribute is not modified)
			 * or when modification of Attribute with newValue fails (sourcing Attribute might by modified).
			 *
			 * If you wish to check the change first, you can always pass cloned Attribute object.
			 *
			 * @see Entry#clone()
			 * @see Attribute#clone()
			 *
			 * @param entry Sourcing Entry containing modified Attribute
			 * @param attribute Sourcing Attribute containing all values to be modified
			 * @param oldValue Old value to be removed
			 * @param newValue New value to be added
			 * @return TRUE if new value is different than old and valid and attribute was locally modified / FALSE otherwise
			 */
			private boolean checkAndChangeAttribute(LdapEntryTreeNode ldapNode, Entry entry, Attribute attribute, Value oldValue, Value newValue) {

				// check equality
				if (!Objects.equals(oldValue, newValue)) {

					try {
						// NULL value means empty user input -> removing attribute
						if (newValue != null) attribute.add(newValue);
					} catch (LdapInvalidAttributeValueException e1) {
						LdapNotificationHandler.handleError(e1, ldapNode.getLdapServer().getName(), "Unable to change value of Attribute '" + attribute.getUpId() + "'.");
						return false;
					}
					// remove old value only if there was any
					if (oldValue != null) {
						attribute.remove(oldValue);
					}

					AttributeType type = SchemaUtils.getAttributeType(ldapNode, attribute);
					if (type != null && type.isSingleValued() && attribute.size() > 1) {
						LdapNotificationHandler.handleError(null,ldapNode.getLdapServer().getName(), "Attribute '" + attribute.getUpId() + "' is single valued!");
						return false;
					}

					if (!entry.get(attribute.getId()).iterator().hasNext()) {
						// it was a last value of attribute !!
						try {
							entry.remove(attribute);
						} catch (LdapException e) {
							LdapNotificationHandler.handleError(e, ldapNode.getLdapServer().getName(), "Unable to remove empty Attribute '"+attribute.getUpId()+"'.");
						}
					}

					return true;

				}
				return false;

			}

			@Override
			public void editingCanceled(ChangeEvent e) {
				// no change to object if editing was canceled
			}

		});
		table.getColumn(AttributeTableModel.COLUMN_NAMES[2]).setCellEditor(attributeTableCellEditor);

		/* FIXME - use only once we will have AttributeTableCellEditor2 working
		IdeEventQueue.EventDispatcher myCommiter = new IdeEventQueue.EventDispatcher() {
			@Override
			public boolean dispatch(@NotNull AWTEvent e) {

				if (e instanceof KeyEvent) {
					//System.out.println(e);
				} else {
					return false;
				}

				//if (e.getID() == KeyEvent.KEY_PRESSED) {
				KeyEvent event = (KeyEvent) e;
				if (e.getID() == KeyEvent.KEY_PRESSED && event.getKeyCode() == KeyEvent.VK_ENTER && (event.getModifiers() & InputEvent.CTRL_MASK) != 0) {
					Component owner = UIUtil.findParentByCondition(
							KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner(),
							component -> {
								return component instanceof AttributeTable;
							});
					if (owner instanceof AttributeTable && ((AttributeTable) owner).isEditing()) {
						((AttributeTable) owner).getCellEditor().stopCellEditing();
						//((JBTable) owner).editingStopped((ChangeEvent)e.getSource());
						return true;
					}
				} else if (e.getID() == KeyEvent.KEY_RELEASED && event.getKeyCode() == KeyEvent.VK_ESCAPE) {
					System.out.println(e);
					System.out.println(e.getSource());
					Component focused = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
					Component owner = UIUtil.findParentByCondition(focused, component -> {
						return component instanceof AttributeTable;
					});
					((AttributeTable) focused).editingCanceled(null);
					if (owner instanceof AttributeTable && ((AttributeTable) owner).isEditing()) {
						((AttributeTable) owner).getCellEditor().cancelCellEditing();
						return true;
					}
				}

				//}
				return false;
			}
		};
		IdeEventQueue.getInstance().addDispatcher(myCommiter, this);
		 */

		registerKeyboardShortcuts();

		buildUI();

	}

	private void buildUI() {

		this.panel.setLoadingText(LdapBundle.message("ldap.editor.loading"));
		this.panel.add(northPanel, BorderLayout.NORTH);
		this.panel.add(new JBScrollPane(this.table), BorderLayout.CENTER);

		// cancel all attribute type popups when reloading
		this.panel.addListener(new JBLoadingPanelListener.Adapter() {
			@Override
			public void onLoadingStart() {
				myAlarm.cancelAllRequests();
			}
		});

		DefaultActionGroup actionGroup = new DefaultActionGroup();

		actionGroup.add(new ActionGroup(LdapBundle.message("ldap.editor.updateSwitcher.text", (isDirectEdit()) ? LdapBundle.message("ldap.editor.updateSwitcher.auto.text") : LdapBundle.message("ldap.editor.updateSwitcher.manual.text")), true) {
			@Override
			public boolean displayTextInToolbar() {
				return true;
			}
			@Override
			public boolean useSmallerFontForTextInToolbar() {
				return true;
			}
			@Override
			public void update(@NotNull AnActionEvent e) {
				if (isDirectEdit()) {
					e.getPresentation().setText(LdapBundle.message("ldap.editor.updateSwitcher.text", LdapBundle.message("ldap.editor.updateSwitcher.auto.text")));
					e.getPresentation().setDescription(LdapBundle.message("ldap.editor.updateSwitcher.auto.description"));
				} else {
					e.getPresentation().setText(LdapBundle.message("ldap.editor.updateSwitcher.text", LdapBundle.message("ldap.editor.updateSwitcher.manual.text")));
					e.getPresentation().setDescription(LdapBundle.message("ldap.editor.updateSwitcher.manual.description"));
				}
				if (isLocallyReadOnly()) {
					e.getPresentation().setEnabled(false);
				}
			}

			@NotNull
			@Override
			public AnAction[] getChildren(@Nullable AnActionEvent e) {

				if (isLocallyReadOnly()) {
					return new AnAction[0];
				}

				ArrayList<AnAction> actions = new ArrayList<>();
				actions.add(new CheckboxAction(LdapBundle.message("ldap.editor.updateSwitcher.auto.text"), LdapBundle.message("ldap.editor.updateSwitcher.auto.description"), null) {
					@Override
					public boolean isSelected(@NotNull AnActionEvent e) {
						return isDirectEdit();
					}

					@Override
					public void setSelected(@NotNull AnActionEvent e, boolean state) {

						if (isModified() && !isDirectEdit()) {
							int result = Messages.showYesNoCancelDialog(getComponent(),
									LdapBundle.message("ldap.editor.updateSwitcher.askOnChanges.text"),
									LdapBundle.message("ldap.editor.updateSwitcher.askOnChanges.title"),
									LdapBundle.message("ldap.editor.updateSwitcher.askOnChanges.save"),
									LdapBundle.message("ldap.editor.updateSwitcher.askOnChanges.discard"),
									LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.cancel"),
									Messages.getQuestionIcon());
							if (result == Messages.OK) {
								LdapActions.getWrappedAction(SaveEditorChangesAction.ID).actionPerformed(e);
							} else if (result == Messages.CANCEL) {
								return;
							} else {
								((RefreshEditorAction)LdapActions.getAction(RefreshEditorAction.ID)).actionPerformed(e, false);
							}
						}

						setDirectEdit(state);
					}

				});
				actions.add(new CheckboxAction(LdapBundle.message("ldap.editor.updateSwitcher.manual.text"), LdapBundle.message("ldap.editor.updateSwitcher.manual.description"), null) {
					@Override
					public boolean isSelected(@NotNull AnActionEvent e) {
						return !isDirectEdit();
					}

					@Override
					public void setSelected(@NotNull AnActionEvent e, boolean state) {
						setDirectEdit(!state);
					}
				});

				return actions.toArray(new AnAction[0]);

			}
		});

		// TODO - create place to "store" pending changes or make action to perform diff against current entry.
		actionGroup.add(mySaveChangesAction);
		actionGroup.add(myDiscardChangesAction);
		actionGroup.addSeparator();

		actionGroup.add(myEditAttributeValueAction);
		actionGroup.add(myAddAttributeValueAction);
		actionGroup.add(myRemoveAttributeValueAction);
		actionGroup.add(myCopyAttributeValueAction);
		actionGroup.addSeparator();
		actionGroup.add(myRefreshEditorAction);
		actionGroup.add(myAddAttributeAction);
		actionGroup.add(myRemoveAttributeAction);

		actionGroup.addSeparator();

		// TODO - rename / move entry - we need icons or popup menu
		//actionGroup.addAction(LdapActions.getWrappedAction(RenameEntryAction.ID));
		//actionGroup.addAction(LdapActions.getWrappedAction(MoveEntryAction.ID));
		actionGroup.addAction(myDeleteEntryAction);
		actionGroup.addAction(myExportToLDIFAction);
		// TODO - add/remove objectClass

		// TODO - Implement table preferences - action.

		actionGroup.addSeparator();

		DefaultActionGroup actionGroup2 = new DefaultActionGroup();

		actionGroup2.add(LdapActions.getWrappedAction(ExpandAllValuesAction.ID));
		actionGroup2.add(LdapActions.getWrappedAction(CollapseAllValuesAction.ID));

		actionGroup2.add(new AnAction() {
			@Override
			public void actionPerformed(@NotNull AnActionEvent e) {
				//
			}
			@Override
			public void update(@NotNull AnActionEvent e) {
				e.getPresentation().setIcon(LdapIcons.FILTER);
				e.getPresentation().setText(LdapBundle.message("ldap.editor.table.filter.text"));
				e.getPresentation().setEnabled(false);
				super.update(e);
			}
		});

		// SHOW / HIDE DECORATED VALUES

		actionGroup2.add(new ToggleAction() {
			@Override
			public boolean isSelected(@NotNull AnActionEvent e) {
				return !isShowDecoratedValues();
			}

			@Override
			public void setSelected(@NotNull AnActionEvent e, boolean state) {
				setShowDecoratedValues(!state);
				getTable().repaint();
			}

			@Override
			public void update(@NotNull AnActionEvent e) {
				e.getPresentation().setIcon(LdapIcons.TOGGLE_VISIBILITY);
				if (isSelected(e)) {
					e.getPresentation().setText(LdapBundle.message("ldap.editor.table.showDecorated.text"));
				} else {
					e.getPresentation().setText(LdapBundle.message("ldap.editor.table.showRaw.text"));
				}
			}
		});

		// TABLE settings

		actionGroup2.add(new ActionGroup(LdapBundle.message("ldap.editor.table.settingsGroup.text"), true) {
			@NotNull
			@Override
			public AnAction[] getChildren(@Nullable AnActionEvent e) {

				ArrayList<AnAction> actions = new ArrayList<>();

				actions.add(new CheckboxAction(LdapBundle.message("ldap.editor.table.settingsGroup.fetchUserAttributes.text")) {
					@Override
					public boolean isSelected(@NotNull AnActionEvent e) {
						return getLocallyRequestedAttributes().contains(SchemaConstants.ALL_USER_ATTRIBUTES);
					}

					@Override
					public void setSelected(@NotNull AnActionEvent e, boolean state) {

						int result = Messages.NO;  // means dont save and perform action
						if (isModified() && !isDirectEdit()) {
							result = Messages.showYesNoCancelDialog(getComponent(),
									LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.text"),
									LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.title"),
									LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.save"),
									LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.discard"),
									LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.cancel"),
									Messages.getQuestionIcon());
						}

						if (result == Messages.OK) {
							LdapActions.getWrappedAction(SaveEditorChangesAction.ID).actionPerformed(e);
						} else if (result == Messages.CANCEL) {
							return;
						}

						// update state and refresh
						if (state) {
							getLocallyRequestedAttributes().add(SchemaConstants.ALL_USER_ATTRIBUTES);
						} else {
							getLocallyRequestedAttributes().remove(SchemaConstants.ALL_USER_ATTRIBUTES);
						}
						((RefreshEditorAction)LdapActions.getAction(RefreshEditorAction.ID)).actionPerformed(e, false);

					}
				});

				actions.add(new CheckboxAction(LdapBundle.message("ldap.editor.table.settingsGroup.fetchOperationalAttributes.text")) {
					@Override
					public boolean isSelected(@NotNull AnActionEvent e) {
						return getLocallyRequestedAttributes().contains(SchemaConstants.ALL_OPERATIONAL_ATTRIBUTES);
					}

					@Override
					public void setSelected(@NotNull AnActionEvent e, boolean state) {

						int result = Messages.NO;  // means dont save and perform action
						if (isModified() && !isDirectEdit()) {
							result = Messages.showYesNoCancelDialog(getComponent(),
									LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.text"),
									LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.title"),
									LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.save"),
									LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.discard"),
									LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.cancel"),
									Messages.getQuestionIcon());
						}

						if (result == Messages.OK) {
							LdapActions.getWrappedAction(SaveEditorChangesAction.ID).actionPerformed(e);
						} else if (result == Messages.CANCEL) {
							return;
						}

						// update state and refresh
						if (state) {
							getLocallyRequestedAttributes().add(SchemaConstants.ALL_OPERATIONAL_ATTRIBUTES);
						} else {
							getLocallyRequestedAttributes().remove(SchemaConstants.ALL_OPERATIONAL_ATTRIBUTES);
						}
						((RefreshEditorAction)LdapActions.getAction(RefreshEditorAction.ID)).actionPerformed(e, false);
					}
				});

				actions.add(new Separator());

				actions.add(new CheckboxAction(LdapBundle.message("ldap.editor.table.settingsGroup.sortObjectClassFirst.text")) {
					@Override
					public boolean isSelected(@NotNull AnActionEvent e) {
						return ((AttributeTableModel)table.getModel()).isSortObjectClassFirst();
					}

					@Override
					public void setSelected(@NotNull AnActionEvent e, boolean state) {
						((AttributeTableModel)table.getModel()).setSortObjectClassFirst(state);
						((AttributeTableModel)table.getModel()).refresh();
					}
				});
				actions.add(new CheckboxAction(LdapBundle.message("ldap.editor.table.settingsGroup.sortMustFirst.text")) {
					@Override
					public boolean isSelected(@NotNull AnActionEvent e) {
						return ((AttributeTableModel)table.getModel()).isSortMustAttributesFirst();
					}

					@Override
					public void setSelected(@NotNull AnActionEvent e, boolean state) {
						((AttributeTableModel)table.getModel()).setSortMustAttributesFirst(state);
						((AttributeTableModel)table.getModel()).refresh();
					}
				});
				actions.add(new CheckboxAction(LdapBundle.message("ldap.editor.table.settingsGroup.sortOperationalLast.text")) {
					@Override
					public boolean isSelected(@NotNull AnActionEvent e) {
						return ((AttributeTableModel)table.getModel()).isSortOperationalAttributesLast();
					}

					@Override
					public void setSelected(@NotNull AnActionEvent e, boolean state) {
						((AttributeTableModel)table.getModel()).setSortOperationalAttributesLast(state);
						((AttributeTableModel)table.getModel()).refresh();
					}
				});

				return actions.toArray(new AnAction[0]);
			}

			@Override
			public void update(@NotNull AnActionEvent e) {
				e.getPresentation().setIcon(LdapIcons.SETTINGS);
			}
		});

		// build toolbar
		ActionToolbar actionToolbar = ActionManager.getInstance().createActionToolbar(LDAP_EDITOR_TOOLBAR, actionGroup, true);
		actionToolbar.setOrientation(JToolBar.HORIZONTAL);
		actionToolbar.setTargetComponent(this.panel);
		actionToolbar.getComponent().setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0));
		Box toolbarBox = Box.createHorizontalBox();
		toolbarBox.add(actionToolbar.getComponent());

		ActionToolbar actionToolbar2 = ActionManager.getInstance().createActionToolbar(LDAP_EDITOR_TOOLBAR, actionGroup2, true);
		actionToolbar2.setOrientation(JToolBar.HORIZONTAL);
		actionToolbar2.setTargetComponent(this.panel);
		actionToolbar2.getComponent().setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0));
		actionToolbar2.setReservePlaceAutoPopupIcon(false);
		Box toolbarBox2 = Box.createHorizontalBox();
		toolbarBox2.add(actionToolbar2.getComponent());

		BorderLayoutPanel toolbarPanel = new BorderLayoutPanel();
		toolbarPanel.add(toolbarBox, BorderLayout.WEST);
		toolbarPanel.add(toolbarBox2, BorderLayout.EAST);
		toolbarPanel.setBorder(new MatteBorder(0,0,1,0,JBColor.border()));

		this.northPanel.add(toolbarPanel, BorderLayout.SOUTH);

		// set read-only header
		if (isLocallyReadOnly()) {
			EditorNotificationPanel panel = new EditorNotificationPanel();
			panel.setText(LdapBundle.message("ldap.editor.connectionReadOnly"));

			panel.createActionLabel(LdapBundle.message("ldap.editor.connectionReadOnly.allow"), () -> {
				setLocallyReadOnly(false);
				setHeaderComponent(null);
			});

			panel.createActionLabel(LdapBundle.message("ldap.editor.connectionReadOnly.close"), () -> setHeaderComponent(null));

			setHeaderComponent(panel);
		}

	}

	@NotNull
	@Override
	public JComponent getComponent() {
		return this.panel;
	}

	@Nullable
	@Override
	public JComponent getPreferredFocusedComponent() {
		return this.table;
	}

	@NotNull
	@Override
	public String getName() {
		return "Entry editor";
	}

	@Override
	public void setState(@NotNull FileEditorState fileEditorState) {

	}

	@Override
	public boolean isModified() {
		return !Objects.equals(getLdapNode().getEntry(), clonedEntry);
	}

	/**
	 * Sets Entry representing last known committed state of Editor
	 *
	 * @param entry
	 */
	public void setLastState(@NotNull Entry entry) {
		this.clonedEntry = entry.clone();
	}

	/**
	 * Return Entry representing last known committed state of Editor
	 *
	 * @return
	 */
	public Entry getLastState() {
		return this.clonedEntry;
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public void selectNotify() {
		// called when editor is selected
		markModified(isModified());
	}

	@Override
	public void deselectNotify() {

	}

	/**
	 * Changes modified flag to the new value
	 * @param modified
	 */
	private void markModified(boolean modified) {
		myChangeSupport.firePropertyChange(PROP_MODIFIED, previousModifiedState, modified);
		previousModifiedState = modified;
	}

	@Override
	public void addPropertyChangeListener(@NotNull PropertyChangeListener propertyChangeListener) {
		myChangeSupport.addPropertyChangeListener(propertyChangeListener);
	}

	@Override
	public void removePropertyChangeListener(@NotNull PropertyChangeListener propertyChangeListener) {
		myChangeSupport.removePropertyChangeListener(propertyChangeListener);
	}

	@Nullable
	@Override
	public BackgroundEditorHighlighter getBackgroundHighlighter() {
		return null;
	}

	@Nullable
	@Override
	public FileEditorLocation getCurrentLocation() {
		// construct location with current selected model item
		return new MyFileEditorLocation(this);
	}

	@Override
	public void dispose() {
		myAlarm.cancelAllRequests();
		markModified(false);
		// unregister listeners
		for (PropertyChangeListener listener : myChangeSupport.getPropertyChangeListeners()) {
			myChangeSupport.removePropertyChangeListener(listener);
		}
		((LdapEntryTreeNode)this.file.getLdapTreeNode()).setEditor(null);
		this.table = null;
		this.structureViewBuilder = null;
	}

	@Nullable
	@Override
	public StructureViewBuilder getStructureViewBuilder() {
		if (structureViewBuilder == null) {
			structureViewBuilder = new LdapNodeStructureViewBuilderProvider().getStructureViewBuilder(new LdapNodeFileType(), this.file,  this.project);
		}
		return structureViewBuilder;
	}

	@Override
	public VirtualFile getFile() {
		return this.file;
	}


	// CUSTOM LOGIC

	public boolean isLocallyReadOnly() {
		return this.isLocallyReadOnly;
	}

	public void setLocallyReadOnly(boolean locallyReadOnly) {
		this.isLocallyReadOnly = locallyReadOnly;
	}

	public boolean isShowDecoratedValues() {
		return showDecoratedValues;
	}

	public void setShowDecoratedValues(boolean showDecoratedValues) {
		this.showDecoratedValues = showDecoratedValues;
	}

	public Set<String> getLocallyRequestedAttributes() {
		return locallyRequestedAttributes;
	}

	public boolean isDirectEdit() {
		return directEdit;
	}

	public void setDirectEdit(boolean directEdit) {
		this.directEdit = directEdit;
	}

	public boolean isUseCompactTable() {
		return useCompactTable;
	}

	public void setUseCompactTable(boolean useCompactTable) {
		this.useCompactTable = useCompactTable;
	}

	public boolean isShortMenu() {
		return isShortMenu;
	}

	public void setShortMenu(boolean shortMenu) {
		isShortMenu = shortMenu;
	}

	/**
	 * Sets FileEditor header component, usually some banner with information related to the file.
	 *
	 * @see Editor#setHeaderComponent(JComponent)
	 * @see EditorNotificationPanel
	 *
	 * @param component Component to be set as header
	 */
	protected void setHeaderComponent(JComponent component) {

		BorderLayout layout = (BorderLayout)northPanel.getLayout();
		Component previousComponent = layout.getLayoutComponent(BorderLayout.NORTH);

		if (previousComponent != null) northPanel.remove(previousComponent);

		if (component != null) {
			northPanel.add(component, BorderLayout.NORTH);
		}

		northPanel.updateUI();

	}

	private void registerKeyboardShortcuts() {


		myEditAttributeValueAction.registerCustomShortcutSet(new CustomShortcutSet(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0)),
				table, this);

		myAddAttributeValueAction.registerCustomShortcutSet(CommonShortcuts.getNew(), table, this);
		myRemoveAttributeValueAction.registerCustomShortcutSet(CommonShortcuts.getDelete(), table, this);

		myAddAttributeAction.registerCustomShortcutSet(
				new CustomShortcutSet(KeyStroke.getKeyStroke(KeyEvent.VK_INSERT, InputEvent.CTRL_DOWN_MASK)),
				table, this);
		myRemoveAttributeAction.registerCustomShortcutSet(
				new CustomShortcutSet(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, InputEvent.CTRL_DOWN_MASK)),
				table, this);

		myCopyAttributeValueAction.registerCustomShortcutSet(CommonShortcuts.getCopy(), table, this);
		myRenameEntryAction.registerCustomShortcutSet(CommonShortcuts.getRename(), table, this);

		myRefreshEditorAction.registerCustomShortcutSet(
				new CustomShortcutSet(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0)),
				table, this);

		myConnectionSettingsAction.registerCustomShortcutSet(
				new CustomShortcutSet(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, InputEvent.SHIFT_DOWN_MASK)),
				table, this);

		myLocateEntryAction.registerCustomShortcutSet(
				new CustomShortcutSet(KeyStroke.getKeyStroke(KeyEvent.VK_F1, InputEvent.ALT_DOWN_MASK)),
				table, this);

		mySaveChangesAction.registerCustomShortcutSet(
				new CustomShortcutSet(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK)),
				table, this);

	}

	private ActionGroup buildActionGroup(boolean isShortMenu) {

		DefaultActionGroup actionGroup = new DefaultActionGroup();
		actionGroup.addAction(myEditAttributeValueAction);
		actionGroup.addAction(myAddAttributeValueAction);
		actionGroup.addAction(myRemoveAttributeValueAction);
		if (!isShortMenu) actionGroup.addAction(myCopyAttributeValueAction);
		actionGroup.addSeparator();
		if (!isShortMenu) actionGroup.addAction(myRefreshEditorAction);
		actionGroup.addAction(myAddAttributeAction);
		actionGroup.addAction(myRemoveAttributeAction);

		if (!isShortMenu) {
			actionGroup.addSeparator();
			actionGroup.addAction(myCopyDnAction);
			actionGroup.addAction(myAddEntryAction);
			actionGroup.addAction(myDeleteEntryAction);
			actionGroup.addAction(myRenameEntryAction);
			actionGroup.addAction(myMoveEntryAction);
			actionGroup.addSeparator();

			actionGroup.addAction(myFilterAction);
			actionGroup.addAction(mySearchAction);
			actionGroup.addSeparator();

			actionGroup.addAction(myExportToLDIFAction);
			actionGroup.addAction(myImportFromLDIFAction);
			actionGroup.addSeparator();

			DefaultActionGroup schemaGroup = new DefaultActionGroup();
			schemaGroup.getTemplatePresentation().setText("Schema");
			schemaGroup.setPopup(true);
			schemaGroup.addAction(mySynchronizeSchemaAction);
			schemaGroup.addAction(myBrowseSchemaAction);
			schemaGroup.addAction(myExportSchemaAction);
			actionGroup.add(schemaGroup);
			actionGroup.addAction(myConnectionSettingsAction);

		}
		return actionGroup;

	}


	// FIXME - following is for backward compatibility - we will revisit it later

	/**
	 * Return {@link AttributeTable} associated with this {@link FileEditor}
	 *
	 * @return AttributeTable associated with editor (single LDAP Entry)
	 */
	@NotNull
	public AttributeTable getTable() {
		return this.table;
	}

	/**
	 * Return {@link AttributeTableModel}associated with this {@link FileEditor}
	 *
	 * @return AttributeTableModel associated with editor (single LDAP Entry)
	 */
	@NotNull
	public AttributeTableModel getModel() {
		return (AttributeTableModel) table.getModel();
	}

	/**
	 * Return {@link LdapEntryTreeNode} associated with this {@link FileEditor}
	 *
	 * @return LdapEntryTreeNode associated with editor (single LDAP Entry)
	 */
	@NotNull
	public LdapEntryTreeNode getLdapNode() {
		return (LdapEntryTreeNode) this.file.getLdapTreeNode();
	}

	/**
	 * Return {@link Project} this {@link FileEditor} is associated with
	 *
	 * @return Project associated with editor (single LDAP Entry)
	 */
	@NotNull
	public Project getProject() {
		return this.project;
	}

	/**
	 * Sets loading state of editor and displays "Loading..." above it
	 * in a thread save manner.
	 */
	public void setLoading(boolean loading) {
		setLoading(loading, LdapBundle.message("ldap.editor.loading"));
	}

	/**
	 * Sets loading state of editor and displays custom message above it
	 * in a thread save manner.
	 */
	public synchronized void setLoading(boolean loading, String message) {
		if (loading) {
			this.isLoading.set(true);
			this.panel.setLoadingText(message);
			this.panel.startLoading();
		} else {
			this.panel.stopLoading();
			this.isLoading.set(false);
		}
	}

	/**
	 * Return TRUE if editor is currently loading data
	 * in thread save manner.
	 *
	 * @return TRUE if editor content is loading
	 */
	public synchronized boolean isLoading() {
		return isLoading.get();
	}

}
