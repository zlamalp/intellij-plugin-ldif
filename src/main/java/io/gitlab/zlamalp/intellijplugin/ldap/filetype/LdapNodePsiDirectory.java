package io.gitlab.zlamalp.intellijplugin.ldap.filetype;

import com.intellij.ide.navigationToolbar.NavBarModel;
import com.intellij.mock.MockFileManager;
import com.intellij.openapi.Disposable;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Comparing;
import com.intellij.openapi.util.Computable;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileFilter;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiTreeChangeListener;
import com.intellij.psi.impl.PsiManagerEx;
import com.intellij.psi.impl.PsiManagerImpl;
import com.intellij.psi.impl.PsiModificationTrackerImpl;
import com.intellij.psi.impl.PsiTreeChangeEventImpl;
import com.intellij.psi.impl.file.PsiDirectoryImpl;
import com.intellij.psi.impl.file.impl.FileManager;
import com.intellij.psi.util.PsiModificationTracker;
import com.intellij.ui.treeStructure.Tree;
import com.intellij.util.ui.tree.TreeUtil;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapServerTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeRootNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel.LDAP_TOO_WINDOW_NAME;

/**
 * Implementation of {@link PsiDirectory} for all {@link LdapNodeVirtualFile}s, which are considered directories
 * by their implementation (state of its inner {@link LdapTreeNode}). It is solely used to override default
 * {@link PsiDirectory#navigate(boolean)} method.
 *
 * Instances of it are manually created/returned by {@link LdapNavBarPresentation#adjustElement(PsiElement)}
 * when {@link NavBarModel} calls its {@link NavBarModel#normalize(PsiElement)} method.
 *
 * NOTE: There is no standardized way to return this PsiDirectory generically from our LdapNodeVirtualFile.
 * IDE lack extension point for PsiDirectory presentation and more specifically association like it does
 * for PsiFiles, where FileViewProvider comes in place.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapNodePsiDirectory extends PsiDirectoryImpl {

	MockPsiManager manager;

	public LdapNodePsiDirectory(@NotNull PsiManagerImpl manager, @NotNull VirtualFile file) {
		super(manager, file);
		this.manager = new MockPsiManager(manager.getProject(), manager);
	}

	@NotNull
	@Override
	public PsiManager getManager() {
		return manager;
	}

	@Override
	public void navigate(boolean requestFocus) {

		VirtualFile virtualFile = getVirtualFile();
		if (virtualFile instanceof LdapNodeVirtualFile) {

			LdapTreeNode node = ((LdapNodeVirtualFile)virtualFile).getLdapTreeNode();

			if (node instanceof LdapEntryTreeNode) {

				// Load child entries
				if (node.getAllowsChildren() && !((LdapEntryTreeNode) node).isChildrenLoaded() && ((LdapEntryTreeNode) node).getChildren().isEmpty()) {
					LdapUtils.refresh((LdapEntryTreeNode)node, LdapUtils.RefreshType.WITH_CHILDREN);
				}

				// open file if its is leaf
				if (((LdapEntryTreeNode) node).getChildren().isEmpty()) {
					FileEditorManager.getInstance(getProject()).openFile(virtualFile, requestFocus, true);
				}

				// select in ldap tree browser
				ToolWindowManager.getInstance(getProject()).getToolWindow(LDAP_TOO_WINDOW_NAME).show(new Runnable() {
					@Override
					public void run() {
						Tree tree = LdapTreePanel.getInstance(getProject()).getTree();
						if (tree != null) {
							TreeUtil.selectNode(tree, node);
						}
					}
				});

			} else if (node instanceof LdapServerTreeNode) {
				ToolWindowManager.getInstance(getProject()).getToolWindow(LDAP_TOO_WINDOW_NAME).show(new Runnable() {
					@Override
					public void run() {
						Tree tree = LdapTreePanel.getInstance(getProject()).getTree();
						if (tree != null) {
							TreeUtil.selectNode(tree, node);
						}
					}
				});
			} else if (node instanceof LdapTreeRootNode) {
				ToolWindowManager.getInstance(getProject()).getToolWindow(LDAP_TOO_WINDOW_NAME).show(new Runnable() {
					@Override
					public void run() {
						// TODO - do something, like open LDAP connection settings ?
					}
				});
			}
		}
	}

	@Override
	public boolean canNavigate() {
		return true;
	}

	@Override
	public boolean canNavigateToSource() {
		return true;
	}

	/**
	 * Mock implementation of {@link PsiManager}. It is necessary, since {@link PsiManagerImpl} is final.
	 *
	 * We use it to override default {@link PsiManager#findDirectory(VirtualFile)} implementation since it
	 * returns instances of runtime created PsiJavaDirectoryImpl and its navigate() method doesn't
	 * respect our custom {@link LdapNodePsiDirectory#navigate(boolean)} implementation.
	 *
	 * It wouldn't be necessary if {@link NavBarModel} was not so stubborn when it resolves click/navigate
	 * actions. Since it first correctly calls {@link NavBarModel#normalize(PsiElement)}, but then
	 * for directories with no children it re-creates used {@link PsiElement} from {@link VirtualFile},
	 * which is normally handled by {@link PsiManagerImpl} and boom, wrong PsiJavaDirectoryImpl is back.
	 */
	public static final class MockPsiManager extends PsiManagerEx {

		private final Project myProject;
		PsiManagerImpl realManager;
		private MockFileManager myMockFileManager;
		private PsiModificationTrackerImpl myPsiModificationTracker;

		public MockPsiManager(@NotNull Project project) {
			this.myProject = project;
		}

		public MockPsiManager(@NotNull Project project, @NotNull PsiManagerImpl manager) {
			this(project);
			this.realManager = manager;
		}

		@Override
		public PsiDirectory findDirectory(@NotNull VirtualFile file) {
			if (file instanceof LdapNodeVirtualFile) {
				if (file.isDirectory()) {
					return new LdapNodePsiDirectory(realManager, file);
				}
			}
			return realManager.findDirectory(file);
		}

		@Override
		@NotNull
		public Project getProject() {
			return myProject;
		}

		@Override
		public PsiFile findFile(@NotNull VirtualFile file) {
			return null;
		}

		@Override
		@Nullable
		public FileViewProvider findViewProvider(@NotNull VirtualFile file) {
			return null;
		}

		@Override
		public boolean areElementsEquivalent(PsiElement element1, PsiElement element2) {
			return Comparing.equal(element1, element2);
		}

		@Override
		public void reloadFromDisk(@NotNull PsiFile file) {
		}

		@Override
		public void addPsiTreeChangeListener(@NotNull PsiTreeChangeListener listener) {
		}

		@Override
		public void addPsiTreeChangeListener(@NotNull PsiTreeChangeListener listener, @NotNull Disposable parentDisposable) {
		}

		@Override
		public void removePsiTreeChangeListener(@NotNull PsiTreeChangeListener listener) {
		}

		@Override
		@NotNull
		public PsiModificationTracker getModificationTracker() {
			if (myPsiModificationTracker == null) {
				myPsiModificationTracker = new PsiModificationTrackerImpl(myProject);
			}
			return myPsiModificationTracker;
		}

		@Override
		public void startBatchFilesProcessingMode() {
		}

		@Override
		public void finishBatchFilesProcessingMode() {
		}

		@Override
		public <T> T runInBatchFilesMode(@NotNull Computable<T> computable) {
			return null;
		}

		@Override
		public <T> T getUserData(@NotNull Key<T> key) {
			return null;
		}

		@Override
		public <T> void putUserData(@NotNull Key<T> key, T value) {
		}

		@Override
		public boolean isDisposed() {
			return false;
		}

		@Override
		public void dropResolveCaches() {
			getFileManager().cleanupForNextTest();
		}

		@Override
		public void dropPsiCaches() {
			dropResolveCaches();
		}

		@Override
		public boolean isInProject(@NotNull PsiElement element) {
			return false;
		}

		@Override
		public boolean isBatchFilesProcessingMode() {
			return false;
		}

		@Override
		public boolean isAssertOnFileLoading(@NotNull VirtualFile file) {
			return false;
		}

		@Override
		public void beforeChange(boolean isPhysical) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void afterChange(boolean isPhysical) {
			throw new UnsupportedOperationException();
		}

		@Override
		@NotNull
		public FileManager getFileManager() {
			if (myMockFileManager == null) {
				myMockFileManager = new MockFileManager(this);
			}
			return myMockFileManager;
		}

		@Override
		public void beforeChildRemoval(@NotNull final PsiTreeChangeEventImpl event) {
		}

		@Override
		public void beforeChildReplacement(@NotNull final PsiTreeChangeEventImpl event) {
		}

		@Override
		public void beforeChildAddition(@NotNull PsiTreeChangeEventImpl event) {
		}

		@Override
		public void setAssertOnFileLoadingFilter(@NotNull VirtualFileFilter filter, @NotNull Disposable parentDisposable) {

		}

	}

}
