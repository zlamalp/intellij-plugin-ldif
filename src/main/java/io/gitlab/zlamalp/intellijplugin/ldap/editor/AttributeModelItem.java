package io.gitlab.zlamalp.intellijplugin.ldap.editor;

import com.intellij.ui.ColoredTableCellRenderer;
import com.intellij.ui.SimpleTextAttributes;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.AttributeValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeType;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.constants.SchemaConstants;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.jetbrains.annotations.NotNull;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import java.util.Objects;

/**
 * This class represents single pair of Entry Attribute=Value.
 * For multi-valued Attributes, multiple instances exists with same Attribute and LdapNode but different Value.
 * Its used in AttributeTableModel for displaying LDAP Entry attributes and values.
 *
 * @see AttributeTableModel
 * @see LdapEntryEditor
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class AttributeModelItem {

	private LdapEntryTreeNode ldapNode;
	private AttributeValueProvider valueProvider;
	private boolean collapsed = false;
	private boolean isCollapsible = false;

	/**
	 * Table model wrapping original LDAP Attribute and its single value representing single row in table editor
	 *
	 * @param node LdapNode associated with this item
	 * @param attribute Attribute associated with this item
	 * @param value One of values of Attribute associated with this item
	 */
	public AttributeModelItem(@NotNull LdapEntryTreeNode node , @NotNull Attribute attribute, Value value) {
		this.ldapNode = node;
		this.valueProvider = LdapUtils.getLdapValueProvider(node, attribute.getUpId());
		this.valueProvider.setRawValue(value);
		// determine collapsible and collapsed state of model item
		int collapseThreshold = LdapUtils.getPreferredLdapSettings(ldapNode.getLdapServer().getSettings()).getWrapAttributesBy();
		if (value != null && Objects.equals(value, attribute.get()) && attribute.size() >= collapseThreshold && collapseThreshold > 0 && !isCellObjectClass()) {
			this.isCollapsible = true;
			this.collapsed = true;
		}
	}

	/**
	 * Get LdapNode (LDAP Entry) in which this Item is Attribute-Value pair.
	 *
	 * @return Associated LdapNode (Entry)
	 */
	public LdapEntryTreeNode getLdapNode() {
		return ldapNode;
	}

	/**
	 * Get proper ValueProvider associated with this Attribute and Value.
	 * It can be used to correctly get/set/convert attribute value.
	 *
	 * @return ValueProvider associated with this Attribute and Value.
	 */
	public AttributeValueProvider getValueProvider() {
		return valueProvider;
	}

	/**
	 * Get Attribute associated with this Item.
	 *
	 * @return Attribute associated with this Item.
	 */
	public Attribute getAttribute() {
		return valueProvider.getAttribute();
	}

	/**
	 * Get raw Value associated with this Item.
	 *
	 * @return Raw Value associated with this Item.
	 */
	public Value getRawValue() {
		return valueProvider.getRawValue();
	}

	/**
	 * Get String representation of Value associated with this Item.
	 *
	 * @return String representation of Value associated with this Item.
	 */
	public String getStringValue() {
		return valueProvider.getStringValue();
	}

	/**
	 * Render display value of attribute in passed renderer.
	 *
	 * @param renderer renderer to use to append content to
	 * @param style default style (usually plain, bold or italic from table cell renderer logic)
	 * @param raw TRUE if value should be rendered raw / FALSE if to render decorated/modified value
	 */
	void renderDisplayValue(ColoredTableCellRenderer renderer, SimpleTextAttributes style, boolean raw) {
		valueProvider.renderDisplayValue(renderer, style, raw);
	}

	/**
	 * Return TRUE if attribute is "objectClass" attribute
	 *
	 * @return TRUE if objectClass
	 */
	public boolean isCellObjectClass() {
		return SchemaConstants.OBJECT_CLASS_AT.equalsIgnoreCase(valueProvider.getAttribute().getId()) ||
				SchemaConstants.OBJECT_CLASS_AT_OID.equalsIgnoreCase(valueProvider.getAttribute().getId());
	}

	/**
	 * Return TRUE if attribute is "userPassword" attribute
	 *
	 * @return TRUE if userPassword
	 */
	public boolean isCellUserPassword() {
		return SchemaConstants.USER_PASSWORD_AT.equalsIgnoreCase(valueProvider.getAttribute().getId()) ||
				SchemaConstants.USER_PASSWORD_AT_OID.equalsIgnoreCase(valueProvider.getAttribute().getId());
	}

	/**
	 * Is attribute leading part of Entry DN ? If so, we want to disable editing and user must use "RenameAction"
	 *
	 * @return TRUE if is attribute part of DN
	 */
	public boolean isCellPartOfDN() {
		try {
			LdapName name = new LdapName(ldapNode.getDn().toString());
			// entry has no DN
			if (name.getRdns().size() == 0) return false;
			// entry has DN
			return valueProvider.getAttribute().getId().equalsIgnoreCase(name.getRdn(name.getRdns().size()-1).getType());
		} catch (InvalidNameException ex) {
			return false;
		}
	}

	/**
	 * Return TRUE if attribute is MUST by any of Entry objectClass schema
	 *
	 * @return TRUE if MUST
	 */
	public boolean isMustAttribute() {

		if (!SchemaUtils.isMustAttribute(ldapNode, valueProvider.getAttribute().getId())) {
			// fallback in case of schema is wrong
			return isCellObjectClass() || isCellPartOfDN();
		}
		return true;

	}

	/**
	 * Return TRUE if attribute is OPERATIONAL
	 *
	 * @return TRUE if MUST
	 */
	public boolean isOperationAttribute() {

		if (!SchemaUtils.isOperationalAttribute(ldapNode.getLdapServer().getSchema(), valueProvider.getAttribute().getId())) {

			AttributeType type = SchemaUtils.getAttributeType(ldapNode, valueProvider.getAttribute());
			if (type != null) {
				return type.isOperational();
			}

		} else {
			// by our parser
			return true;
		}
		// unknown
		return false;

	}

	/**
	 * Return TRUE if attribute value is allowed for modification
	 *
	 * @return TRUE if modifiable by user
	 */
	public boolean isUserModifiable() {

		AttributeType type = SchemaUtils.getAttributeType(ldapNode, valueProvider.getAttribute());
		if (type != null) {
			return type.isUserModifiable();
		} else {
			// unknown - we assume it is
			return true;
		}

	}

	/**
	 * Is EDITOR or Connection read only ? If so we want to disable editing
	 *
	 * @return TRUE if attribute or connection is read only connection
	 */
	public boolean isReadOnly() {
		if (ldapNode.getEditor() != null) {
			return ldapNode.getEditor().isLocallyReadOnly();
		}
		return ldapNode.getLdapServer().isReadOnly();
	}

	/**
	 * Is attribute of ROOT_DSE attribute ? If so, we want to disable editing
	 *
	 * @return TRUE if is attribute of ROOT_DSE attribute
	 */
	public boolean isRootNode() {
		return Objects.equals(ldapNode.getNodeType(), LdapNodeType.ROOT_DSE);
	}

	/**
	 * Return true if attribute is known to be single valued
	 *
	 * @return TRUE if its known to be single valued
	 */
	public boolean isSingleValued() {

		AttributeType type = SchemaUtils.getAttributeType(ldapNode, valueProvider.getAttribute());
		if (type != null) {
			return type.isSingleValued();
		} else {
			// we don't know, probably not single valued
			return false;
		}

	}

	/**
	 * Return TRUE if attribute values are collapsed to single row
	 *
	 * @return TRUE if collapsed
	 */
	public boolean isCollapsed() {
		return collapsed;
	}

	/**
	 * Set collapsed state to attribute
	 *
	 * @param collapsed TRUE for collapsed
	 */
	public void setCollapsed(boolean collapsed) {
		this.collapsed = collapsed;
	}

	/**
	 * Return TRUE if attribute is collapsible
	 *
	 * @return TRUE if collapsible
	 */
	public boolean isCollapsible() {
		return isCollapsible;
	}

	/**
	 * Set collapsible state to attribute
	 *
	 * @param collapsible TRUE for collapsible
	 */
	public void setCollapsible(boolean collapsible) {
		isCollapsible = collapsible;
	}

}
