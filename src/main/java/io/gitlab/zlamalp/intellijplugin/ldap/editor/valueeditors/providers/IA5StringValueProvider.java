package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers;

import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.IA5StringValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.constants.SchemaConstants;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;

/**
 * Basic IA5String value editor for LDAP Entry attribute values.
 * Only HumanReadable (non-binary) attributes can be used with this editor.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class IA5StringValueProvider extends AbstractValueProvider {

	private static Logger log = LoggerFactory.getLogger(IA5StringValueProvider.class);

	public IA5StringValueProvider(@NotNull LdapEntryTreeNode node, @NotNull Attribute attribute, Value value) {

		super(node, attribute, value);

		if (!attribute.isHumanReadable() && (value != null && !value.isHumanReadable())) {
			throw new IllegalArgumentException(attribute.getUpId() + " is not HumanReadable. You can't use IA5StringValueProvider to edit its value.");
		}

		AttributeType type = SchemaUtils.getAttributeType(node, attribute);
		if (type != null) {
			if (!SchemaConstants.IA5_STRING_SYNTAX.equals(type.getSyntaxOid())) {
				throw new IllegalArgumentException(attribute.getUpId() + " doesn't have IA5String syntax. You can't use IA5StringValueProvider to edit its value.");
			}
		}

	}

	@Override
	public AttributeValueEditor getValueEditor(Component component, boolean addingNewValue) {
		return new IA5StringValueEditor(component, this, addingNewValue);
	}

}
