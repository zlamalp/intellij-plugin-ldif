package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers;

import com.intellij.LdapBundle;
import com.intellij.openapi.editor.colors.EditorColorsManager;
import com.intellij.openapi.editor.colors.EditorColorsScheme;
import com.intellij.ui.ColoredTableCellRenderer;
import com.intellij.ui.JBColor;
import com.intellij.ui.SimpleTextAttributes;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.BinaryValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.intellij.images.util.ImageInfoReader;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.text.DecimalFormat;

/**
 * JPEG file value editor for LDAP Entry attribute values.
 * It provides custom rendering for table values.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class JpegValueProvider extends BinaryValueProvider {

	private static Logger log = LoggerFactory.getLogger(JpegValueProvider.class);

	public JpegValueProvider(@NotNull LdapEntryTreeNode node, @NotNull Attribute attribute, Value value) {
		super(node, attribute, value);
	}

	@Override
	public void renderDisplayValue(ColoredTableCellRenderer renderer, SimpleTextAttributes style, boolean raw) {

		if (getRawValue() == null ||getRawValue().isNull()) {
			renderer.append("", style, true);
		} else {
			if (!raw) {
				EditorColorsScheme scheme = EditorColorsManager.getInstance().getGlobalScheme();
				renderer.setFont(new Font(scheme.getEditorFontName(), Font.PLAIN, scheme.getEditorFontSize()));
				ImageInfoReader.Info info = ImageInfoReader.getInfo(getRawValue().getBytes());
				style = style.derive(style.getStyle() | SimpleTextAttributes.STYLE_ITALIC, JBColor.darkGray, new JBColor(0xeffae7, 0x49544a), null);
				renderer.append(((info != null) ? (info.width + "x" + info.height + ", ") : "") + LdapBundle.message("ldap.editor.table.renderer.binaryFile", "JPEG") + ", " + sizeToUnits(), style, true);
			} else {
				// fallback to standard renderer
				super.renderDisplayValue(renderer, style, raw);
			}
		}

	}

	@Override
	public AttributeValueEditor getValueEditor(Component component, boolean addingNewValue) {
		return new BinaryValueEditor(component, this, addingNewValue);
	}

	private String sizeToUnits() {

		long size = getRawValue().getBytes().length;
		if(size <= 0) return "0";
		final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
		int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
		return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];

	}

}
