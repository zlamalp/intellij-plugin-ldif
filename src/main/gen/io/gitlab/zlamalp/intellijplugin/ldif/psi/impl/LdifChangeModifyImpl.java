// This is a generated file. Not intended for manual editing.
package io.gitlab.zlamalp.intellijplugin.ldif.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.*;

public class LdifChangeModifyImpl extends ASTWrapperPsiElement implements LdifChangeModify {

  public LdifChangeModifyImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull LdifVisitor visitor) {
    visitor.visitChangeModify(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof LdifVisitor) accept((LdifVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<LdifChangeModifyAdd> getChangeModifyAddList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, LdifChangeModifyAdd.class);
  }

  @Override
  @NotNull
  public List<LdifChangeModifyDelete> getChangeModifyDeleteList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, LdifChangeModifyDelete.class);
  }

  @Override
  @NotNull
  public List<LdifChangeModifyReplace> getChangeModifyReplaceList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, LdifChangeModifyReplace.class);
  }

}
