package io.gitlab.zlamalp.intellijplugin.ldap.ui;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogEarthquakeShaker;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.openapi.util.registry.Registry;
import com.intellij.openapi.wm.IdeFocusManager;
import com.intellij.ui.DocumentAdapter;
import com.intellij.ui.components.JBCheckBox;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBTextField;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.exception.LdapInvalidDnException;
import org.apache.directory.api.ldap.model.name.Dn;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import java.util.List;

/**
 * Dialog window allowing user to rename LDAP Entry ({@see LdapTreeNode}). Provided data are validated.
 *
 * Rename operation itself must be performed outside of this dialog {@see io.gitlab.zlamalp.intellijplugin.ldap.actions.RenameEntryAction}
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class RenameEntryDialog extends DialogWrapper {

	private JPanel wrapper;
	private JBLabel currentDn;
	private JBLabel restOfDn;
	private JBTextField newNameField;
	private JBCheckBox deleteOldCheckBox;
	private LdapEntryTreeNode node;
	private boolean initialized = false;

	/**
	 * Create dialog for renaming Entry in LDAP.
	 *
	 * @param project Project this action is associated with
	 * @param canBeParent whether dialog window can be parent
	 * @param node LdapTreeNode to perform rename for
	 */
	public RenameEntryDialog(@Nullable Project project, boolean canBeParent, @NotNull LdapEntryTreeNode node) {
		super(project, canBeParent);
		this.node = node;
		init();
	}

	@Nullable
	@Override
	protected JComponent createCenterPanel() {
		if (!initialized) {
			initialize();
		}
		return wrapper;
	}

	@Nullable
	@Override
	public JComponent getPreferredFocusedComponent() {
		return newNameField;
	}

	@Nullable
	@Override
	protected ValidationInfo doValidate() {

		if (newNameField.getText() == null || newNameField.getText().isEmpty()) {
			return new ValidationInfo("Invalid DN format! New name can't be empty.", newNameField);
		}
		if (newNameField.getText().matches("(.*)=$")) {
			return new ValidationInfo("Invalid DN format!", newNameField);
		}

		try {
			Dn dn = new Dn(newNameField.getText()+","+restOfDn.getText());
		} catch (LdapInvalidDnException e) {
			return new ValidationInfo("Invalid DN format!", newNameField);
		}
		return super.doValidate();

	}

	/**
	 * init the form
	 */
	private void initialize() {

		setTitle("Rename '"+node.getDn()+"'");

		currentDn.setText(node.getDn().toString());
		newNameField.setText(node.getRdn());
		newNameField.setColumns(node.getRdn().length()+3);

		setOKButtonText("Rename");

		newNameField.getDocument().addDocumentListener(new DocumentAdapter() {
			@Override
			protected void textChanged(DocumentEvent documentEvent) {
				triggerValidation();
			}
		});

		try {
			restOfDn.setText(node.getDn().getAncestorOf(node.getRdn()).toString());
		} catch (LdapInvalidDnException ex) {
			LdapNotificationHandler.handleError(ex, "LDAP: "+node.getDn(), "Invalid DN passed to Rename Entry dialog.");
		}

	}

	/**
	 * Manually triggers validation of the dialog wrapper
	 */
	private void triggerValidation() {

		List<ValidationInfo> infoList = doValidateAll();
		if (!infoList.isEmpty()) {
			ValidationInfo info = infoList.get(0);
			if (info.component != null && info.component.isVisible()) {
				IdeFocusManager.getInstance(null).requestFocus(info.component, true);
			}

			if (!Registry.is("ide.inplace.validation.tooltip")) {
				DialogEarthquakeShaker.shake(getPeer().getWindow());
			}

			startTrackingValidation();
			if(infoList.stream().anyMatch(info1 -> !info1.okEnabled)) return;
		}

	}

	/**
	 * Get new name for the Entry or NULL if input was invalid
	 *
	 * @return new Entry DN or NULL
	 */
	public Dn getNewDn() {
		try {
			return new Dn(newNameField.getText()+","+restOfDn.getText());
		} catch (LdapInvalidDnException e) {
			// FIXME failed
		}
		return null;
	}

	/**
	 * Whether user wish to delete old RDN (default = TRUE)
	 *
	 * @return TRUE = delete old RND on rename / FALSE keep old RND on rename
	 */
	public boolean isDeleteOld() {
		return deleteOldCheckBox.isSelected();
	}

}
