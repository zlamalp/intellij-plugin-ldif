package io.gitlab.zlamalp.intellijplugin.ldap.schema;

import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTable;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.apache.directory.api.ldap.model.schema.LdapSyntax;
import org.apache.directory.api.ldap.model.schema.MatchingRule;
import org.apache.directory.api.ldap.model.schema.MatchingRuleUse;
import org.apache.directory.api.ldap.model.schema.ObjectClass;
import org.apache.directory.api.ldap.model.schema.SchemaManager;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.OidSyntaxChecker;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Representation of LDAP Schema internally used and stored within each LDAP server connection.
 *
 * @see LdapServer
 *
 * @author Pavel Zlámal
 */
public class LdapSchema {

	private static Logger log = LoggerFactory.getLogger(LdapSchema.class);
	private static OidSyntaxChecker oidSyntaxChecker = OidSyntaxChecker.INSTANCE;

	/**
	 * UUID of LdapServerConnection this schema is associated with
	 */
	private String uuid;

	private SchemaManager schemaManager;

	// schema attributes
	private Map<String, ObjectClass> objectClasses = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, AttributeType> attributeTypes = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, LdapSyntax> ldapSyntaxes = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, MatchingRule> matchingRules = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
	private Map<String, MatchingRuleUse> matchingRuleUse = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
	// TODO - ActiveDirectory schema elements support
	private List<String> dITStructureRules = new ArrayList<>();
	private List<String> dITContentRules = new ArrayList<>();
	private List<String> nameForms = new ArrayList<>();

	// Caches
	private Set<String> attributeTypeNames = new HashSet<>();
	private Set<String> operationalAttributes = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

	/**
	 * Attribute values annotations - used to annotate values in table (mostly translate OID values)
	 *
	 * @see AttributeTable (cell renderer)
	 */
	private transient static Map<String, Map<String, String>> attrValueAnnotation = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

	static {

		// initialize attribute value annotations
		// use lowercased attr names to be sure

		// openLDAP attributes
		attrValueAnnotation.put("supportedcontrol", new HashMap<>());
		attrValueAnnotation.put("supportedextension", new HashMap<>());
		attrValueAnnotation.put("supportedfeatures", new HashMap<>());

		attrValueAnnotation.get("supportedcontrol").put("2.16.840.1.113730.3.4.18", "Proxied Authorization v.2 RFC 4370");
		attrValueAnnotation.get("supportedcontrol").put("2.16.840.1.113730.3.4.2", "ManageDsaIT RFC 3377");
		attrValueAnnotation.get("supportedcontrol").put("1.3.6.1.4.1.4203.1.10.1", "Subentries RFC 3673");
		attrValueAnnotation.get("supportedcontrol").put("1.2.826.0.1.3344810.2.3", "Matched Values Control RFC 3876");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.319", "Simple Paged Results RFC 2696");
		attrValueAnnotation.get("supportedcontrol").put("1.3.6.1.1.12", "Assertion Control RFC 4528");
		attrValueAnnotation.get("supportedcontrol").put("1.3.6.1.1.13.1", "LDAP Pre-read Control RFC 4527");
		attrValueAnnotation.get("supportedcontrol").put("1.3.6.1.1.13.2", "LDAP Post-read Control RFC 4527");
		attrValueAnnotation.get("supportedcontrol").put("1.3.6.1.4.1.4203.1.9.1.1", "Content Synchronization Operation RFC 4530");
		attrValueAnnotation.get("supportedcontrol").put("1.3.6.1.1.22", "Don't Use Copy RFC 6171");

		attrValueAnnotation.get("supportedextension").put("1.3.6.1.1.8", "Cancel Operation RFC 3909");
		attrValueAnnotation.get("supportedextension").put("1.3.6.1.4.1.4203.1.11.1", "Modify Password RFC 3088");
		attrValueAnnotation.get("supportedextension").put("1.3.6.1.4.1.4203.1.11.3", "Who am I? RFC 4532");

		attrValueAnnotation.get("supportedfeatures").put("1.3.6.1.1.14", "Modify-Increment RFC 4525");
		attrValueAnnotation.get("supportedfeatures").put("1.3.6.1.4.1.4203.1.5.1", "All Operational Attributes RFC 3674");
		attrValueAnnotation.get("supportedfeatures").put("1.3.6.1.4.1.4203.1.5.2", "Requesting Attributes by Object Class RFC 4529");
		attrValueAnnotation.get("supportedfeatures").put("1.3.6.1.4.1.4203.1.5.3", "True/False filters RFC 4526");
		attrValueAnnotation.get("supportedfeatures").put("1.3.6.1.4.1.4203.1.5.4", "Language Tag Options RFC 3866");
		attrValueAnnotation.get("supportedfeatures").put("1.3.6.1.4.1.4203.1.5.5", "Language Range Options RFC 3866");

		// MS AD related attributes
		attrValueAnnotation.put("supportedcapabilities", new HashMap<>());

		attrValueAnnotation.get("supportedcapabilities").put("1.2.840.113556.1.4.1670", "Active Directory V51: Windows Server® 2003");
		attrValueAnnotation.get("supportedcapabilities").put("1.2.840.113556.1.4.1791", "Active Directory LDAP Integration: signing and sealing on an NTLM authenticated connection");
		attrValueAnnotation.get("supportedcapabilities").put("1.2.840.113556.1.4.1935", "Active Directory V60: Server® 2008");
		attrValueAnnotation.get("supportedcapabilities").put("1.2.840.113556.1.4.2080", "Active Directory V61R2: Windows Server® 2008 R2");
		attrValueAnnotation.get("supportedcapabilities").put("1.2.840.113556.1.4.800", "Active Directory");

		attrValueAnnotation.get("supportedextension").put("1.2.840.113556.1.4.1781", "Fast concurrent bind");
		attrValueAnnotation.get("supportedextension").put("1.3.6.1.4.1.1466.101.119.1", "Dynamic Refresh");
		attrValueAnnotation.get("supportedextension").put("1.3.6.1.4.1.1466.20037", "Start TLS");

		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.1338", "Verify name");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.1339", "Domain scope");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.1340", "Search options");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.1413", "RODC DCPROMO");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.1341", "Permissive modify");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.1504", "Attribute scoped query");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.1852", "Quota");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.1907", "Shutdown notify");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.1948", "Range retrieval no error");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.1974", "Force update");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.2026", "Input DN");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.2064", "Show recycled");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.2065", "Show deactivated link");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.319", "Simple Paged Results");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.417", "Show deleted");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.473", "Server Side Sorting");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.474", "Server Side Sorting response");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.521", "Cross-domain move");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.528", "Server search notification");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.529", "Extended DN");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.619", "Lazy commit");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.801", "Security descriptor flags");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.802", "Range property");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.805", "Tree delete");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.841", "Directory synchronization");
		attrValueAnnotation.get("supportedcontrol").put("1.2.840.113556.1.4.970", "Get stats");
		attrValueAnnotation.get("supportedcontrol").put("2.16.840.1.113730.3.4.10", "VLV Response");
		attrValueAnnotation.get("supportedcontrol").put("2.16.840.1.113730.3.4.9", "VLV Request");

	}

	/**
	 * Create schema for specified LDAP server connection and LDAP schema Entry.
	 *
	 * @param uuid LdapServerConnection UUID this schema is associated with
	 * @param schemaManager SchemaManager to get fallback schema values from
	 * @param schemaAttributes Attributes to use to init schema
	 */
	public LdapSchema(String uuid, SchemaManager schemaManager, Entry schemaAttributes) {

		this.uuid = uuid;
		this.schemaManager = schemaManager;

		for (LdapSyntax syntax : LdapSchemaParser.parseLdapSyntaxes(schemaAttributes)) {
			storeLdapSyntax(syntax);
		}

		for (MatchingRule rule : LdapSchemaParser.parseMatchingRule(schemaAttributes)) {
			// fill Syntax object by source OID
			if (rule.getSyntaxOid() != null) {
				if (ldapSyntaxes.containsKey(rule.getSyntaxOid())) {
					rule.setSyntax(ldapSyntaxes.get(rule.getSyntaxOid()));
				} else {
					// fallback to Apache Directory Schema and fill it back to known syntaxes
					try {
						LdapSyntax foundSyntax = findLdapSyntaxInSchemaManager(rule.getSyntaxOid());
						rule.setSyntax(foundSyntax);
						storeLdapSyntax(foundSyntax);
					} catch (LdapException e) {
						log.error("LdapSyntax not found by OID/NAME: {} for MatchingRule: {}/{}", rule.getSyntaxOid(), rule.getOid(), rule.getName());
					}
				}
			}
			storeMatchingRule(rule);
		}

		for (AttributeType attributeType : LdapSchemaParser.parseAttributeTypes(schemaAttributes)) {

			if (attributeType.getSyntaxOid() != null) {
				// syntax length is part of attribute type and not modified
				if (ldapSyntaxes.containsKey(attributeType.getSyntaxOid())) {
					attributeType.setSyntax(ldapSyntaxes.get(attributeType.getSyntaxOid()));
				} else {
					// fallback to Apache Directory Schema and fill it back to known syntaxes
					try {
						LdapSyntax foundSyntax = findLdapSyntaxInSchemaManager(attributeType.getSyntaxOid());
						attributeType.setSyntax(foundSyntax);
						storeLdapSyntax(foundSyntax);
					} catch (LdapException e) {
						log.error("LdapSyntax not found by OID/NAME: {} for AttributeType: {}/{}", attributeType.getSyntaxOid(), attributeType.getOid(), attributeType.getName());
					}
				}
			}
			if (attributeType.getEqualityOid() != null) {
				if (matchingRules.containsKey(attributeType.getEqualityOid())) {
					attributeType.setEquality(matchingRules.get(attributeType.getEqualityOid()));
				} else {
					// fallback to Apache Directory Schema and fill it back to known matching rules
					try {
						MatchingRule foundMR = findMatchingRuleInSchemaManager(attributeType.getEqualityOid());
						attributeType.setEquality(foundMR);
						storeMatchingRule(foundMR);
					} catch (LdapException e) {
						log.error("EQUALITY MatchingRule not found by OID/NAME: {} for AttributeType: {}/{}", attributeType.getEqualityOid(), attributeType.getOid(), attributeType.getName());
					}
				}
			}
			if (attributeType.getSubstringOid() != null) {
				if (matchingRules.containsKey(attributeType.getSubstringOid())) {
					attributeType.setSubstring(matchingRules.get(attributeType.getSubstringOid()));
				} else {
					// fallback to Apache Directory Schema and fill it back to known matching rules
					try {
						MatchingRule foundMR = findMatchingRuleInSchemaManager(attributeType.getSubstringOid());
						attributeType.setSubstring(foundMR);
						storeMatchingRule(foundMR);
					} catch (LdapException e) {
						log.error("SUBSTRING MatchingRule not found by OID/NAME: {} for AttributeType: {}/{}", attributeType.getSubstringOid(), attributeType.getOid(), attributeType.getName());
					}
				}
			}
			if (attributeType.getOrderingOid() != null) {
				if (matchingRules.containsKey(attributeType.getOrderingOid())) {
					attributeType.setOrdering(matchingRules.get(attributeType.getOrderingOid()));
				} else {
					// fallback to Apache Directory Schema and fill it back to known matching rules
					try {
						MatchingRule foundMR = findMatchingRuleInSchemaManager(attributeType.getOrderingOid());
						attributeType.setOrdering(foundMR);
						storeMatchingRule(foundMR);
					} catch (LdapException e) {
						log.error("ORDERING MatchingRule not found by OID/NAME: {} for AttributeType: {}/{}", attributeType.getOrderingOid(), attributeType.getOid(), attributeType.getName());
					}
				}
			}
			storeAttributeType(attributeType);
		}

		for (MatchingRuleUse ruleUse : LdapSchemaParser.parseMatchingRuleUse(schemaAttributes)) {
			// fill applicable attribute types by source OIDs
			if (ruleUse.getApplicableAttributeOids() != null) {
				List<AttributeType> applicableTypes = new ArrayList<>();
				for (String oidOrName : ruleUse.getApplicableAttributeOids()) {
					if (attributeTypes.containsKey(oidOrName)) {
						applicableTypes.add(attributeTypes.get(oidOrName));
					} else {
						// TODO - load types from apache dir schema manager ??
						log.error("AttributeType not found by OID/NAME: {} for MatchingRuleUse: {}/{}", oidOrName, ruleUse.getOid(), ruleUse.getName());
					}
				}
				if (!applicableTypes.isEmpty()) {
					for (AttributeType type : applicableTypes) {
						ruleUse.addApplicableAttribute(type);
					}
				}
			}
			storeMatchingRuleUse(ruleUse);
		}

		for (ObjectClass objectClass : LdapSchemaParser.parseObjectClasses(schemaAttributes)) {

			List<AttributeType> mustAttributes = new ArrayList<>();
			for (String oidOrName : objectClass.getMustAttributeTypeOids()) {
				if (attributeTypes.containsKey(oidOrName)) {
					mustAttributes.add(attributeTypes.get(oidOrName));
				} else {
					// fallback to Apache Directory Schema and fill it back to known attribute types
					try {
						AttributeType foundAttributeType = findAttributeTypeInSchemaManager(oidOrName);
						mustAttributes.add(foundAttributeType);
						storeAttributeType(foundAttributeType);
					} catch (LdapException e) {
						log.error("MUST AttributeType not found by OID/NAME: {} for ObjectClass: {}/{}", oidOrName, objectClass.getOid(), objectClass.getName(), e);
					}
				}
			}
			if (!mustAttributes.isEmpty()) objectClass.setMustAttributeTypes(mustAttributes);

			List<AttributeType> mayAttributes = new ArrayList<>();
			for (String oidOrName : objectClass.getMayAttributeTypeOids()) {
				if (attributeTypes.containsKey(oidOrName)) {
					mayAttributes.add(attributeTypes.get(oidOrName));
				} else {
					// fallback to Apache Directory Schema and fill it back to known attribute types
					try {
						AttributeType foundAttributeType = findAttributeTypeInSchemaManager(oidOrName);
						mayAttributes.add(foundAttributeType);
						storeAttributeType(foundAttributeType);
					} catch (LdapException e) {
						log.error("MAY AttributeType not found by OID/NAME: {} for ObjectClass: {}/{}", oidOrName, objectClass.getOid(), objectClass.getName(), e);
					}
				}
			}
			if (!mayAttributes.isEmpty()) objectClass.setMayAttributeTypes(mayAttributes);

			storeObjectClass(objectClass);
		}

		// refresh objectClass superiors
		for (ObjectClass objectClass : objectClasses.values()) {
			// fill only those without superiors since values in map are not unique
			if (objectClass.getSuperiors().isEmpty()) {

				List<ObjectClass> superiors = new ArrayList<>();
				for (String oidOrName : objectClass.getSuperiorOids()) {
					if (objectClasses.containsKey(oidOrName)) {
						superiors.add(objectClasses.get(oidOrName));
					} else {
						log.error("Superior ObjectClass not found by OID/NAME: {} for ObjectClass: {}/{}", oidOrName, objectClass.getOid(), objectClass.getName());
					}
				}
				if (!superiors.isEmpty()) {
					objectClass.setSuperiors(superiors);
				}
			}
		}

		// refresh attributeType superiors
		for (AttributeType attributeType : attributeTypes.values()) {
			// fill only those without superiors since values in map are not unique
			if (attributeType.getSuperiorOid() != null && attributeType.getSuperior() == null) {
				AttributeType superiorType = attributeTypes.getOrDefault(attributeType.getSuperiorOid(), null);
				if (superiorType == null) {
					log.error("Superior AttributeType not found by OID/NAME: {} for AttributeType: {}/{}", attributeType.getSuperiorOid(), attributeType.getOid(), attributeType.getName());
					continue;
				}
				attributeType.setSuperior(superiorType);
			}
		}

		// refresh attributeType params based on all known superiors
		for (AttributeType attributeType : attributeTypes.values()) {
			fillAttributeTypeFromSuperior(attributeType, attributeType.getSuperior());
		}

		// TODO - ActiveDirectory support -> handle rest of these attributes as objects !!!
		if (schemaAttributes != null) {
			dITStructureRules = getValuesOfSchemaAttribute(schemaAttributes.get("dITStructureRules"));
			dITContentRules = getValuesOfSchemaAttribute(schemaAttributes.get("dITContentRules"));
			nameForms = getValuesOfSchemaAttribute(schemaAttributes.get("nameForms"));
		}

	}

	private LdapSyntax findLdapSyntaxInSchemaManager(String oidOrName) throws LdapException {
		if (oidSyntaxChecker.isValidSyntax(oidOrName)) {
			return schemaManager.getRegistries().getLdapSyntaxRegistry().lookup(oidOrName);
		} else {
			String oid = schemaManager.getRegistries().getLdapSyntaxRegistry().getOidByName(oidOrName);
			return schemaManager.getRegistries().getLdapSyntaxRegistry().lookup(oid);
		}
	}

	private MatchingRule findMatchingRuleInSchemaManager(String oidOrName) throws LdapException {
		if (oidSyntaxChecker.isValidSyntax(oidOrName)) {
			return schemaManager.getRegistries().getMatchingRuleRegistry().lookup(oidOrName);
		} else {
			String oid = schemaManager.getRegistries().getMatchingRuleRegistry().getOidByName(oidOrName);
			return schemaManager.getRegistries().getMatchingRuleRegistry().lookup(oid);
		}
	}

	private AttributeType findAttributeTypeInSchemaManager(String oidOrName) throws LdapException {
		if (oidSyntaxChecker.isValidSyntax(oidOrName)) {
			return schemaManager.getRegistries().getAttributeTypeRegistry().lookup(oidOrName);
		} else {
			String oid = schemaManager.getRegistries().getAttributeTypeRegistry().getOidByName(oidOrName);
			return schemaManager.getRegistries().getAttributeTypeRegistry().lookup(oid);
		}
	}

	private void storeLdapSyntax(@NotNull LdapSyntax syntax) {
		// store by oid - syntaxes doesn't have names, just descriptions
		ldapSyntaxes.put(syntax.getOid(), syntax);
	}

	private void storeMatchingRule(@NotNull MatchingRule matchingRule) {
		// store by oid
		matchingRules.put(matchingRule.getOid(), matchingRule);
		// store by all names
		for (String name : matchingRule.getNames()) {
			matchingRules.put(name, matchingRule);
		}
	}

	private void storeMatchingRuleUse(@NotNull MatchingRuleUse matchingRule) {
		// store by oid
		matchingRuleUse.put(matchingRule.getOid(), matchingRule);
		// store by all names
		for (String name : matchingRule.getNames()) {
			matchingRuleUse.put(name, matchingRule);
		}
	}

	private void storeAttributeType(@NotNull AttributeType attributeType) {
		// store by oid
		attributeTypes.put(attributeType.getOid(), attributeType);
		// store by all names
		for (String name : attributeType.getNames()) {
			attributeTypes.put(name, attributeType);
			// fill cache with names
			attributeTypeNames.add(name);
		}
	}

	private void storeObjectClass(@NotNull ObjectClass objectClass) {
		// store by oid
		objectClasses.put(objectClass.getOid(), objectClass);
		// store by all names
		for (String name : objectClass.getNames()) {
			objectClasses.put(name, objectClass);
		}
	}

	/**
	 * Fill AttributeType properties (syntax/equality/substring/ordering) from its superior type.
	 * Whole type hierarchy is resolved and only when AttributeType has empty property, then it is
	 * replaced by value from superior (might be empty too).
	 *
	 * @param typeToFill AttributeType to be filled from superior type
	 * @param superiorType Superior AttributeType to get properties from
	 */
	private void fillAttributeTypeFromSuperior(AttributeType typeToFill, AttributeType superiorType) {

		// no superior - quick exit
		if (superiorType == null) return;

		if (typeToFill.getSyntax() == null && superiorType.getSyntax() != null) {
			typeToFill.setSyntax(superiorType.getSyntax());
		}
		if (typeToFill.getSyntaxLength() == 0 && superiorType.getSyntaxLength() > 0) {
			typeToFill.setSyntaxLength(superiorType.getSyntaxLength());
		}
		if (typeToFill.getEquality() == null && superiorType.getEquality() != null) {
			typeToFill.setEquality(superiorType.getEquality());
		}
		if (typeToFill.getSubstring() == null && superiorType.getSubstring() != null) {
			typeToFill.setSubstring(superiorType.getSubstring());
		}
		if (typeToFill.getOrdering() == null && superiorType.getOrdering() != null) {
			typeToFill.setOrdering(superiorType.getOrdering());
		}

		// FIXME - are other properties inherited too ??

		// climb attribute type hierarchy
		fillAttributeTypeFromSuperior(typeToFill, superiorType.getSuperior());

	}


	// ------------- STANDARD METHODS -----------------------------


	/**
	 * Return UUID of associated LDAP Server Connection.
	 *
	 * @return UUID of LdapServerConnection this schema is associated with
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * Set UUID of associated LDAP Server Connection.
	 *
	 * @param uuid UUID of LdapServerConnection this schema should be associated with
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Map<String, ObjectClass> getObjectClasses() {
		return objectClasses;
	}

	public void setObjectClasses(Map<String, ObjectClass> objectClasses) {
		this.objectClasses = objectClasses;
	}

	public Map<String, LdapSyntax> getLdapSyntaxes() {
		return ldapSyntaxes;
	}

	public void setLdapSyntaxes(Map<String, LdapSyntax> ldapSyntaxes) {
		this.ldapSyntaxes = ldapSyntaxes;
	}

	public Map<String, AttributeType> getAttributeTypes() {
		return attributeTypes;
	}

	public void setAttributeTypes(Map<String, AttributeType> attributeTypes) {
		this.attributeTypes = attributeTypes;
	}

	public Map<String, MatchingRule> getMatchingRules() {
		return matchingRules;
	}

	public void setMatchingRules(Map<String, MatchingRule> matchingRules) {
		this.matchingRules = matchingRules;
	}

	public Map<String, MatchingRuleUse> getMatchingRuleUse() {
		return matchingRuleUse;
	}

	public void setMatchingRuleUse(Map<String, MatchingRuleUse> matchingRuleUse) {
		this.matchingRuleUse = matchingRuleUse;
	}

	public List<String> getdITStructureRules() {
		return dITStructureRules;
	}

	public void setdITStructureRules(List<String> dITStructureRules) {
		this.dITStructureRules = dITStructureRules;
	}

	public List<String> getdITContentRules() {
		return dITContentRules;
	}

	public void setdITContentRules(List<String> dITContentRules) {
		this.dITContentRules = dITContentRules;
	}

	public List<String> getNameForms() {
		return nameForms;
	}

	public void setNameForms(List<String> nameForms) {
		this.nameForms = nameForms;
	}

	public Set<String> getAttributeTypeNames() {
		return attributeTypeNames;
	}

	public void setAttributeTypeNames(Set<String> attributeTypeNames) {
		this.attributeTypeNames = attributeTypeNames;
	}

	/**
	 * Return attribute value annotation if present or null
	 *
	 * @param attributeName Name of attribute to ask for annotation
	 * @return Attribute annotation or null
	 */
	public String getAnnotation(@NotNull String attributeName, @NotNull String attributeValue) {

		if (attrValueAnnotation.get(attributeName) != null) {
			return attrValueAnnotation.get(attributeName).get(attributeValue);
		}
		return null;
	}

	public SchemaManager getSchemaManager() {
		return this.schemaManager;
	}

	/**
	 * Return schema attribute values as list of strings
	 *
	 * @param attribute Attribute to get values from
	 * @return List of Attribute values as strings
	 */
	private List<String> getValuesOfSchemaAttribute(Attribute attribute) {

		List<String> result = new ArrayList<String>();
		if (attribute == null) return result;
		for (Value val : attribute) {
			result.add(val.getString());
		}
		return result;

	}

	private String printList(List<String> list) {

		if (list == null) return "[]";
		StringJoiner joiner = new StringJoiner("\n");
		for (String item : list) {
			joiner.add(item);
		}
		return "[" + joiner.toString() + "]";

	}

	private String printMapValueToRows(Map<String,?> sourceMap) {

		if (sourceMap == null || sourceMap.isEmpty()) return "[]";
		StringJoiner joiner = new StringJoiner("\n");
		for (Map.Entry<String,?> entry : sourceMap.entrySet()) {
			joiner.add(entry.getValue().toString());
		}
		return "[\n" + joiner.toString() + "\n]";

	}

	@Override
	public String toString() {
		return "LdapSchema:[" +
				"\nobjectClasses=" + printMapValueToRows(objectClasses) +
				", \nattributeTypes=" + printMapValueToRows(attributeTypes)+
				", \nldapSyntaxes=" + printMapValueToRows(ldapSyntaxes) +
				", \nmatchingRules=" + printMapValueToRows(matchingRules) +
				", \nmatchingRuleUse=" + printMapValueToRows(matchingRuleUse) +
				", \ndITStructureRules=" + printList(dITStructureRules) +
				", \ndITContentRules=" + printList(dITContentRules) +
				", \nnameForms=" + printList(nameForms) +
				"\n]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		LdapSchema that = (LdapSchema) o;
		return Objects.equals(uuid, that.uuid);
	}

	@Override
	public int hashCode() {
		return Objects.hash(uuid);
	}

}
