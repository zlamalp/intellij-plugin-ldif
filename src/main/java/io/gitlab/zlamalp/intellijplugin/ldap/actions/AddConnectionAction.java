package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.DumbAware;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.connection.ManageLdapServersDialog;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

/**
 * Opens {@link ManageLdapServersDialog} in the mode of adding new {@link LdapServer} connection.
 *
 * @author Pavel Zlámal
 */
public class AddConnectionAction extends AnAction implements DumbAware {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.addConnection";
	private static Logger log = LoggerFactory.getLogger(AddConnectionAction.class);

	public AddConnectionAction() {
	}

	public AddConnectionAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		if (LdapActionUtils.isFromTree(e)) {

			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			new ManageLdapServersDialog(e.getProject(), treePanel, new LdapServer()).show();
			// TODO - we might want to update LdapTreePanel and its model based on new state of LDAP connections

		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if AddConnection action should be active.
	 *
	 * @param e triggered event
	 * @return TRUE if action should be active
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		return LdapActionUtils.isFromTree(e);

	}

}
