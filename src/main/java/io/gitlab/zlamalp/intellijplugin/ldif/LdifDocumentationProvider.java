package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.lang.documentation.AbstractDocumentationProvider;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.Nullable;

/**
 * Provides documentation for LDIF elements.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifDocumentationProvider extends AbstractDocumentationProvider {

	@Nullable
	@Override
	public String getQuickNavigateInfo(PsiElement element, PsiElement originalElement) {
		System.out.println(element.getClass().getCanonicalName());
		return super.getQuickNavigateInfo(element, originalElement);
	}

	@Override
	public String generateDoc(PsiElement element, @Nullable PsiElement originalElement) {

		return super.generateDoc(element, originalElement);
	}

}
