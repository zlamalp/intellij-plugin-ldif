package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers;

import com.intellij.ui.ColoredTableCellRenderer;
import com.intellij.ui.SimpleColoredComponent;
import com.intellij.ui.SimpleTextAttributes;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.StringValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;

import static com.intellij.ui.SimpleTextAttributes.LINK_ATTRIBUTES;

/**
 * Basic OID value editor for LDAP Entry attribute values.
 * Only attributes with OID syntax can be used with it. If schema is unsure, then
 * attribute types of "supportedcontrol", "supportedextension", "supportedfeatures" and "supportedcapabilities"
 * are allowed.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class OidValueProvider extends AbstractValueProvider {

	private static Logger log = LoggerFactory.getLogger(OidValueProvider.class);

	public OidValueProvider(@NotNull LdapEntryTreeNode node, @NotNull Attribute attribute, Value value) {
		super(node, attribute, value);
		if (!LdapUtils.hasOidSyntax(attribute, node.getLdapServer().getSchema())) {
			throw new IllegalArgumentException("You can't use OidValueProvider to edit value of "+attribute.getUpId()+" attribute. It doesn't have OID syntax.");
		}

	}

	@Override
	public void renderDisplayValue(ColoredTableCellRenderer renderer, SimpleTextAttributes style, boolean raw) {

		renderer.append(getStringValue(), style, true);
		if (!raw) {
			// if possible, append OID -> name translation
			SimpleTextAttributes localStyle = style.derive(SimpleTextAttributes.REGULAR_ITALIC_ATTRIBUTES.getStyle() | style.getStyle(), null, null, null);
			if (node.getLdapServer().getSchema() != null) {
				String annotation = node.getLdapServer().getSchema().getAnnotation(getAttribute().getId(), getStringValue());
				if (annotation != null) {
					String[] split = annotation.split(" RFC ");
					if (split.length > 1) {
						renderer.append(" (" + split[0] + " ", localStyle);
						renderer.append("RFC "+ split[1],
								LINK_ATTRIBUTES.derive(localStyle.getStyle(), null, null, null),
								new SimpleColoredComponent.BrowserLauncherTag("https://www.ietf.org/rfc/rfc"+split[1]+".txt"));
						renderer.append(" )", localStyle);
					} else {
						renderer.append(" (" + annotation + ")", localStyle);
					}
				}
			}
		}

	}

	@Override
	public AttributeValueEditor getValueEditor(Component component, boolean addingNewValue) {
		return new StringValueEditor(component, this, addingNewValue);
	}

}
