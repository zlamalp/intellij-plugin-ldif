package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.LdapBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUIUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeType;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapServerTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.LdapNotificationHandler;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.MoveEntryDialog;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.util.Enumeration;
import java.util.Objects;

/**
 * Move selected LdapTreeNode in LdapTreePanel or Entry directly from opened editor.
 *
 * @see LdapTreePanel
 * @see LdapEntryTreeNode
 * @see LdapServerTreeNode
 *
 * @author Pavel Zlámal
 */
public class MoveEntryAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.move";
	private static Logger log = LoggerFactory.getLogger(MoveEntryAction.class);

	public MoveEntryAction() {
	}

	public MoveEntryAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);

		if (LdapActionUtils.isFromTree(e)) {

			LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class, null);
			if (selectedTreeNodes.length == 1) {
				performAction(treePanel, selectedTreeNodes[0]);
			}

		} else if (LdapActionUtils.isFromEditor(e)) {

			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null) {
				LdapEntryTreeNode node = editor.getLdapNode();
				performAction(treePanel, node);
				// TODO - maybe handle opened Editor - reopen it from new location ??
			}

		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if MoveEntry action should be active.
	 *
	 * @param e triggered event
	 * @return TRUE if action should be active
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class, null);
			return selectedTreeNodes.length == 1 &&
					Objects.equals(selectedTreeNodes[0].getNodeType(), LdapNodeType.NODE) &&
					((selectedTreeNodes[0].getEditor() != null) ?
					!selectedTreeNodes[0].getEditor().isLocallyReadOnly() :
					!selectedTreeNodes[0].getLdapServer().isReadOnly());
		} else if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null && !editor.isLoading()) {
				LdapEntryTreeNode node = editor.getLdapNode();
				if (node.getNodeType().equals(LdapNodeType.NODE)) {
					return (node.getEditor() != null) ? !node.getEditor().isLocallyReadOnly() : !node.getLdapServer().isReadOnly();
				}
			}
		}
		return false;

	}

	/**
	 * Perform Move Entry action on selected node
	 *
	 * @param treePanel LdapTreePanel to refresh nodes in
	 * @param node Node to refresh
	 */
	private void performAction(@NotNull LdapTreePanel treePanel, @NotNull LdapEntryTreeNode node) {

		MoveEntryDialog dialog = new MoveEntryDialog(treePanel.getProject(), true, node);
		if (dialog.showAndGet()) {

			Task task = new Task.Modal(treePanel.getProject(),
					LdapBundle.message("ldap.update.modal.moveEntry", node.getDn()),
					true) {

				Exception exception = null;
				LdapConnection connection = null;

				@Override
				public void run(@NotNull ProgressIndicator indicator) {

					try {

						indicator.setIndeterminate(false);
						indicator.setText(LdapBundle.message("ldap.update.modal.connecting"));
						indicator.setFraction(0.1);
						LdapUIUtils.waitIfUIDebug();
						indicator.checkCanceled();
						connection = node.getLdapServer().getConnection();

						indicator.setFraction(0.5);
						indicator.setText(LdapBundle.message("ldap.update.modal.movingEntry", dialog.getNewParentDn()));
						LdapUIUtils.waitIfUIDebug();
						indicator.checkCanceled();
						log.info("Moving {} to new parent {}", node.getDn(), dialog.getNewParentDn());
						connection.move(node.getDn().toString(), dialog.getNewParentDn());
						indicator.setFraction(1.0);

					} catch (LdapException ex) {
						exception = ex;
					}

				}

				@Override
				public void onFinished() {

					// close connection even if user canceled operation
					if (connection != null) {
						try {
							node.getLdapServer().releaseConnection(connection);
						} catch (LdapException e) {
							LdapNotificationHandler.handleError(e, node.getLdapServer().getName(), "Can't release connection.");
						}
					}

				}

				@Override
				public void onSuccess() {

					if (exception != null) {
						LdapNotificationHandler.handleError(exception, node.getLdapServer().getName(), "Can't move '"+node.getDn()+"' Entry to '"+dialog.getNewParentDn()+"'.");
					} else {

						LdapEntryTreeNode oldParent = (LdapEntryTreeNode)node.getParent();
						TreeNode oldRoot = node.getRoot();

						if (oldParent != null) {
							// update model locally, no need to refresh whole subtree
							treePanel.getModel().removeNodeFromParent(node);
						}

						// if "root" of original node wasn't the node itself (abandoned / removed from tree)
						if (oldRoot != node) {
							// check if new parent is present in the tree
							LdapEntryTreeNode newParentNode = find((DefaultMutableTreeNode)oldRoot, dialog.getNewParentDn());
							if (newParentNode != null) {
								// FIXME - we removed force "waiting" is it OK ?
								LdapUtils.refresh(newParentNode, LdapUtils.RefreshType.WITH_CHILDREN);
								treePanel.getModel().nodeStructureChanged(newParentNode);
							}

						}

						// TODO - moved entry is not loaded in tree yet, we should probably load it all the way to the moved Entry ?


					}

				}

			};
			ProgressManager.getInstance().run(task);

		}

	}

	/**
	 * Find proper root node based on DN, if not present NULL is returned
	 *
	 * @param root root node for search
	 * @param dn DN to search by
	 * @return Found LdapTreeNode or NULL
	 */
	private LdapEntryTreeNode find(DefaultMutableTreeNode root, @NotNull String dn) {
		Enumeration<TreeNode> e = root.depthFirstEnumeration();
		while (e.hasMoreElements()) {
			TreeNode node = e.nextElement();
			if (node instanceof LdapEntryTreeNode) {
				if (dn.equalsIgnoreCase(((LdapEntryTreeNode)node).getDn().toString())) {
					return (LdapEntryTreeNode)node;
				}
			}
		}
		return null;
	}

}
