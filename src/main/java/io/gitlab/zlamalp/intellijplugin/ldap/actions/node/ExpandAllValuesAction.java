package io.gitlab.zlamalp.intellijplugin.ldap.actions.node;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTable;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeModelItem;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTableModel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * An Action to which expands all collapsed values in attribute table in LdapEntryEditor.
 * Threshold for automatic values wrapping is configurable by plugin settings.
 *
 * @see LdapEntryEditor
 * @see AttributeTable
 * @see AttributeTableModel
 * @see AttributeModelItem
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class ExpandAllValuesAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.expandAllValues";

	public ExpandAllValuesAction() {
	}

	public ExpandAllValuesAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(AnActionEvent e) {

		if (!isActionActive(e)) return;

		LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
		if (editor != null) {
			for (AttributeModelItem item : editor.getModel().getItems()) {
				editor.getModel().getExpandedItems().add(item.getAttribute().getId());
			}
			editor.getModel().refresh();
		}

	}

	@Override
	public void update(AnActionEvent e) {

		e.getPresentation().setEnabled(isActionActive(e));

	}

	/**
	 * Return TRUE if ExpandAllValuesAction action should be active.
	 *
	 * @param e triggered event
	 * @return TRUE if action should be active
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null && !editor.isLoading()) {
				return LdapUtils.getPreferredLdapSettings(editor.getLdapNode().getLdapServer().getSettings()).getWrapAttributesBy()>0;
			}
		}
		return false;

	}

}
