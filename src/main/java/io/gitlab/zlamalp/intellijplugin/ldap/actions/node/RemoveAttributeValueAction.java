package io.gitlab.zlamalp.intellijplugin.ldap.actions.node;

import com.intellij.LdapBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.ui.Messages;
import com.intellij.util.PlatformIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUIUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeModelItem;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTable;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTableModel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.LdapNotificationHandler;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.DefaultModification;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Modification;
import org.apache.directory.api.ldap.model.entry.ModificationOperation;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

/**
 * Action which removes single value from Attribute.
 * Attribute and Value are selected in table of opened LdapEntryEditor, representing single LDAP Entry.
 *
 * @see LdapNodeVirtualFile
 * @see LdapEntryEditor
 * @see AttributeTable
 * @see AttributeTableModel
 *
 * @author Attila Majoros
 * @author Pavel Zlámal
 */
public class RemoveAttributeValueAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.removeValue";
	private static Logger log = LoggerFactory.getLogger(RemoveAttributeValueAction.class);

	public RemoveAttributeValueAction() {
	}

	public RemoveAttributeValueAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		// Cancel if attribute value can't be removed
		if (!isActionActive(e)) return;

		LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
		LdapEntryTreeNode node = editor.getLdapNode();
		AttributeModelItem selectedItem = editor.getModel().getItems().get(editor.getTable().convertRowIndexToModel(editor.getTable().getSelectedRow()));

		int result = Messages.showOkCancelDialog(editor.getComponent(),
				LdapBundle.message("action.ldap.RemoveAttributeValueAction.modal.text", selectedItem.getAttribute().getUpId()),
				LdapBundle.message("action.ldap.RemoveAttributeValueAction.modal.title" ),
				LdapBundle.message("action.ldap.RemoveAttributeValueAction.modal.ok" ),
				LdapBundle.message("action.ldap.RemoveAttributeValueAction.modal.cancel" ),
				PlatformIcons.DELETE_ICON);

		if (result == Messages.OK) {

			Attribute attribute = selectedItem.getAttribute();
			Value oldValue = selectedItem.getRawValue();

			// check if value can be removed locally
			if (!checkAndSetAttributeValue(node, node.getEntry().clone(), attribute.clone(), oldValue)) return;

			// we can remove

			if (editor.isDirectEdit()) {

				Task task = new Task.Modal(e.getProject(), LdapBundle.message("ldap.update.modal.modifyEntry", node.getDn()), true) {

					Exception exception = null;
					LdapConnection connection = null;

					@Override
					public void run(@NotNull ProgressIndicator indicator) {
						indicator.setIndeterminate(false);
						try {

							indicator.setText(LdapBundle.message("ldap.update.modal.connecting"));
							indicator.setFraction(0.1);
							LdapUIUtils.waitIfUIDebug();
							indicator.checkCanceled();
							connection = node.getLdapServer().getConnection();

							indicator.setFraction(0.5);
							indicator.setText(LdapBundle.message("ldap.update.modal.removingAttributeValue", attribute.getUpId()));
							LdapUIUtils.waitIfUIDebug();

							Modification removeMod = new DefaultModification(ModificationOperation.REMOVE_ATTRIBUTE, attribute.getUpId(), oldValue);
							indicator.checkCanceled();
							log.info("Removing value from attribute: {}, value: {}", attribute.getUpId(), oldValue);
							connection.modify(node.getDn(), removeMod);

							indicator.setFraction(1.0);

						} catch (LdapException ex) {
							exception = ex;
						}

					}


					@Override
					public void onSuccess() {

						if (exception != null) {

							// local model was not modified, so just show error
							LdapNotificationHandler.handleError(exception, node.getLdapServer().getName(), "Unable to remove value from Attribute '" + attribute.getUpId() + "'.");

						} else {

							// TODO - what is better - locally modify model or re-fetch whole entry from LDAP ?
							// LdapUtils.refresh(ldapNode);
							// update local model representation
							checkAndSetAttributeValue(node, node.getEntry(), attribute, oldValue);
							// we modified LDAP, make current state the last known
							editor.setLastState(node.getEntry());

						}

						editor.getModel().refresh();

					}

					@Override
					public void onFinished() {

						// close connection even if user canceled operation
						if (connection != null) {
							try {
								node.getLdapServer().releaseConnection(connection);
							} catch (LdapException e) {
								LdapNotificationHandler.handleError(e, node.getLdapServer().getName(), "Can't release connection.");
							}
						}

					}

				};
				ProgressManager.getInstance().run(task);

			} else {

				// MODIFY MODEL AND REFRESH VIEW
				// we had to modify local model before actually deciding "directEdit", so now just refresh
				checkAndSetAttributeValue(node, node.getEntry(), attribute, oldValue);
				editor.getModel().refresh();

			}

		}
	}

	/**
	 * Check if Entry contains relevant attribute and if it contains passed old value. If so, then value is removed and TRUE returned.
	 * If entry doesn't contains attribute or attribute doesn't contains old value false is returned.
	 * If passed value is NULL, then nothing is removed from attribute.
	 * If attribute is empty at the end, then it is removed from Entry.
	 *
	 * @param node
	 * @param entry
	 * @param attribute
	 * @param oldValue
	 * @return
	 */
	private boolean checkAndSetAttributeValue(@NotNull LdapEntryTreeNode node, @NotNull Entry entry, @NotNull Attribute attribute, Value oldValue) {

		// modify local Attribute model first -> client side value validation if possible
		if (entry.get(attribute.getId()) != null) {
			if (oldValue != null) {
				boolean removed = entry.get(attribute.getId()).remove(oldValue);
				if (!removed) {
					log.error("Local representation of '{}' doesn't have Value '{}' we want to remove from Attribute {}.", node.getDn(), oldValue, attribute);
					LdapNotificationHandler.handleError(null, node.getLdapServer().getName(), "Unable to remove value from Attribute '"+attribute.getUpId()+"'.");
					return false;
				}
			}
			// TODO - depending on the desired workflow, we might want to prevent or allow removing of last value of MUST attribute
			// we might expect user will add new value later, but now it is already prevent by the fact, that this action is not
			// active (row is not removable).
			if (!entry.get(attribute.getId()).iterator().hasNext()) {
				// it was a last value of attribute !!
				try {
					entry.remove(attribute);
				} catch (LdapException e) {
					LdapNotificationHandler.handleError(e, node.getLdapServer().getName(), "Unable to remove Attribute '"+attribute.getUpId()+"'.");
				}
			}
		} else {
			log.error("Local representation of '{}' doesn't have an Attribute '{}' we want to remove value from: {}", node.getDn(), attribute, oldValue);
			LdapNotificationHandler.handleError(null, node.getLdapServer().getName(), "Unable to remove value from Attribute '"+attribute.getUpId()+"'.");
			return false;
		}
		return true;

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if RemoveValue action is allowed for selected Attribute.
	 *
	 * @param e Event triggering this
	 * @return TRUE if Action is active or FALSE
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null && !editor.isLoading()) {
				int selectedRow = editor.getTable().getSelectedRow();
				if (selectedRow != -1) {
					return editor.getModel().isRowRemovable(editor.getTable().convertRowIndexToModel(selectedRow));
				}
			}
		}
		return false;

	}

}
