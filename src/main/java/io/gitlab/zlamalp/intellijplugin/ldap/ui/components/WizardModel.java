package io.gitlab.zlamalp.intellijplugin.ldap.ui.components;

import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Model class for the WizardDialog. It contains and handles step switching within WizardDialog.
 *
 * It is expected to be extended in order to hold any additional arbitrary data
 * for the whole WizardDialog life-cycle.
 *
 * During step switching it calls onNext() / onPrevious() etc. on current step before switching.
 * This ensure, that the WizardStep implementation can store/modify data within model.
 *
 * TODO: Simplify some methods ??
 *
 * @author Pavel Zlámal
 */
public class WizardModel {

	private String title = null;
	private boolean isGoalAchieved = false;
	private List<WizardStep> steps = new ArrayList<>();
	private WizardStep currentStep = null;
	private WizardDialog<? extends WizardModel> dialog;

	/**
	 * Create Model class for WizardDialog. It handles content switching for wizard steps
	 * and is expected to be extended in order to hold and manage any arbitrary data
	 * during whole wizard go through.
	 *
	 * @param title Will be used as title for the dialog displaying this wizard model
	 */
	public WizardModel(String title) {
		this.title = title;
	}

	/**
	 * Create Model class for WizardDialog. It handles content switching for wizard steps
	 * and is expected to be extended in order to hold and manage any arbitrary data
	 * during whole wizard go through.
	 *
	 * @param title Will be used as title for the dialog displaying this wizard model
	 * @param steps Steps of wizard dialog
	 */
	public WizardModel(String title, List<WizardStep> steps) {
		this.title = title;
		this.steps = steps;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isGoalAchieved() {
		return isGoalAchieved;
	}

	public void setGoalAchieved(boolean goalAchieved) {
		isGoalAchieved = goalAchieved;
	}

	public void addStep(WizardStep step) {
		steps.add(step);
	}

	public void removeStep(WizardStep step) {
		steps.remove(step);
	}

	public List<WizardStep> getSteps() {
		return steps;
	}

	public void setSteps(List<WizardStep> steps) {
		this.steps = steps;
	}

	/**
	 * Return current step. Initialize to the first step on first call.
	 *
	 * @return current step.
	 */
	public final WizardStep getCurrentStep() {
		assert !steps.isEmpty();
		if (currentStep == null) {
			changeToStep(steps.get(0));
		}
		return currentStep;
	}

	public final int getCurrentStepIndex() {
		return steps.indexOf(getCurrentStep());
	}

	public final JComponent getCurrentComponent() {
		WizardStep step = getCurrentStep();
		return step.getComponent();
	}

	/**
	 * Return TRUE if provided step is the first
	 *
	 * @param step step to check
	 * @return TRUE if it`s first step
	 */
	public final boolean isFirst(WizardStep step){
		return 0 == (steps.indexOf(step));
	}

	/**
	 * Return TRUE if provided step is the last
	 *
	 * @param step step to check
	 * @return TRUE if it`s last step
	 */
	public final boolean isLast(WizardStep step) {
		return (steps.size()-1) == steps.indexOf(step);
	}

	/**
	 * Gets next step of wizard
	 * Returns NULL if there are no more next steps or passed step is not part of the model.
	 *
	 * @param step step to get NEXT for
	 * @return next step
	 */
	@Nullable
	public final WizardStep getNextFor(WizardStep step) {
		int index = steps.indexOf(step);
		if (index >= steps.size()-1 || index < 0) return null;
		return steps.get(index + 1);
	}

	/**
	 * Gets previous step of wizard
	 * Returns NULL if there are no more previous steps or passed step is not part of the model.
	 *
	 * @param step step to get PREVIOUS for
	 * @return previous step
	 */
	@Nullable
	public final WizardStep getPreviousFor(WizardStep step) {
		int index = steps.indexOf(step);
		if (index <= 0) return null;
		return steps.get(index -1);
	}

	/**
	 * Sets passed step as current a notify it about it.
	 *
	 * Previously defined "current step" (if any) is not notified about the change!
	 * See next() and previous() methods to
	 *
	 * @see #next()
	 * @see #previous()
	 *
	 * @param step step to set as current
	 */
	public final void changeToStep(WizardStep step) {
		currentStep = step;
		step.onChangeToStep(this);
	}

	/**
	 * Navigate to the next step and return it.
	 *
	 * Current state is notified about navigation change. If next step doesn't exists,
	 * then current step is not changed / notified and null is returned
	 *
	 * @return next step or null
	 */
	@Nullable
	public final WizardStep next() {
		WizardStep nextStep = getNextFor(getCurrentStep());
		if (nextStep != null) {
			getCurrentStep().onNext(this);
			changeToStep(nextStep);
		}
		return nextStep;
	}

	/**
	 * Navigate to the previous step and return it.
	 *
	 * Current state is notified about navigation change. If previous step doesn't exists,
	 * then current step is not changed / notified and null is returned.
	 *
	 * @return previous step or null
	 */
	@Nullable
	public final WizardStep previous() {
		WizardStep previousStep = getPreviousFor(getCurrentStep());
		if (previousStep != null) {
			getCurrentStep().onPrevious(this);
			changeToStep(previousStep);
		}
		return previousStep;
	}

	/**
	 * Notify current step about finishing the dialog.
	 * Returns true, if it can be or was finished (goal achieved / last step)
	 * and false otherwise.
	 *
	 * @return TRUE finished / FALSE not finished
	 */
	public boolean finish() {
		if (isGoalAchieved() || isLast(getCurrentStep())) {
			getCurrentStep().onFinish(this);
			return isGoalAchieved();
		}
		return false;
	}

	/**
	 * Notify current step about cancellation.
	 */
	public void cancel() {
		getCurrentStep().onCancel(this);
	}

	public WizardDialog<? extends WizardModel> getDialog() {
		return dialog;
	}

	public void setDialog(WizardDialog<? extends WizardModel> dialog) {
		this.dialog = dialog;
	}

}
