package io.gitlab.zlamalp.intellijplugin.ldap.tree;

import com.intellij.openapi.project.DumbAware;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.ui.content.Content;
import org.jetbrains.annotations.NotNull;

import static io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons.LDAP_TOOL_WINDOW;

/**
 * Factory which tels IDE to build LDAP plugin main {@link ToolWindow}
 * containing our {@link LdapTreePanel} component.
 *
 * @author Attila Majoros
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapToolWindowFactory implements ToolWindowFactory, DumbAware {

	@Override
	public void createToolWindowContent(@NotNull Project project, @NotNull ToolWindow toolWindow) {

		LdapTreePanel treePanel = LdapTreePanel.getInstance(project);
		Content content = toolWindow.getContentManager().getFactory().createContent(treePanel, "", false);
		toolWindow.getContentManager().addContent(content);
		toolWindow.setIcon(LDAP_TOOL_WINDOW);
		toolWindow.setTitleActions(treePanel.getTabActions());

	}

}
