<idea-plugin>
	<id>io.gitlab.zlamalp.intellijplugin.ldap</id>
	<name>LDAP &amp; LDIF Support</name>
	<version>0.9</version>
	<vendor email="zlamal@cesnet.cz">Pavel Zlámal</vendor>

	<description><![CDATA[
	  <h2>LDAP</h2>
	  <p>Best-effort schema aware LDAP client
      <ul>
        <li>Store connections to LDAP servers within IDE or Projects.</li>
        <li>Browse LDAP tree in a tree view, edit entries in a table view.</li>
        <li>Add/Rename/Move/Delete LDAP entries.</li>
        <li>Add/Remove/Replace/Delete entry attributes and their values.</li>
        <li>Filter results or perform custom Searches.</li>
        <li>UI support for userPassword Check/Bind/Set/Change.</li>
        <li>Configurable export of any branch/entry or schema to LDIF.</li>
      </ul>
      <h2>LDIF</h2>
      <ul>
        <li>Syntax highlighting for LDIF file.</li>
        <li>Partial support for Structure view.</li>
      </ul>
      <hr/>
      <p>Originally based on a code and ideas from <b>LDAP Browser</b> plugin by <i>Attila Majoros</i> and <b>Apache Directory Studio</b> by <i>The Apache Software Foundation</i>.
    ]]></description>

	<change-notes><![CDATA[
	<p><b>Version 0.9</b></p>
	<p>
	  <br/>* Initial support for Argon2 in userPassword (for now fixed hashing params).
      <br/>* Updated code using deprecated API.
      <br/><br/>
    </p>
	<p><b>Version 0.8</b></p>
	<p>
	  <br/><b>This is probably a breaking update !!</b>
	  <br/>* Minimum required version of IDEA is 2020.2.
      <br/>* Plugin was internally repackaged.
      <br/>* Plugin has new unique ID.
      <br/>* We are now using Gradle to build plugin and to manage its dependencies.
      <br/>* Use ResourcesBundle to support translations.
      <br/>* Converted all IDE 'Components' to 'Services' to support async load.
      <br/>* Register all generic LDAP actions programmatically instead of static definition in plugin.xml
      <br/>* Added Schema settings and overall better support for schema resolving.
      <br/>* Syntax aware attribute value editors.
      <br/>* Fixed entry loading/refreshing regarding loading state in editor/tree.
      <br/>* Support IDE NavBar for LDAP tree browsing/navigation.
      <br/>* Support colors for LDAP connections (displayed in tree browser and file editor tabs).
      <br/><br/>
    </p>
	<p><b>Version 0.7</b></p>
	<p>
	  <br/><b>This is a breaking update !!</b>
      <br/>* Minimum required version of IDEA is 2019.1.
      <br/>* All stored connections are lost and must be re-created manually.
    </p>
      <br/>* We now use connection pool instead of single connection. This way you can perform long-term export while browsing the tree or modifying some entry. Previously everything was blocked until first action was done.
      <br/>* Support for IDEA progress bar on all actions including cancellation when possible, we prevent UI blocks. Entry/Attribute modifications are modal if not under 300ms, blocking the UI on purpose.
      <br/>* Support for moving and renaming of Entry.
      <br/>* Support both ADD/REMOVE and REPLACE for Attribute modifications (based on schema knowledge about equality).
      <br/>* LDAP connection settings UI is now visually aligned with stock DB connections settings. Added more configuration options for the connection.
      <br/>* Store passwords in PasswordSafe (encrypted) instead of plaintext in connection settings.
      <br/>* Improved performance when browsing through the LDAP tree. We can quickly load tenths of thousands of Entries.
      <br/>* Improved performance of Export to LDIF.
      <br/>* Reorganized and unified popup (right-click) menu for TreeView and Entry table editor.
      <br/>* Support for custom LDAP searches and filtering of child entries.
      <br/>* Respect read-only flag for connection in Entry table editor.
      <br/>* Show/hide decorated values in Entry table editor.
      <br/>* Show new popup with attribute type info when hovering attribute name in Entry editor.
      <br/>* Allow to fetch specific attributes or change default sorting for Entry editor overriding connection settings.
      <br/>* Fixed issues when reordering the columns in Entry editor.
      <br/>* Fixed table styles.
      <br/>* Added Copy DN action.
      <br/><br/>
	<p><b>Version 0.6</b></p>
      <br/>* Support collapsing attributes in table editor with more than 10 values (threshold is configurable).
      <br/>* Reworked plugin Actions to properly reflect enabled/disabled state of originating component.
      <br/>* Resolve if Entry has sub-Entries by querying for 'hasSubordinates' attribute.
      <br/>* Display operational attributes in table editor with italic font.
      <br/>* Sort operation attributes last in table editor by default.
      <br/>* Introduced LDAP type property of connection. It helps us to build proper best-effort schema validation.
      <br/>* LDAP Schema used for client side validation is no longer stored within each connection, but rather build from pre-defined schemas of ApacheDirectory API and from actual connection.
      <br/>* Updated Apache Directory API library to 2.0.0 AM2.
      <br/><br/>
	<p><b>Version 0.5</b></p>
      <br/>* Plugin preferences can be now managed from IDE settings page as "LDAP browser" option.
      <br/>* LDAP connections can be stored within IDE or specific Project (see "Shared" option in connection settings).
      <br/>* LDAP connections settings dialog now look like IDE settings dialog (or DB connections dialog).
      <br/>* LDAP connection can have empty password, user is asked on Connect action and its cached for session only.
      <br/>* We keep locally cached tree nodes on connect/disconnect action on connection. You can manually refresh any node.
      <br/>* Redesigned LdapNodeEditor with Attributes table (look like other IDEA tables, default sorting).
      <br/>* Redesigned Actions in Toolbars and right-click popups in LdapTreePanel and LdapNodeEditor.
      <br/>* Implemented proper value editors. We can now check or set new passwords.
      <br/>* New AddAttribute dialog allows setting Attribute options (like language or binary flag).
      <br/>* Unified Export to LDIF... action, asks for export settings in dialog window.
      <br/>* Updated Apache Directory API library to 1.0.2 with security fixes.
      <br/><br/>
	<p><b>Version 0.4</b></p>
      <br/>* Complete rewrite of plugin to use Apache Directory API instead of Spring-LDAP. Its now much quicker to retrieve necessary data from LDAP.
      <br/>* When loading thousands of entries, wrap them to folders for each 1000 in a Tree view for better IDE performance.
      <br/>* Better implementation of attribute value management actions and decisions, if action is available.
      <br/>* Implemented first ValueProviders so we can generically work with specific attributes, like binary GUID or SID.
      <br/>* Use REPLACE instead of REMOVE/ADD when editing attribute value, since we might need to modify binary attribute without equality.
      <br/>* Added possibility for future client side validation based on Schema, but no LDAP/AD seems to be perfectly RFC valid (by Apache Directory API code).
      <br/><br/>
	<p><b>Version 0.3</b></p>
      <br/>* Support for adding LDAP entry attributes and its values (based on schema - entry object classes).
      <br/>* Added StructureView for LDAP entries (display number of attribute values).
      <br/>* On browsing tree view load only objectClass attributes to know entry type/purpose.
      <br/>* On opening entry load all attributes including all values of ranged attributes.
      <br/>* Better support for MS AD LDAP interface and schema.
      <br/>* Unified toolbars and right-click menus.
      <br/><br/>
	<p><b>Version 0.2</b></p>
      <br/>* Support for LDAP server browsing.
      <br/>* Support storing LDAP server connections inside project.
      <br/>* Display entry attributes in editable table (only remove actions now update LDAP server).
      <br/>* Support exporting LDAP entry or sub-tree to temporary LDIF file.
      <br/>* Allow single char keys and values for LDIF entry attributes.
      <br/><br/>
	<p><b>Version 0.1</b></p>
      <br/>* Support for LDIF file format (lexer, parser) according to RFC 2849.
      <br/>* Support for syntax highlighting and color settings.
      <br/>* Format of DNs is not checked yet, now it's just a value/base64 value/url value.
      <br/>* Schema change LDIF is not supported (considered as standard ldif).
      <br/>* Partial support for Structure view.
    ]]>
	</change-notes>

	<!-- please see http://www.jetbrains.org/intellij/sdk/docs/basics/getting_started/build_number_ranges.html for description -->
	<idea-version since-build="232.9559.62"/>

	<!-- please see http://www.jetbrains.org/intellij/sdk/docs/basics/getting_started/plugin_compatibility.html
		 on how to target different products -->
	<!-- uncomment to enable plugin in all products -->
	<depends>com.intellij.modules.lang</depends>
	<depends>com.intellij.platform.images</depends>

	<extensions defaultExtensionNs="com.intellij">

		<!-- CONFIGURATION / COMPONENTS / SERVICES -->

		<!-- Create LDAP actions programmatically to support translation -->
		<applicationService id="LdapActionService" serviceImplementation="io.gitlab.zlamalp.intellijplugin.ldap.LdapActions"/>

		<!-- Register LDAP & LDIF plugin preferences and LDAP server connections storage (IDE wide) -->
		<applicationService id="LdapPluginSettingsService" serviceImplementation="io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapPluginSettingsService" />

		<!-- Register LDAP server connections storage (Project wide) -->
		<projectService id="LdapProjectSettingsService" serviceImplementation="io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapProjectSettingsService" />

		<!-- Register LDAP tree browser tool window as project service -->
		<projectService id="LdapTreePanel" serviceImplementation="io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel" order="after LdapProjectSettingsService" />

		<!-- Register configurable entities displayed in IDE global "Settings" dialog window -->
		<applicationConfigurable instance="io.gitlab.zlamalp.intellijplugin.ldap.settings.plugin.LdapPluginConfigurable" parentId="tools" id="LdapPluginSettings" displayName="LDAP Browser" />
		<applicationConfigurable instance="io.gitlab.zlamalp.intellijplugin.ldap.settings.schema.SchemaConfigurable" parentId="LdapPluginSettings" id="SchemaSettings" displayName="Schemas" />


		<!-- LDAP BROWSER -->


		<!-- Tool window for browsing LDAP trees -->
		<toolWindow id="LDAP" anchor="left" factoryClass="io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapToolWindowFactory" canCloseContents="false" icon="io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons.LDAP_TOOL_WINDOW" />

		<!-- LDAP Entry (file) -->
		<fileType name="LDAP Node" extensions="ldapnode" implementationClass="io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeFileType"/>
		<fileType.fileViewProviderFactory filetype="LDAP Node" implementationClass="io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeFileViewProviderFactory" />
		<fileEditorProvider implementation="io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditorProvider"/>
		<editorTabColorProvider implementation="io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditorProvider"/>
		<structureViewBuilder factoryClass="io.gitlab.zlamalp.intellijplugin.ldap.editor.struct.LdapNodeStructureViewBuilderProvider" />
		<iconProvider implementation="io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeFilePresentationProvider" order="first" />
		<editorTabTitleProvider implementation="io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeFilePresentationProvider"/>

		<!-- IDE NavBar presentation for LdapNode files -->
		<navbar implementation="io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNavBarPresentation" />


		<!-- LDIF LANGUAGE SUPPORT -->


		<fileType name="Ldif" language="Ldif" extensions="ldif" implementationClass="io.gitlab.zlamalp.intellijplugin.ldif.LdifFileType"/>
		<lang.parserDefinition language="Ldif" implementationClass="io.gitlab.zlamalp.intellijplugin.ldif.LdifParserDefinition"/>

		<!-- Syntax -->
		<lang.syntaxHighlighterFactory language="Ldif" implementationClass="io.gitlab.zlamalp.intellijplugin.ldif.LdifSyntaxHighlighterFactory"/>
		<colorSettingsPage implementation="io.gitlab.zlamalp.intellijplugin.ldif.LdifColorSettingsPage"/>

		<!-- Allow IDE shortcut to comment current line -->
		<lang.commenter language="Ldif" implementationClass="io.gitlab.zlamalp.intellijplugin.ldif.LdifCommenter"/>

		<!-- Structure view -->
		<lang.psiStructureViewFactory language="Ldif" implementationClass="io.gitlab.zlamalp.intellijplugin.ldif.LdifStructureViewFactory"/>

		<!-- Annotator TODO - not fully working yet -->
		<annotator language="Ldif" implementationClass="io.gitlab.zlamalp.intellijplugin.ldif.LdifAnnotator"/>

		<!-- Line Marker Provider TODO - testing -->
		<codeInsight.lineMarkerProvider language="Ldif" implementationClass="io.gitlab.zlamalp.intellijplugin.ldif.LdifLineMarkerProvider"/>

		<!-- Reference contributor TODO - not working yet
		<psi.referenceContributor implementation="cz.zlamalp.psi.LdifReferenceContributor"/>
		<lang.findUsagesProvider language="Ldif" implementationClass="cz.zlamalp.psi.LdifFindUsagesProvider"/>
		<lang.refactoringSupport language="Ldif" implementationClass="cz.zlamalp.psi.LdifRefactoringSupportProvider"/>
		 -->

		<!-- Code completion TODO - not working yet
		<completion.contributor language="Ldif" implementationClass="cz.zlamalp.ldif.LdifCompletionContributor"/>
		-->

		<!-- Code Folding TODO - not working yet
		<lang.foldingBuilder language="Ldif" implementationClass="cz.zlamalp.ldif.LdifFoldingBuilder"/>
		-->

		<!-- Documentation  TODO - not working yet
		<lang.documentationProvider language="Ldif" implementationClass="cz.zlamalp.ldif.LdifDocumentationProvider"/>
		-->

	</extensions>

</idea-plugin>
