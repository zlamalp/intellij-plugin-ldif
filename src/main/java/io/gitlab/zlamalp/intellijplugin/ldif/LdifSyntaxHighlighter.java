package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.*;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifTypes;
import org.jetbrains.annotations.NotNull;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

/**
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifSyntaxHighlighter extends SyntaxHighlighterBase {

	public static final TextAttributesKey SEPARATOR = createTextAttributesKey("LDIF_SEPARATOR", DefaultLanguageHighlighterColors.KEYWORD);
	public static final TextAttributesKey KEY = createTextAttributesKey("LDIF_KEY", DefaultLanguageHighlighterColors.KEYWORD);
	public static final TextAttributesKey VALUE = createTextAttributesKey("LDIF_VALUE", DefaultLanguageHighlighterColors.STRING);
	public static final TextAttributesKey BASE64VALUE = createTextAttributesKey("LDIF_BASE64VALUE", DefaultLanguageHighlighterColors.IDENTIFIER);
	public static final TextAttributesKey COMMENT = createTextAttributesKey("LDIF_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);
	public static final TextAttributesKey BAD_CHARACTER = createTextAttributesKey("LDIF_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER);
	public static final TextAttributesKey CONSTANT = createTextAttributesKey("LDIF_CONSTANT", DefaultLanguageHighlighterColors.CONSTANT);
	public static final TextAttributesKey NUMBERS = createTextAttributesKey("LDIF_NUMBER_KEYS", DefaultLanguageHighlighterColors.NUMBER);

	private static final TextAttributesKey[] BAD_CHAR_KEYS = new TextAttributesKey[]{BAD_CHARACTER};
	private static final TextAttributesKey[] SEPARATOR_KEYS = new TextAttributesKey[]{SEPARATOR};
	private static final TextAttributesKey[] COMMENT_KEYS = new TextAttributesKey[]{COMMENT};
	private static final TextAttributesKey[] EMPTY_KEYS = new TextAttributesKey[0];

	private static final TextAttributesKey[] KEY_KEYS = new TextAttributesKey[]{KEY};
	private static final TextAttributesKey[] VALUE_KEYS = new TextAttributesKey[]{VALUE};
	private static final TextAttributesKey[] VALUE_64_KEYS = new TextAttributesKey[]{BASE64VALUE};
	private static final TextAttributesKey[] CONSTANT_KEYS = new TextAttributesKey[]{CONSTANT};
	private static final TextAttributesKey[] NUMBER_KEYS = new TextAttributesKey[]{NUMBERS};

	@NotNull
	@Override
	public Lexer getHighlightingLexer() {
		return new LdifLexerAdapter();
	}

	@NotNull
	@Override
	public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
		if (tokenType.equals(LdifTypes.SEPARATOR) ||
				tokenType.equals(LdifTypes.SEPARATOR_64) ||
				tokenType.equals(LdifTypes.SEPARATOR_FILE)) {
			return SEPARATOR_KEYS;
		} else if (tokenType.equals(LdifTypes.KEY) ||
				tokenType.equals(LdifTypes.KEY_DN) ||
				tokenType.equals(LdifTypes.KEY_VERSION) ||
				tokenType.equals(LdifTypes.KEY_CONTROL) ||
				tokenType.equals(LdifTypes.KEY_CHANGE) ||
				tokenType.equals(LdifTypes.NEWSUP) ||
				tokenType.equals(LdifTypes.NEWRDN) ||
				tokenType.equals(LdifTypes.DELOLDRDN)) {
			return KEY_KEYS;
		} else if (tokenType.equals(LdifTypes.VALUE)) {
			return VALUE_KEYS;
		} else if (tokenType.equals(LdifTypes.VALUE_64)) {
			return VALUE_64_KEYS;
		} else if (tokenType.equals(LdifTypes.ADD) ||
				tokenType.equals(LdifTypes.DELETE) ||
				tokenType.equals(LdifTypes.DASH) ||
				tokenType.equals(LdifTypes.MODIFY) ||
				tokenType.equals(LdifTypes.MODDN) ||
				tokenType.equals(LdifTypes.MODRDN) ||
				tokenType.equals(LdifTypes.CRITICALITY) ||
				tokenType.equals(LdifTypes.KEY_ADD) ||
				tokenType.equals(LdifTypes.KEY_DELETE) ||
				tokenType.equals(LdifTypes.KEY_REPLACE) ||
				tokenType.equals(LdifTypes.OPT) || tokenType.equals(LdifTypes.OPT_SEP)) {
			return CONSTANT_KEYS;
		} else if (tokenType.equals(LdifTypes.VALUE_OID) ||
				tokenType.equals(LdifTypes.VALUE_DIGIT)) {
			return NUMBER_KEYS;
		} else if (tokenType.equals(LdifTypes.COMMENT)) {
			return COMMENT_KEYS;
		} else if (tokenType.equals(TokenType.BAD_CHARACTER)) {
			return BAD_CHAR_KEYS;
		} else {
			return EMPTY_KEYS;
		}
	}
}
