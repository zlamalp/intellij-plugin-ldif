package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers;

import com.intellij.ui.ColoredTableCellRenderer;
import com.intellij.ui.SimpleTextAttributes;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.codehaus.plexus.util.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.awt.*;

public abstract class AbstractValueProvider implements AttributeValueProvider {

	protected LdapEntryTreeNode node;
	protected Attribute attribute;
	protected Value value;

	public AbstractValueProvider(@NotNull LdapEntryTreeNode node, @NotNull Attribute attribute, Value value) {
		this.node = node;
		this.attribute = attribute;
		setRawValue(value); // since our implementation might want to modify it
	}

	@NotNull
	@Override
	public LdapEntryTreeNode getEntry() {
		return this.node;
	}

	@NotNull
	@Override
	public Attribute getAttribute() {
		return this.attribute;
	}

	@Override
	public void renderDisplayValue(ColoredTableCellRenderer renderer, SimpleTextAttributes style, boolean raw) {
		renderer.append(StringUtils.abbreviate(StringUtils.defaultString(getStringValue(), ""),500).replace("\n", "⏎"), style, true);
	}

	@Override
	public Value getRawValue() {
		return this.value;
	}

	@Override
	public void setRawValue(Value rawValue) {
		this.value = rawValue;
	}

	@Override
	public String getStringValue() {

		if (this.value == null) return null;
		if (this.value.isNull()) return null;

		if (this.value.isHumanReadable()) {
			return this.value.getString();
		} else {
			return convertToString(value.getBytes());
		}

	}

	@Override
	public Value constructNewValue(String stringValue) throws LdapInvalidAttributeValueException {

		Value value = null;
		AttributeType type = SchemaUtils.getAttributeType(node, attribute);

		if (attribute.getAttributeType() != null && type != null) {
			value = new Value(type, stringValue);
		} else {
			value = new Value(stringValue);
		}

		return value;

	}

	@Override
	public Value constructNewValue(byte[] byteValue) throws LdapInvalidAttributeValueException {

		Value value = null;
		AttributeType type = SchemaUtils.getAttributeType(node, attribute);

		if (attribute.getAttributeType() != null && type != null) {
			value = new Value(type, byteValue);
		} else {
			value = new Value(byteValue);
		}

		return value;

	}

	@Override
	public String convertToString(byte[] bytes) {
		throw new UnsupportedOperationException(getClass().getSimpleName() +" doesn't support byte[] -> String conversion.");
	}

	@Override
	public byte[] convertToBytes(String string) {
		throw new UnsupportedOperationException(getClass().getSimpleName() +" doesn't support String -> byte[] conversion.");
	}

	@Override
	public abstract AttributeValueEditor getValueEditor(Component component, boolean addingNewValue);

}
