package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors;

import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.BinaryValueProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;

/**
 * Attribute Value Editor for OctetString attributes.
 * This syntax allows empty value (empty byte array).
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class OctetStringValueEditor extends DefaultValueEditor {

	private static Logger log = LoggerFactory.getLogger(OctetStringValueEditor.class);

	/**
	 * Create editor for OctetString attributes.
	 *
	 * @param parent Parent Component for modal dialog
	 * @param valueProvider Value provider for edited Attribute and its Value
	 * @param isAddingNewValue TRUE when we want add new value to attribute / FALSE if we are editing current value
	 */
	public OctetStringValueEditor(@NotNull Component parent, @NotNull BinaryValueProvider valueProvider, boolean isAddingNewValue) {
		super(parent, valueProvider, isAddingNewValue);
		removeEmpty = false;
		init();
	}

	@Nullable
	@Override
	protected String getDimensionServiceKey() {
		return DefaultValueEditor.class.getCanonicalName();
	}

}
