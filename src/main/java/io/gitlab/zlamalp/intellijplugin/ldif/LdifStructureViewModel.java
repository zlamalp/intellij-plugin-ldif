package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.ide.structureView.*;
import com.intellij.ide.util.treeView.smartTree.Sorter;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;

/**
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifStructureViewModel extends StructureViewModelBase implements StructureViewModel.ElementInfoProvider {

	public LdifStructureViewModel(PsiFile psiFile) {
		super(psiFile, new LdifStructureViewElement(psiFile));
	}

	@NotNull
	public Sorter[] getSorters() {
		return new Sorter[]{Sorter.ALPHA_SORTER};
	}

	@Override
	public boolean isAlwaysShowsPlus(StructureViewTreeElement element) {
		return false;
	}

	@Override
	public boolean isAlwaysLeaf(StructureViewTreeElement element) {
		return false;
	}
}
