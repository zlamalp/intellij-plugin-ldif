package io.gitlab.zlamalp.intellijplugin.ldap.filetype;

import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileSystem;
import com.intellij.openapi.vfs.ex.dummy.DummyFileSystem;
import com.intellij.testFramework.LightVirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapServerTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeRootNode;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Representation of single LDAP {@link Entry} as a instance of {@link VirtualFile}. It is necessary for entry editing
 * using {@link LdapEntryEditor} (our standard implementation of IDEs {@link FileEditor}.
 *
 * Each instance is associated with single {@link LdapTreeNode} (sourcing LDAP tree model item) and might be linked to
 * multiple {@link LdapNodePsiFile} or {@link LdapNodePsiDirectory} (see {@link #isDirectory()}.
 *
 * Most of implementation is dummy, since {@link LdapEntryEditor} edits {@link Entry} aka {@link LdapTreeNode} directly
 * using custom GUI (table) and we do not use any Read/Write actions and actual FileSystem as IDE does for actual files.
 *
 * Hence this is just a temporary representation of current LDAP tree entry state.
 *
 * @author Attila Majoros
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapNodeVirtualFile extends LightVirtualFile {

	private static final Logger log = LoggerFactory.getLogger(LdapNodeVirtualFile.class);

	private LdapTreeNode ldapTreeNode;

	public LdapNodeVirtualFile(@NotNull LdapTreeNode ldapTreeNode) {
		this.ldapTreeNode = ldapTreeNode;
	}

	@NotNull
	@Override
	public String getName() {
		if (ldapTreeNode instanceof LdapTreeRootNode) return ldapTreeNode.toString();
		if (ldapTreeNode instanceof LdapServerTreeNode) return ldapTreeNode.toString();
		return ldapTreeNode.toString() + "." + LdapNodeFileType.EXTENSION;
	}

	@Override
	public String getPresentableName() {
		if (ldapTreeNode instanceof LdapTreeRootNode) return ldapTreeNode.toString();
		if (ldapTreeNode instanceof LdapServerTreeNode) return ldapTreeNode.toString();
		return getNameWithoutExtension();
	}

	@NotNull
	@Override
	public VirtualFileSystem getFileSystem() {
		return new DummyFileSystem();
	}

	@NotNull
	@Override
	public String getPath() {
		if (ldapTreeNode instanceof LdapTreeRootNode) return "/LDAP/";
		String path = ldapTreeNode.getLdapServer().getUrl();
		if (ldapTreeNode instanceof LdapEntryTreeNode) {
			path = path + "/" + ((LdapEntryTreeNode) ldapTreeNode).getDn().toString();
		}
		return path;
	}

	@Override
	public boolean isWritable() {
		// Fixme - for our root node this is not true
		if (ldapTreeNode.getLdapServer() != null) {
			return !ldapTreeNode.getLdapServer().isReadOnly();
		}
		return true;
	}

	@Override
	public boolean isDirectory() {

		if (ldapTreeNode instanceof LdapTreeRootNode) return true;
		if (ldapTreeNode instanceof LdapServerTreeNode) return true;

		// FIXME - this is dependant on how we load LDAP tree (eg. RootDSE is without Entry)
		if (ldapTreeNode instanceof LdapEntryTreeNode) {
			if (Objects.equals(LdapNodeType.ROOT_DSE, ((LdapEntryTreeNode) ldapTreeNode).getNodeType())) {
				// root_dse is always folder
				return true;
			}
			if (((LdapEntryTreeNode) ldapTreeNode).getEntry() == null) {
				// entry not loaded, consider it as file to fix NavBar directory resolving for "empty" nodes.
				return false;
			}
		}
		return !ldapTreeNode.isLeaf();

	}

	@Override
	public boolean isValid() {
		return ldapTreeNode != null;
	}

	@Override
	public VirtualFile getParent() {
		TreeNode parentTreeNode = ldapTreeNode.getParent();
		if (parentTreeNode instanceof LdapTreeNode) {
			return ((LdapTreeNode) parentTreeNode).getVirtualFile();
		}
		return null;
	}

	@Override
	public VirtualFile[] getChildren() {
		List<VirtualFile> childVirtualFiles = new ArrayList<>();
		for (int i = 0; i< ldapTreeNode.getChildCount(); i++) {
			TreeNode node = ldapTreeNode.getChildAt(i);
			if (node instanceof LdapTreeNode) {
				if (((LdapTreeNode) node).getVirtualFile() != null) {
					childVirtualFiles.add(((LdapTreeNode) node).getVirtualFile());
				}
			}
		}
		return childVirtualFiles.toArray(new VirtualFile[0]);
	}

	@NotNull
	@Override
	public OutputStream getOutputStream(Object o, long l, long l1) throws IOException {
		return new ByteArrayOutputStream();
	}

	@NotNull
	@Override
	public byte[] contentsToByteArray() throws IOException {
		return new byte[0];
	}

	@Override
	public long getTimeStamp() {
		return 0;
	}

	@Override
	public long getLength() {
		return 0;
	}

	@Override
	public void refresh(boolean asynchronous, boolean recursive, @Nullable Runnable postRunnable) {

	}

	@Override
	public InputStream getInputStream() throws IOException {
		return null;
	}

	@Override
	public long getModificationStamp() {
		return 0;
	}

	@NotNull
	public LdapTreeNode getLdapTreeNode() {
		return this.ldapTreeNode;
	}

	@Override
	public String toString() {
		if (ldapTreeNode instanceof LdapEntryTreeNode) {
			return ((LdapEntryTreeNode)ldapTreeNode).getDn().toString();
		}
		return ldapTreeNode.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		LdapNodeVirtualFile that = (LdapNodeVirtualFile) o;
		return Objects.equals(ldapTreeNode, that.ldapTreeNode);
	}

	@Override
	public int hashCode() {
		return Objects.hash(ldapTreeNode);
	}

	@Nullable
	public Icon getIcon() {
		return ldapTreeNode.getIcon();
	}

}
