package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.LdapBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeType;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.SetFilterDialog;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.Objects;

/**
 * Shows UI to set search "filter" for selected LdapTreeNode (LdapNode).
 * After which it performs refresh on the entry and its children with applied filter.
 *
 * @see LdapTreePanel
 * @see LdapEntryTreeNode
 *
 * @author Attila Majoros
 * @author Pavel Zlámal
 */
public class SetFilterAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.filter";
	private static Logger log = LoggerFactory.getLogger(SetFilterAction.class);

	public SetFilterAction() {
	}

	public SetFilterAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class,
					node -> node.getNodeType().equals(LdapNodeType.NODE) || node.getNodeType().equals(LdapNodeType.BASE));

			if (selectedTreeNodes.length == 1) {
				if (new SetFilterDialog(e.getProject(), true, selectedTreeNodes[0]).showAndGet()) {
					LdapUtils.refresh(selectedTreeNodes[0], LdapUtils.RefreshType.ONLY_CHILDREN);
				}
			}
		} else if (LdapActionUtils.isFromEditor(e)) {

			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null) {
				LdapEntryTreeNode treeNode = editor.getLdapNode();
				if (new SetFilterDialog(e.getProject(), true, treeNode).showAndGet()) {
					LdapUtils.refresh(treeNode, LdapUtils.RefreshType.ONLY_CHILDREN);
				}
			}

		}

	}

	@Override
	public void update(AnActionEvent e) {

		e.getPresentation().setEnabled(isActionActive(e));

		LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);

		if (LdapActionUtils.isFromTree(e)) {

			LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class,
					node -> node.getNodeType().equals(LdapNodeType.NODE) || node.getNodeType().equals(LdapNodeType.BASE));

			if (selectedTreeNodes.length == 1) {
				// check first selected and modify name of item based on it
				if (selectedTreeNodes[0].getFilter() != null &&
						!selectedTreeNodes[0].getFilter().isEmpty()) {
					e.getPresentation().setText(LdapBundle.message("action.ldap.SetFilterAction.alternative.text"));
				}
			}

		} else if (LdapActionUtils.isFromEditor(e)) {

			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null) {
				String filter = editor.getLdapNode().getFilter();
				if (filter != null && !filter.isEmpty()) {
					e.getPresentation().setText(LdapBundle.message("action.ldap.SetFilterAction.alternative.text"));
				}
			}
		}

	}

	/**
	 * Return TRUE if SetFilter action is allowed for selected LdapNodes.
	 *
	 * @param e Event triggering this action
	 * @return TRUE if Action is active or FALSE
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class,
					node -> node.getNodeType().equals(LdapNodeType.NODE) || node.getNodeType().equals(LdapNodeType.BASE));
			return selectedTreeNodes.length == 1 && (selectedTreeNodes[0].getAllowsChildren() || StringUtils.isNotBlank(selectedTreeNodes[0].getFilter()));
		} else if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null && !editor.isLoading()) {
				LdapEntryTreeNode node = editor.getLdapNode();
				return (Objects.equals(node.getNodeType(), LdapNodeType.NODE) ||
						Objects.equals(node.getNodeType(), LdapNodeType.BASE)) &&
						(node.getAllowsChildren() || StringUtils.isNotBlank(node.getFilter()));
			}
		}
		return false;
	}

}
