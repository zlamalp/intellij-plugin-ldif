// This is a generated file. Not intended for manual editing.
package io.gitlab.zlamalp.intellijplugin.ldif.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface LdifChangeNewsup extends PsiElement {

  @Nullable
  LdifValueBase64 getValueBase64();

  @Nullable
  LdifValueNormal getValueNormal();

}
