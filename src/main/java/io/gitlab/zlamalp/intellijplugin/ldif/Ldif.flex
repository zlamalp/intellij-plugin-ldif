package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.psi.tree.IElementType;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifTypes;
import com.intellij.psi.TokenType;
import com.intellij.lexer.FlexLexer;

%%

%class LdifLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%eof{  return;
%eof}
%{
    private IElementType resolvedValueTokenType = LdifTypes.VALUE_DIGIT; // digit is lowes rule
%}

CRLF=\R
COMMENT=("#")[^\r\n]*{CRLF}

KEY_DN="dn"
KEY_VERSION="version:"
KEY_CONTROL="control"
KEY_CHANGE="changetype"

SEPARATOR=(":"){WHITE_SPACE}*
SEPARATOR_64=("::"){WHITE_SPACE}*
SEPARATOR_FILE=(":<"){WHITE_SPACE}*

ONE="1"
ZERO="0"
LDAP_OID=[0-9]+("."[0-9]+)*
TRUE="true"
FALSE="false"
DASH="-"
ADD="add"
DELETE="delete"
MODDN="moddn"
MODRDN="modrdn"
MODIFY="modify"
REPLACE="replace"
NEWRDN="newrdn"
DELOLDRDN="deleteoldrdn"
NEWSUP="newsuperior"

VALUE_SINGLE_LINE=[^ \r\n\f\t][^\r\n\f\t]*
VALUE_CONTINUE_LINE=[^\r\n\f\t][^\r\n\f\t]*
VALUE_64_SINGLE_LINE=[a-zA-Z0-9+/=]+

VALUE_OID={LDAP_OID}     //({CRLF}{WHITE_SPACE}{LDAP_OID})*
VALUE_DIGIT=[0-9]+ //({CRLF}{WHITE_SPACE}[0-9]+)*

VALUE={VALUE_SINGLE_LINE}    //({CRLF}{WHITE_SPACE}{VALUE_CONTINUE_LINE})*
VALUE_64={VALUE_64_SINGLE_LINE} //({CRLF}{WHITE_SPACE}{VALUE_64_SINGLE_LINE})*
VALUE_FILE="file://"{VALUE}

KEY_CHAR=[^-:; \n\r\t\f][^:; \n\r\t\f]*

WHITE_SPACE=" "

%state WAITING_SEPARATOR
%state WAITING_ANY_SEPARATOR

%state WAITING_VALUE
%state WAITING_VALUE_64
%state WAITING_VALUE_FILE
%state WAITING_ANY_VALUE

%state WAITING_CRITICALITY_CONTROL
%state WAITING_CRITICALITY_VALUE
%state WAITING_CRITICALITY_SEPARATOR
%state AFTER_WHITE

%state WAITING_CHANGE_VALUE
%state WAITING_DELOLDRDN
%state WAITING_MODIFY

%state ATTRIBUTE_TYPE
%state OPTIONS
%state VERSION
%state DN
%state CONTROL
%state CONTROL2
%state CHANGE

%%

/*
 * Basically any line-start or generic content
 */
<YYINITIAL> {
  [0-9a-zA-Z]        { yypushback(yylength()); yybegin(ATTRIBUTE_TYPE); }  // everything starts with some attribute type
  {COMMENT}          { return LdifTypes.COMMENT; }  // comment line can be anywhere
  {DASH}             { return LdifTypes.DASH; }     // end token for parts of ldif change entry
  {CRLF}             { return LdifTypes.CRLF; }     // new line
}

/*
 * Handle specific and generic attribute types
 */
<ATTRIBUTE_TYPE> {

  // shared ldif keywords
  {KEY_VERSION}      { yybegin(VERSION); return LdifTypes.KEY_VERSION; }
  {KEY_DN}           { yybegin(DN); return LdifTypes.KEY_DN; }

  // ldif-change entry keywords
  {KEY_CONTROL}      { yybegin(CONTROL); return LdifTypes.KEY_CONTROL; }
  {KEY_CHANGE}       { yybegin(CHANGE); return LdifTypes.KEY_CHANGE; }
  {ADD}":"           { yybegin(WAITING_MODIFY); yypushback(1); return LdifTypes.KEY_ADD; }
  {DELETE}":"        { yybegin(WAITING_MODIFY); yypushback(1); return LdifTypes.KEY_DELETE; }
  {REPLACE}":"       { yybegin(WAITING_MODIFY); yypushback(1); return LdifTypes.KEY_REPLACE; }
  {DELOLDRDN}        { yybegin(WAITING_DELOLDRDN); return LdifTypes.DELOLDRDN; }
  {NEWSUP}           { yybegin(DN); return LdifTypes.NEWSUP; }
  {NEWRDN}           { yybegin(DN); return LdifTypes.NEWRDN; }

  // shared generic ldif keywords and continuation to separators
  [^-:; \n\r\t\f][^:; \n\r\t\f]*     { return LdifTypes.KEY; } // eat content
  ";"                                { yybegin(OPTIONS); return LdifTypes.OPT_SEP; } // check options
  ":"                                { yypushback(1); yybegin(WAITING_ANY_SEPARATOR); } // check separator and value

}

/*
 * Handle attribute type options
 */
<OPTIONS> {
  ";"               { return LdifTypes.OPT_SEP; }
  {KEY_CHAR}        { return LdifTypes.OPT; }
  ":"               { yypushback(1); yybegin(WAITING_ANY_SEPARATOR); }
  // handles ldif modify add/delete/replace rows for attributes with options like "add: name;ns-x-test"
  {CRLF}            { yybegin(YYINITIAL); return LdifTypes.CRLF; }
}

/*
 * "version: 1" rules
 */
<VERSION> {
  {WHITE_SPACE}     { return LdifTypes.WHITE_SPACE; }
  {ONE}             { yybegin(YYINITIAL); return LdifTypes.VALUE_DIGIT; }
}

/*
 *  Handling separators before DN
 */
<DN> {
  // TODO - we will have specific dn value in the future, maybe external rule in parser?
  {SEPARATOR}     { yybegin(WAITING_VALUE);
                    // reset value recognition to the one with lovest priority
                    resolvedValueTokenType = LdifTypes.VALUE_DIGIT;
                    return LdifTypes.SEPARATOR; }
  {SEPARATOR_64}  { yybegin(WAITING_VALUE_64); return LdifTypes.SEPARATOR_64; }
}

/*
 * "control: oid criticality value" rules
 */
<CONTROL> {
  {SEPARATOR}            { yybegin(CONTROL2); return LdifTypes.SEPARATOR; }
}
<CONTROL2> {
  {LDAP_OID}             { return LdifTypes.VALUE_OID; }
  {WHITE_SPACE}          { return LdifTypes.WHITE_SPACE; } // process optional criticality (true/false)
  ({TRUE}|{FALSE})       { return LdifTypes.CRITICALITY; } // true/false
  {SEPARATOR_FILE}       { yybegin(WAITING_VALUE_FILE); return LdifTypes.SEPARATOR_FILE; }
  {SEPARATOR_64}         { yybegin(WAITING_VALUE_64); return LdifTypes.SEPARATOR_64; }
  {SEPARATOR}            { yybegin(WAITING_VALUE); return LdifTypes.SEPARATOR; }
  {CRLF}                 { yybegin(YYINITIAL); return LdifTypes.CRLF; }          // ends control with or without value
  [^]                    { return TokenType.BAD_CHARACTER; }
}

/*
"changetype: add/delete/moddn/modrdn/modify" rules
*/
<CHANGE> {
  {SEPARATOR}            { yybegin(WAITING_CHANGE_VALUE); return LdifTypes.SEPARATOR; }
  [^]                    { return TokenType.BAD_CHARACTER; }
}

<WAITING_CHANGE_VALUE> {
  {ADD}                        { return LdifTypes.ADD; }
  {DELETE}                     { return LdifTypes.DELETE; }
  {MODDN}                      { return LdifTypes.MODDN; }
  {MODRDN}                     { return LdifTypes.MODRDN; }
  {MODIFY}                     { return LdifTypes.MODIFY; }
  {CRLF}                       { yybegin(YYINITIAL); return LdifTypes.CRLF; }
  [^]                          { return TokenType.BAD_CHARACTER; }
}

/*
 * "changetype: modify -> add/delete/replace" rules
 */
<WAITING_MODIFY> {
  {KEY_CHAR}           { return LdifTypes.KEY; }
  {SEPARATOR}          { return LdifTypes.SEPARATOR; }
  ";"                  { yybegin(OPTIONS); return LdifTypes.OPT_SEP; } // ends modify attributes with options eg.: "modify: name;ns-x-test"
  {CRLF}               { yybegin(YYINITIAL); return LdifTypes.CRLF; }  // ends modify attributes without options eg.: "modify: objectClass"
}

/*
"NEWRDN, DELOLDURN, NEWSUP" rules
*/
<WAITING_DELOLDRDN> {
  {SEPARATOR}           { yybegin(yystate()); return LdifTypes.SEPARATOR; }
  {ONE}                 { yybegin(YYINITIAL); return LdifTypes.VALUE_DIGIT; }
  {ZERO}                { yybegin(YYINITIAL); return LdifTypes.VALUE_DIGIT; }
}

/*
 * Handle all types of attribute type separators ( ":" , "::" , ":<" )
 */
<WAITING_ANY_SEPARATOR> {
  {SEPARATOR_FILE}              { yybegin(WAITING_VALUE_FILE); return LdifTypes.SEPARATOR_FILE; }
  {SEPARATOR_64}                { yybegin(WAITING_VALUE_64); return LdifTypes.SEPARATOR_64; }
  {SEPARATOR}                   { yybegin(WAITING_VALUE); return LdifTypes.SEPARATOR; }
  // null value is represented by CRLF after ":" without space
  ":"{CRLF}                     { yypushback(1); yybegin(YYINITIAL); return LdifTypes.SEPARATOR; }
  // some syntaxes allow empty string as a value, represented as whitespace* - not sure if it conflicts with correct NULL value
  ":"{WHITE_SPACE}*{CRLF}       { yypushback(1); yybegin(YYINITIAL); return LdifTypes.SEPARATOR; }
  // not allowed combinations FIXME - tohle označuje ": " jak chybu, přitom bychom chtěli naznačit, že chybí za tím value
  {SEPARATOR}{CRLF}             { yybegin(YYINITIAL); return TokenType.BAD_CHARACTER; }
  {SEPARATOR_64}{CRLF}          { yybegin(YYINITIAL); return TokenType.BAD_CHARACTER; }
  {SEPARATOR_FILE}{CRLF}        { yybegin(YYINITIAL); return TokenType.BAD_CHARACTER; }
}

/*
 * Handle all generic attribute values
 */

// FIXME - tohle půjde vylepšit tak, že si uložíme celý načtený obsah a ten se potom porovná na pravidlo ručně. Jinak totiž když je OID rozdělen tak, že končí tečkou na řádku a pokračuje na dalším číslem, tak se ppovažuje za string
<WAITING_VALUE> {
 {VALUE_DIGIT}           { } // just consume
 {VALUE_OID}             { if (!resolvedValueTokenType.equals(LdifTypes.VALUE)) { resolvedValueTokenType = LdifTypes.VALUE_OID; } }
 {VALUE}                 { if (!resolvedValueTokenType.equals(LdifTypes.VALUE)) { resolvedValueTokenType = LdifTypes.VALUE; } }
 {CRLF}" "               { } // just consume value continuation
 // value continuation might start with whitespace - unlike first list (actual value start)
 {VALUE_CONTINUE_LINE}   { if (!resolvedValueTokenType.equals(LdifTypes.VALUE)) { resolvedValueTokenType = LdifTypes.VALUE; } }
 // end of value, we return it back for parser to check line ending presence and return resolved result!!
 {CRLF}                  { yypushback(1); yybegin(YYINITIAL); IElementType localElementResult = resolvedValueTokenType; resolvedValueTokenType=LdifTypes.VALUE_DIGIT; return localElementResult; }
}

<WAITING_VALUE_64> {
 {VALUE_64}              { }
 {CRLF}" "               { } // just consume value continuation
 {CRLF}                  { yypushback(1); yybegin(YYINITIAL); return LdifTypes.VALUE_64; }
}

<WAITING_VALUE_FILE> {
 {VALUE_FILE}            { }
 {CRLF}" "               { } // just consume value continuation
 {CRLF}                  { yypushback(1); yybegin(YYINITIAL); return LdifTypes.VALUE_FILE; }
}

/*
 * Anything else is a bad character - reset state to initial
 */
[^]                      { yybegin(YYINITIAL); return TokenType.BAD_CHARACTER; }
