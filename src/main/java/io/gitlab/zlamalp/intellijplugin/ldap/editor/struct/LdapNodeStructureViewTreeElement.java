package io.gitlab.zlamalp.intellijplugin.ldap.editor.struct;

import com.google.common.base.Objects;
import com.intellij.ide.structureView.StructureViewTreeElement;
import com.intellij.ide.util.treeView.smartTree.SortableTreeElement;
import com.intellij.ide.util.treeView.smartTree.TreeElement;
import com.intellij.navigation.ItemPresentation;
import com.intellij.util.PlatformIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeModelItem;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Representation of StructureView tree element - Entry DN or one of its Attribute.
 *
 * @see LdapNodeItemPresentation
 * @see LdapNodeStructureViewModel
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapNodeStructureViewTreeElement implements StructureViewTreeElement, ItemPresentation, SortableTreeElement {

	private LdapEntryTreeNode node;
	protected Attribute attribute;
	private boolean invalidated = false;
	private LdapEntryEditor editor;

	public LdapNodeStructureViewTreeElement(LdapEntryEditor editor, LdapEntryTreeNode node, Attribute attribute) {
		this.editor = editor;
		this.node = node;
		this.attribute = attribute;
	}

	@Nullable
	@Override
	public Icon getIcon(boolean unused) {
		return (attribute != null) ? PlatformIcons.ANNOTATION_TYPE_ICON : PlatformIcons.CUSTOM_FILE_ICON;
	}

	@Override
	public String getPresentableText() {
		return (attribute != null) ? attribute.getId() : node.getDn().toString();
	}

	@Override
	public void navigate(boolean requestFocus) {

		// find first item with same attribute - select leading item
		for (AttributeModelItem item : editor.getModel().getItems()) {
			if (Objects.equal(item.getAttribute(), attribute)) {
				int modelIndex = editor.getModel().getItems().indexOf(item);
				int viewIndex = editor.getTable().convertRowIndexToView(modelIndex);
				editor.getTable().getSelectionModel().setSelectionInterval(viewIndex, viewIndex);
				if (requestFocus) editor.getTable().requestFocus();
				break;
			}
		}

	}

	@Override
	public boolean canNavigate() {
		return true;
	}

	@Override
	public boolean canNavigateToSource() {
		return true;
	}

	@Nullable
	@Override
	public String getLocationString() {
		return null;
	}

	@Override
	public Object getValue() {
		Object returnValue = (invalidated) ? null : (attribute != null) ? attribute : node.getDn();
		setInvalidated(false);
		return returnValue;
	}

	@NotNull
	@Override
	public String getAlphaSortKey() {
		return (getPresentableText() != null) ? getPresentableText() : "n/a";
	}

	@NotNull
	@Override
	public ItemPresentation getPresentation() {
		return new LdapNodeItemPresentation(node, attribute);
	}

	@NotNull
	@Override
	public TreeElement[] getChildren() {

		// child node
		if (attribute != null) return EMPTY_ARRAY;

		// root node - get children (attributes)
		Collection<Attribute> attributes = node.getAttributes();
		if (attributes != null) {
			List<TreeElement> elementList = new ArrayList<>();
			for (Attribute attribute : attributes) {
				if (!attribute.getId().contains(";range=")) {
					// SKIP RANGED ATTRIBUTES SINCE THEY ARE PRESENT ALSO WITHOUT RANGE IDENTIFIER
					elementList.add(new LdapNodeStructureViewTreeElement(editor, node, attribute));
				}
			}
			return elementList.toArray(new TreeElement[elementList.size()]);
		}
		return EMPTY_ARRAY;
	}

	public boolean isInvalidated() {
		return invalidated;
	}

	public void setInvalidated(boolean invalidated) {
		this.invalidated = invalidated;
	}

}
