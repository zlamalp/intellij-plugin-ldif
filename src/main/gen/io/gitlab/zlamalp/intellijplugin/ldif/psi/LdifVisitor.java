// This is a generated file. Not intended for manual editing.
package io.gitlab.zlamalp.intellijplugin.ldif.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;

public class LdifVisitor extends PsiElementVisitor {

  public void visitAttribute(@NotNull LdifAttribute o) {
    visitPsiElement(o);
  }

  public void visitAttributeKey(@NotNull LdifAttributeKey o) {
    visitPsiElement(o);
  }

  public void visitChangeAdd(@NotNull LdifChangeAdd o) {
    visitPsiElement(o);
  }

  public void visitChangeDelete(@NotNull LdifChangeDelete o) {
    visitPsiElement(o);
  }

  public void visitChangeDeloldrdn(@NotNull LdifChangeDeloldrdn o) {
    visitPsiElement(o);
  }

  public void visitChangeModdn(@NotNull LdifChangeModdn o) {
    visitPsiElement(o);
  }

  public void visitChangeModify(@NotNull LdifChangeModify o) {
    visitPsiElement(o);
  }

  public void visitChangeModifyAdd(@NotNull LdifChangeModifyAdd o) {
    visitPsiElement(o);
  }

  public void visitChangeModifyDelete(@NotNull LdifChangeModifyDelete o) {
    visitPsiElement(o);
  }

  public void visitChangeModifyReplace(@NotNull LdifChangeModifyReplace o) {
    visitPsiElement(o);
  }

  public void visitChangeNewrdn(@NotNull LdifChangeNewrdn o) {
    visitPsiElement(o);
  }

  public void visitChangeNewsup(@NotNull LdifChangeNewsup o) {
    visitPsiElement(o);
  }

  public void visitChangetype(@NotNull LdifChangetype o) {
    visitPsiElement(o);
  }

  public void visitControl(@NotNull LdifControl o) {
    visitPsiElement(o);
  }

  public void visitDn(@NotNull LdifDn o) {
    visitNamedElement(o);
  }

  public void visitEntry(@NotNull LdifEntry o) {
    visitPsiElement(o);
  }

  public void visitOption(@NotNull LdifOption o) {
    visitPsiElement(o);
  }

  public void visitValueBase64(@NotNull LdifValueBase64 o) {
    visitPsiElement(o);
  }

  public void visitValueNormal(@NotNull LdifValueNormal o) {
    visitPsiElement(o);
  }

  public void visitValueUrl(@NotNull LdifValueUrl o) {
    visitPsiElement(o);
  }

  public void visitVersion(@NotNull LdifVersion o) {
    visitPsiElement(o);
  }

  public void visitNamedElement(@NotNull LdifNamedElement o) {
    visitPsiElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}
