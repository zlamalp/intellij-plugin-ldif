package io.gitlab.zlamalp.intellijplugin.ldap.actions.node;

import com.intellij.LdapBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUIUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeModelItem;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTable;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTableModel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.LdapNotificationHandler;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.DefaultModification;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Modification;
import org.apache.directory.api.ldap.model.entry.ModificationOperation;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

/**
 * Add single value to existing attribute.
 * Attribute is selected in table of opened LdapNode file (representing single LDAP entry).
 *
 * @see LdapNodeVirtualFile
 * @see LdapEntryEditor
 * @see AttributeTable
 * @see AttributeTableModel
 *
 * @author Attila Majoros
 * @author Pavel Zlámal
 */
public class AddAttributeValueAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.addValue";
	private static Logger log = LoggerFactory.getLogger(AddAttributeValueAction.class);

	public AddAttributeValueAction() {
	}

	public AddAttributeValueAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		// Cancel if Attribute value can't be added
		if (!isActionActive(e)) return;

		LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
		LdapEntryTreeNode node = editor.getLdapNode();

		AttributeModelItem selectedItem = editor.getModel().getItems().get(editor.getTable().convertRowIndexToModel(editor.getTable().getSelectedRow()));

		AttributeValueEditor valueEditorDialog = selectedItem.getValueProvider().getValueEditor(editor.getTable(), true);

		if (valueEditorDialog.getDialogWrapper().showAndGet()) {

			Value newValue = ((AttributeValueEditor)valueEditorDialog).getNewValue();
			Attribute attribute = ((AttributeValueEditor)valueEditorDialog).getAttribute();

			// check if new value is valid and can be set on cloned entities to prevent model change
			if (checkAndSetNewAttributeValue(node, node.getEntry().clone(), attribute.clone(), newValue)) {

				if (editor.isDirectEdit()) {

					// MODIFY LDAP DIRECTLY AND RELOAD ENTRY
					Task task = new Task.Modal(e.getProject(), LdapBundle.message("ldap.update.modal.modifyEntry", node.getDn()), true) {

						Exception exception = null;
						LdapConnection connection = null;

						@Override
						public void run(@NotNull ProgressIndicator indicator) {

							indicator.setIndeterminate(false);
							try {

								indicator.setText(LdapBundle.message("ldap.update.modal.connecting"));
								indicator.setFraction(0.1);
								LdapUIUtils.waitIfUIDebug();
								indicator.checkCanceled();
								connection = node.getLdapServer().getConnection();

								indicator.setFraction(0.5);
								indicator.setText(LdapBundle.message("ldap.update.modal.addingAttributeValue", attribute.getUpId()));
								LdapUIUtils.waitIfUIDebug();

								Modification addMod = new DefaultModification(ModificationOperation.ADD_ATTRIBUTE, attribute.getUpId(), newValue);
								indicator.checkCanceled();
								log.info("Adding value to attribute: {}, value: {}", attribute.getUpId(), newValue);
								connection.modify(node.getDn(), addMod);

								indicator.setFraction(1.0);

							} catch (LdapException ex) {
								exception = ex;
							}

						}

						@Override
						public void onSuccess() {

							if (exception != null) {

								// local model was not modified, so just show error
								LdapNotificationHandler.handleError(exception, node.getLdapServer().getName(), "Unable to add value to Attribute '" + attribute.getUpId() + "'.");

							} else {

								// TODO - what is better - locally modify model or re-fetch whole entry from LDAP ?
								// LdapUtils.refresh(ldapNode);
								// update local model representation
								checkAndSetNewAttributeValue(node, node.getEntry(), attribute, newValue);
								// we modified LDAP, make current state the last known
								editor.setLastState(node.getEntry());

							}

							editor.getModel().refresh();

						}

						@Override
						public void onFinished() {

							// close connection even if user canceled operation
							if (connection != null) {
								try {
									node.getLdapServer().releaseConnection(connection);
								} catch (LdapException e) {
									LdapNotificationHandler.handleError(e, node.getLdapServer().getName(), "Can't release connection.");
								}
							}

						}


					};
					ProgressManager.getInstance().run(task);

				} else {

					// MODIFY MODEL AND REFRESH VIEW
					checkAndSetNewAttributeValue(node, node.getEntry(), attribute, newValue);
					editor.getModel().refresh();

				}

			}

			// value can't be added  -> don't modify model or LDAP.

		}

	}

	/**
	 * Tries to add attribute to the entry if not present and set the value if not present.
	 * IF Entry contains same attribute, value is added into it and sourcing attribute param is not used at all.
	 * It modifies passed entry.
	 *
	 * Return TRUE if it succeed, FALSE if failed
	 *
	 * @param entry
	 * @param attribute
	 * @return
	 */
	private boolean checkAndSetNewAttributeValue(@NotNull LdapEntryTreeNode node,
	                                             @NotNull Entry entry,
	                                             @NotNull Attribute attribute,
	                                             @NotNull Value newValue) {

		Attribute localAttribute = attribute;

		if (entry.get(attribute.getId()) != null) {
			// entry already contains attribute
			localAttribute = entry.get(attribute.getId());
		} else {
			try {
				// not present in Entry - add newly create Attribute
				entry.add(localAttribute);
			} catch (LdapException e) {
				LdapNotificationHandler.handleError(e, "LDAP: " + node.getLdapServer().getName(), "Unable to add value to Attribute '" + localAttribute.getUpId() + "'.");
				return false;
			}
		}

		// try to pass new value to the attribute
		try {
			localAttribute.add(newValue);
		} catch (LdapInvalidAttributeValueException e1) {
			LdapNotificationHandler.handleError(e1, node.getLdapServer().getName(), "Unable to add value to Attribute '" + localAttribute.getUpId() + "'.");
			return false;
		}

		// fallback for no schema validation
		AttributeType type = SchemaUtils.getAttributeType(node, localAttribute);
		if (type != null && type.isSingleValued() && localAttribute.size() > 1) {
			LdapNotificationHandler.handleError(null,node.getLdapServer().getName(), "Unable to add value to Attribute '" + localAttribute.getUpId() + "'. It is single valued!");
			return false;
		}

		return true;

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if AddValue action is allowed for selected Attribute
	 *
	 * @param e Event triggering this
	 * @return TRUE if Action is active or FALSE
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null && !editor.isLoading()) {
				int selectedRow = editor.getTable().getSelectedRow();
				if (selectedRow != -1) {
					return editor.getModel().isRowAddable(editor.getTable().convertRowIndexToModel(selectedRow));
				} else {
					// no row selected
					return false;
				}
			}
		}
		return false;
	}

}
