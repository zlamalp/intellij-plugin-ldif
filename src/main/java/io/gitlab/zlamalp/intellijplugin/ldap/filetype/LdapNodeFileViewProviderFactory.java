package io.gitlab.zlamalp.intellijplugin.ldap.filetype;

import com.intellij.lang.Language;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.FileViewProviderFactory;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import org.jetbrains.annotations.NotNull;

/**
 * IDE {@link FileViewProviderFactory} extension point which returns our {@link LdapNodeFileViewProvider}.
 * It enables us to return custom {@link PsiFile} implementation ({@link LdapNodePsiFile} in this case)
 * for given {@link VirtualFile} ({@link LdapNodeVirtualFile} in this case).
 *
 * @see LdapNodeFileViewProvider
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapNodeFileViewProviderFactory implements FileViewProviderFactory {

	@NotNull
	@Override
	public FileViewProvider createFileViewProvider(@NotNull VirtualFile file, Language language, @NotNull PsiManager manager, boolean eventSystemEnabled) {
		return new LdapNodeFileViewProvider(manager, file, eventSystemEnabled, new LdapNodeFileType());
	}

}
