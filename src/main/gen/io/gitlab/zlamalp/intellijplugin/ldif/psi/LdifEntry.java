// This is a generated file. Not intended for manual editing.
package io.gitlab.zlamalp.intellijplugin.ldif.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface LdifEntry extends PsiElement {

  @NotNull
  List<LdifAttribute> getAttributeList();

  @Nullable
  LdifChangetype getChangetype();

  @Nullable
  LdifControl getControl();

  @NotNull
  LdifDn getDn();

}
