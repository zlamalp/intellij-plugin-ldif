package io.gitlab.zlamalp.intellijplugin.ldap.tree.node;

import com.intellij.pom.Navigatable;
import com.intellij.util.ui.tree.TreeUtil;
import com.intellij.util.xmlb.annotations.Transient;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeType;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import org.apache.commons.lang.StringUtils;
import org.apache.directory.api.ldap.model.constants.SchemaConstants;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.name.Dn;
import org.apache.directory.api.ldap.model.name.Rdn;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * TreeNode representation of single LDAP Entry which is used as backing userObject of a TreeNode.
 *
 * During creation we must specify to which LDAP this entry belongs to, what is its type and what is the DN.
 * Backing Entry retrieved from LDAP is optional, since we want to represent also ROOT_DSE like nodes and
 * also pre-configured BASE nodes for which requestor might not have proper access rights.
 *
 * Nodes are placed in Tree in LdapTreePanel. They are created on-demand, as user navigates the tree.
 * During TreeNode expansion LDAP is queried and child nodes are appended to the parent.
 *
 * If more than configurable limit of children is loaded from LDAP, then they are wrapped in folders-like
 * nodes (LdapTreeNodeWrapper). Limit can be set to 0 to prevent this {@see LdapSettings#getWrapChildrenBy())}
 *
 * Whenever we want to refresh the node or query its children, its DN is a main key, what will be retrieved from
 * LDAP, not sourcing Entry. So you can e.g. rename/move the node by setting new DN and calling standard refresh.
 *
 * @see LdapTreePanel
 * @see LdapNodeType
 * @see LdapSettings#getWrapChildrenBy()
 * @see LdapUtils#refresh(LdapEntryTreeNode, LdapUtils.RefreshType)
 *
 * @author Attila Majoros
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapEntryTreeNode extends DefaultMutableTreeNode implements LdapTreeNode, Comparable<LdapEntryTreeNode>, Navigatable {

	private static Logger log = LoggerFactory.getLogger(LdapEntryTreeNode.class);

	// LDAP server ldapServer associated with this node
	@Transient
	transient private LdapServer ldapServer;

	// DN identifying this LdapTreeNode
	private Dn dn;

	// Our type of node, like the Root_DSE, Base (DSA), node
	private LdapNodeType nodeType;

	// custom filter applied to the children search
	private String filter;

	// virtual file associated with this LDAP entry
	@Transient
	transient private LdapNodeVirtualFile file;

	// editor associated with entry, null if not present
	@Transient
	transient private LdapEntryEditor editor;

	// marker whether node is expected to have children
	private boolean mayHaveChildren = true;

	/**
	 * Marker that nodes children were queried and there is no reason to re-fetch them during LdapTreeNode expansion in LdapTreePanel.
	 * Default is false. Its set to TRUE during particular LdapUtils.refresh() call.
	 *
	 * @see LdapUtils#refresh(LdapEntryTreeNode)
	 */
	private transient boolean childrenLoaded = false;

	/**
	 * Create new TreeNode for LdapTreePanel matching single LDAP Entry.
	 *
	 * @param ldapServer LDAP server ldapServer
	 * @param nodeType   Our internal type of Node (ROOT_DSE, BASE, NODE)
	 * @param dn DN of LDAP Entry
	 * @param entry LDAP Entry itself
	 */
	public LdapEntryTreeNode(@NotNull LdapServer ldapServer, @NotNull LdapNodeType nodeType, @NotNull Dn dn, Entry entry) {

		this.ldapServer = ldapServer;
		this.nodeType = nodeType;
		this.dn = dn;

		// set entry and resolve mayHaveChildren
		setEntry(entry);

		this.file = new LdapNodeVirtualFile(this);

	}

	@Override
	public boolean getAllowsChildren() {

		if (getEntry() == null) return true;
		return mayHaveChildren;

	}

	@Override
	public boolean isLeaf() {

		// not initialized
		if (getEntry() == null) return false;

		return !getAllowsChildren() || (childrenLoaded && super.isLeaf());

	}

	/**
	 * Set marker, whether nodes children were queried
	 *
	 * @param loaded TRUE children loaded / FALSE children not loaded
	 */
	public void setChildrenLoaded(boolean loaded) {
		this.childrenLoaded = loaded;
	}

	/**
	 * Marker whether this node was queried for child nodes
	 *
	 * @return TRUE if nodes children were at least once queried / FALSE otherwise
	 */
	public boolean isChildrenLoaded() {
		return this.childrenLoaded;
	}

	/**
	 * Get all child LdapTreeNodes for this parent (LdapTreeNode) or empty list if no child is present.
	 * Value is taken from local tree structure, no actual ldap search is performed.
	 *
	 * If child nodes are LdapTreeNodeWrapper nodes, then their children
	 * are aggregated and returned instead.
	 *
	 * This means, that children doesn't actually reflect local Tree model, but rather actual LDAP model.
	 *
	 * You can fill this node with children by calling LdapUtils.refresh()
	 *
	 * @see io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils#refresh(LdapEntryTreeNode, io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils.RefreshType)
	 *
	 * @return list of child LdapTreeNodes
	 */
	public @NotNull List<LdapTreeNode> getChildren() {
		List<LdapTreeNode> children = new ArrayList<>();
		for (int i=0; i<getChildCount(); i++) {
			TreeNode node = getChildAt(i);
			if (node instanceof LdapEntryTreeNode) {
				children.add(((LdapEntryTreeNode) node));
			} else if (node instanceof  LdapTreeNodeWrapper) {
				children.add(((LdapTreeNode) node));
			}
		}
		return children;
	}

	/**
	 * Return String representation of this Entry`s RDN or empty string if node doesn't have DN set at all.
	 *
	 * @return RDN as a string or empty string
	 */
	public String getRdn() {

		if (dn != null) {
			return getDn().getRdn().toString();
		}
		return "";

	}

	/**
	 * Set new LDAP Entry to the LdapTreeNode.
	 *
	 * @param entry Entry to be set or NULL
	 */
	public void setEntry(Entry entry) {
		setUserObject(entry); // should trigger node changed, but probably it doesn't ??
		mayHaveChildren = resolveMayHaveChildren(entry);
	}

	/**
	 * Return LDAP Entry associated with this LdapTreeNode or NULL if it was not yet retrieved.
	 *
	 * @return LDAP Entry or NULL.
	 */
	public Entry getEntry() {
		return (Entry)getUserObject();
	}

	/**
	 * Return collection of Entry Attributes if entry is present or NULL.
	 * Local collection might differ from server based on the context in which they were retrieved.
	 *
	 * @return Collection of Attributes or NULL.
	 */
	public Collection<Attribute> getAttributes() {
		return (getEntry() != null) ? getEntry().getAttributes() : null;
	}

	/**
	 * Return specified Entry Attribute or null if not present or entry not loaded.
	 *
	 * @param name Name of attribute
	 * @return Attribute object
	 */
	public Attribute getAttributeByName(String name) {
		// TODO - we should probably enforce search based on ID (OID if attribute type is known and set within attribute) or lower-cased name
		if (getEntry() == null) return null;
		return getEntry().get(name);
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	/**
	 * Return DN representing this LdapTreeNode within sourcing LDAP.
	 * On refreshing, this value determine, what will be fetched from LDAP.
	 * Some nodes (like ROOT_DSE) might not have it set.
	 *
	 * @return DN or null
	 */
	public Dn getDn() {
		return dn;
	}

	/**
	 * Set DN by which is this LdapTreeNode represented in sourcing LDAP.
	 * On refreshing, this value determine, what will be fetched from LDAP.
	 * Some nodes (like ROOT_DSE) might not have it set (can be NULL).
	 *
	 * @param dn DN or null
	 */
	public void setDn(Dn dn) {
		this.dn = dn;
	}

	/**
	 * Return all values of ObjectClass attribute for this LdapTreeNode as list of strings or empty list.
	 * Value is backed by contained Entry object. If not present, empty list is returned.
	 *
	 * @return List of all objectClass values
	 */
	public List<String> getObjectClasses() {
		return resolveObjectClasses(getEntry());
	}

	/**
	 * Return type of this LdapTreeNode
	 *
	 * @return type of this node
	 */
	public LdapNodeType getNodeType() {
		return nodeType;
	}

	/**
	 * Get editor currently associated with this LdapTreeNode
	 *
	 * @return associated AttributeTableWrapper or NULL
	 */
	public LdapEntryEditor getEditor() {
		return editor;
	}

	/**
	 * Set editor associated with this LdapTreeNode
	 *
	 * @param editor Editor to be set
	 */
	public void setEditor(LdapEntryEditor editor) {
		this.editor = editor;
	}

	/**
	 * Get LdapServer related to this TreeNode
	 *
	 * @return LdapServer related to this TreeNode
	 */
	public LdapServer getLdapServer() {
		return ldapServer;
	}

	@NotNull
	@Override
	public String getTreeNodePresentation() {
		return toString();
	}

	@Nullable
	public Icon getIcon() {

		if (StringUtils.isNotBlank(filter)) {
			// TODO - maybe some cross-over icon with filter/nodeType, maybe restrict this icon to tree view only
			return LdapIcons.FILTER;
		}

		if (LdapNodeType.ROOT_DSE.equals(nodeType)) {

			return LdapIcons.ROOT_DSE;

		} else if (LdapNodeType.BASE.equals(nodeType)) {

			return LdapIcons.BASE;

		} else if (LdapNodeType.NODE.equals(nodeType)) {

			if (isLeaf()) {
				return LdapIcons.ENTRY;
			} else {
				return LdapIcons.ENTRY_WITH_CHILDREN;
			}

		}
		// fallback
		return LdapIcons.ENTRY;

	}

	@Nullable
	@Override
	public LdapNodeVirtualFile getVirtualFile() {
		return file;
	}

	@Override
	public String toString() {

		if (LdapNodeType.ROOT_DSE.equals(nodeType)) {
			return "Root DSE";
		} else if (LdapNodeType.BASE.equals(nodeType)) {
			return getDn().toString();
		} else {
			return getDn().getRdn().toString();
		}

	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		LdapEntryTreeNode that = (LdapEntryTreeNode) o;
		return Objects.equals(ldapServer, that.ldapServer) &&
				Objects.equals(dn, that.dn) &&
				Objects.equals(nodeType, that.nodeType);
	}

	@Override
	public int hashCode() {
		return Objects.hash(ldapServer, dn, nodeType);
	}

	/**
	 * Resolve all objectClass attribute values associated with Entry
	 * @param entry Entry to get objectClass values for
	 *
	 * @return List of all objectClass values or empty list.
	 */
	private List<String> resolveObjectClasses(Entry entry) {

		List<String> result = new ArrayList<>();
		if  (entry != null) {
			Attribute attr = entry.get(SchemaConstants.OBJECT_CLASS_AT);
			if (attr == null) {
				attr = entry.get(SchemaConstants.OBJECT_CLASS_AT.toLowerCase());
			}
			if (attr == null) {
				attr = entry.get(SchemaConstants.OBJECT_CLASS_AT_OID);
			}
			if (attr == null) {
				log.error("Unable to get all objectClass attribute values, was empty for {}.", getDn());
				return result;
			}
			for (Value val : attr) {
				result.add(val.getString());
			}
		}
		return result;

	}

	/**
	 * Resolve if Entry may have children in LDAP or not.
	 * Resolve is based on HAS_SUBORDINATES attribute and if not present on our static set of "objectClasses" which usually
	 * don't have children.
	 *
	 * At the end HAS_SUBORDINATES attribute is removed from the Entry is it was not between "requested attributes"
	 * for this LdapTreeNode.
	 *
	 * @return TRUE if may have / has children, FALSE otherwise
	 */
	private boolean resolveMayHaveChildren(Entry entry) {

		Attribute subOrdAttr = null;
		if (entry != null) {
			subOrdAttr = entry.get(SchemaConstants.HAS_SUBORDINATES_AT);
		}

		if (subOrdAttr != null) {

			Set<String> requestedAttributes = LdapUtils.getRequestedAttributes(this);

			// if HAS_SUBORDINATES is not requested attribute, try to remove it, since we manually add it
			// to properly resolve if Entry might have children.
			if (!requestedAttributes.contains(SchemaConstants.ALL_OPERATIONAL_ATTRIBUTES) && !requestedAttributes.contains(SchemaConstants.HAS_SUBORDINATES_AT)) {
				try {
					// entry won't be null otherwise subOrdAttr would be null too
					entry.remove(subOrdAttr);
				} catch (LdapException e) {
					log.error("We couldn't remove unwanted attribute {} from entry {}.", subOrdAttr.getId(), entry.getDn());
				}
			}

			return Objects.equals("TRUE", subOrdAttr.get().getString());

		}

		// fallback to object class resolving who can and can't have children
		return !LdapUtils.skipLoadingChildren(getObjectClasses());

	}

	@Override
	public int compareTo(@NotNull LdapEntryTreeNode o) {

		if (getDn() == null && o.getDn() != null) return -1;
		else if (o.getDn() == null && getDn() != null) return 1;
		else if (getDn() == null && o.getDn() == null) return 0;
		else {
			List<Rdn> localRdns = getDn().getRdns();
			List<Rdn> remoteRdns = o.getDn().getRdns();

			// same length use
			if (localRdns.size() == remoteRdns.size()) return getDn().toString().compareToIgnoreCase(o.getDn().toString());

			int minLength = Math.min(localRdns.size(), remoteRdns.size());

			for (int i=minLength-1; i>=0; i--) {
				int result = localRdns.get(i).compareTo(remoteRdns.get(i));
				// FIXME - if they differ (at the same length), then its result, otherwise we continue
				if (result != 0) return result;
			}

			return localRdns.size() - remoteRdns.size();

			// now decide who is longer
			//return getDn().toString().compareToIgnoreCase(o.getDn().toString());

			//return Comparator.comparing(getDn().getRdns()).thenComparing(o.getDn().getRdns()).compare(this, o);
			/*
			int length = localRdns.size() - remoteRdns.size();
			if (length != 0) return length;

			return getDn().toString().compareToIgnoreCase(o.getDn().toString());
			*/

		}

	}

	@Override
	public void navigate(boolean requestFocus) {
		// fixme - should be project specific
		TreeUtil.selectInTree(this, requestFocus, LdapTreePanel.getInstance().getTree());
	}

	@Override
	public boolean canNavigate() {
		return true;
	}

	@Override
	public boolean canNavigateToSource() {
		return true;
	}
}
