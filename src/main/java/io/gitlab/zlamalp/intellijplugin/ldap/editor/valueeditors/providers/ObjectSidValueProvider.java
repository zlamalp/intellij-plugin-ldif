package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers;

import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.BinaryValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.apache.directory.api.util.Hex;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.Objects;

/**
 * Value provider for objectSID attribute of MS AD.
 *
 * Value is Binary, but we are able to convert it to human readable string and again valid input parse to Binary form.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class ObjectSidValueProvider extends BinaryValueProvider {

	private static Logger log = LoggerFactory.getLogger(ObjectSidValueProvider.class);

	public ObjectSidValueProvider(@NotNull LdapEntryTreeNode node, @NotNull Attribute attribute, Value value) {
		super(node, attribute, value);
		if (!Objects.equals(attribute.getId(), "objectsid")) {
			throw new IllegalArgumentException("You can't use ObjectSidValueProvider to edit value of "+attribute.getUpId()+" attribute.");
		}
		if (value != null && value.isHumanReadable()) {
			throw new IllegalArgumentException("Value passed to ObjectSidValueProvider must be binary.");
		}
	}

	@Override
	public String getStringValue() {

		if (getRawValue() == null) return null;
		if (getRawValue().isNull()) return null;

		return convertToString(getRawValue().getBytes());

	}

	@Override
	public Value constructNewValue(String stringValue) throws LdapInvalidAttributeValueException {
		return constructNewValue(convertToBytes(stringValue));
	}

	@Override
	public String convertToString(byte[] bytes) {

		/*
		 * The binary data structure, from http://msdn.microsoft.com/en-us/library/cc230371(PROT.10).aspx:
		 *   byte[0] - Revision (1 byte): An 8-bit unsigned integer that specifies the revision level of the SID structure. This value MUST be set to 0x01.
		 *   byte[1] - SubAuthorityCount (1 byte): An 8-bit unsigned integer that specifies the number of elements in the SubAuthority array. The maximum number of elements allowed is 15.
		 *   byte[2-7] - IdentifierAuthority (6 bytes): A SID_IDENTIFIER_AUTHORITY structure that contains information, which indicates the authority under which the SID was created. It describes the entity that created the SID and manages the account.
		 *               Six element arrays of 8-bit unsigned integers that specify the top-level authority
		 *               big-endian!
		 *   and then - SubAuthority (variable): A variable length array of unsigned 32-bit integers that uniquely identifies a principal relative to the IdentifierAuthority. Its length is determined by SubAuthorityCount.
		 *              little-endian!
		 */

		if ( bytes == null || bytes.length < 8 )
		{
			return "Invalid SID !!";
			//return Messages.getString( "InPlaceMsAdObjectSidValueEditor.InvalidSid" ); //$NON-NLS-1$
		}

		char[] hex = Hex.encodeHex( bytes );
		StringBuffer sb = new StringBuffer();

		// start with 'S'
		sb.append( 'S' );

		// revision
		int revision = Integer.parseInt( new String( hex, 0, 2 ), 16 );
		sb.append( '-' );
		sb.append( revision );

		// get count
		int count = Integer.parseInt( new String( hex, 2, 2 ), 16 );

		// check length
		if ( bytes.length != ( 8 + count * 4 ) )
		{
			return "Invalid SID !!";
			//return Messages.getString( "InPlaceMsAdObjectSidValueEditor.InvalidSid" ); //$NON-NLS-1$
		}

		// get authority, big-endian
		long authority = Long.parseLong( new String( hex, 4, 12 ), 16 );
		sb.append( '-' );
		sb.append( authority );

		// sub-authorities, little-endian
		for ( int i = 0; i < count; i++ )
		{
			StringBuffer rid = new StringBuffer();
			for ( int k = 3; k >= 0; k-- )
			{
				rid.append( hex[16 + ( i * 8 ) + ( k * 2 )] );
				rid.append( hex[16 + ( i * 8 ) + ( k * 2 ) + 1] );
			}

			long subAuthority = Long.parseLong( rid.toString(), 16 );
			sb.append( '-' );
			sb.append( subAuthority );
		}

		return sb.toString();

	}

	@Override
	public byte[] convertToBytes(String string) {
		throw new UnsupportedOperationException("We can't store objectSID value yet!");
	}

	@Override
	public AttributeValueEditor getValueEditor(Component component, boolean addingNewValue) {
		return new BinaryValueEditor(component, this, addingNewValue);
	}

}
