package io.gitlab.zlamalp.intellijplugin.ldap.schema.ui;

import com.intellij.ui.HyperlinkLabel;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.labels.BoldLabel;
import org.apache.commons.lang.StringUtils;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.apache.directory.api.ldap.model.schema.ObjectClass;

import javax.swing.*;
import javax.swing.text.html.HTMLEditorKit;
import java.util.List;
import java.util.stream.Collectors;

public class ObjectClassPanel {

	private JBLabel names;
	private JPanel wrapper;
	private JBLabel objectClassType;
	private BoldLabel heading;
	private JBLabel oids;
	private JBLabel description;
	private JBLabel superiors;
	private JTextPane mustAttributes;
	private JTextPane mayAttributes;
	private JBLabel extensions;

	public ObjectClassPanel(ObjectClass objectClass) {

		heading.setText("<html>Object class: "+objectClass.getName()+"</html>");

		// allow to copy content
		names.setCopyable(true);
		oids.setCopyable(true);
		description.setCopyable(true);
		superiors.setCopyable(true);
		extensions.setCopyable(true);

		names.setText("<html>"+StringUtils.join(objectClass.getNames().stream()
				.sorted().collect(Collectors.toList()), "<br/>")+"</html>");
		oids.setText(objectClass.getOid());
		objectClassType.setText(objectClass.getType().name());
		description.setText(objectClass.getDescription() != null ? objectClass.getDescription() : "");

		List<ObjectClass> sups = objectClass.getSuperiors();
		superiors.setText("<html>"+StringUtils.join(sups.stream()
				.map(ObjectClass::getName).sorted().collect(Collectors.toList()), "<br/>")+"</html>");

		mustAttributes.setFont(names.getFont());
		mustAttributes.setBackground(null);
		mustAttributes.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		mustAttributes.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);
		HTMLEditorKit eKit = new HTMLEditorKit();
		mustAttributes.setEditorKit(eKit);


		List<AttributeType> must = objectClass.getMustAttributeTypes();
		if (must.size() > 0) {

			for (String name : must.stream()
					.map(AttributeType::getName).sorted().collect(Collectors.toList())) {
				HyperlinkLabel label = new HyperlinkLabel(name);
				label.setBorder(BorderFactory.createEmptyBorder(0,0,0,5));
				mustAttributes.insertComponent(label);
			}

			//mustAttributtes.setText("<html>"+StringUtils.join(convertedSortedMust, "<br/>")+"</html>");
		}
		/*
		mustAttributtes.addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent e) {
				if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
					// if clicked, show the URL
					JOptionPane.showMessageDialog(null, e.getDescription(), "Link clicked!", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		*/

		List<AttributeType> may = objectClass.getMayAttributeTypes();
		mayAttributes.setFont(names.getFont());
		mayAttributes.setBackground(null);
		mayAttributes.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		mayAttributes.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);
		if (may.size() > 0) {

			for (String name : may.stream()
					.map(AttributeType::getName).sorted().collect(Collectors.toList())) {
				HyperlinkLabel label = new HyperlinkLabel(name);
				label.setBorder(BorderFactory.createEmptyBorder(0,0,0,5));
				mayAttributes.insertComponent(label);
			}

			//mayAttributes.setText("<html>"+StringUtils.join(may.stream().map(AttributeType::getName).sorted().collect(Collectors.toList()), "<br/>")+"</html>");
		}

		StringBuilder builder = new StringBuilder("<html>");
		for (String key : objectClass.getExtensions().keySet().stream().sorted().collect(Collectors.toList())) {
			builder.append(key + " = " + StringUtils.join(objectClass.getExtensions().get(key), ", ") + "<br/>");
		}
		builder.append("</html>");
		extensions.setText(builder.toString());



	}

	public JComponent getComponent() {
		return wrapper;
	}

}
