package io.gitlab.zlamalp.intellijplugin.ldap.editor;

import com.intellij.LdapBundle;
import com.intellij.ui.table.JBTable;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * Table model class which stores all LDAP Entry attributes
 *
 * @author Attila Majoros
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class AttributeTableModel implements TableModel {

	public static final String[] COLUMN_NAMES = new String[] {"", LdapBundle.message("ldap.editor.table.attributeColumn.text"), LdapBundle.message("ldap.editor.table.valueColumn.text")};
	public static final Class<?>[] COLUMN_CLASSES = new Class<?>[] {AttributeModelItem.class, AttributeModelItem.class, AttributeModelItem.class};
	public static final boolean[] COLUMN_EDITABLE = new boolean[] {true, false, true};

	private static Logger log = LoggerFactory.getLogger(AttributeTableModel.class);

	private LdapEntryTreeNode ldapNode;
	private List<AttributeModelItem> items;
	private List<TableModelListener> listeners;
	private Set<String> expandedItems = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
	// model sorting
	private boolean sortObjectClassFirst;
	private boolean sortMustAttributesFirst;
	private boolean sortOperationalAttributesLast;

	// FIXME - kde mít tyto volby
	private boolean sortByAttributeType;
	private boolean sortAscending;

	/**
	 * Create TableModel for single LDAP Entry
	 *
	 * @param ldapNode LDAP Entry
	 */
	public AttributeTableModel(@NotNull LdapEntryTreeNode ldapNode) {
		this.ldapNode = ldapNode;
		this.listeners = new ArrayList<>();
		LdapSettings settings = LdapUtils.getPreferredLdapSettings(ldapNode.getLdapServer().getSettings());
		this.sortObjectClassFirst = settings.isSortObjectClassFirst();
		this.sortMustAttributesFirst = settings.isSortMustAttributeFirst();
		this.sortOperationalAttributesLast = settings.isSortOperationalAttributesLast();
		this.sortByAttributeType = settings.isSortByAttributeType();
		this.sortAscending = settings.isSortAscending();
		this.items = createItems();
	}

	/**
	 * Initialize items from all Attributes
	 *
	 * @return List of items for all attributes present in Entry
	 */
	private List<AttributeModelItem> createItems() {

		List<AttributeModelItem> items = new ArrayList<>();

		// refresh current settings
		Map<Integer,List<AttributeModelItem>> itemsMap = new HashMap<>();
		itemsMap.put(0, new ArrayList<>());
		itemsMap.put(1, new ArrayList<>());
		itemsMap.put(2, new ArrayList<>());
		itemsMap.put(3, new ArrayList<>());

		// FIXME - for some reason only attributes present in the Entry at the LdapEntryTreeNode creation are present
		// FIXME if we add attributes to the entry after node creation, they are missing once table model is refreshed
		Collection<Attribute> attributes = ldapNode.getAttributes();
		LdapSettings ldapSettings = LdapUtils.getPreferredLdapSettings(ldapNode.getLdapServer().getSettings());

		int wrapAttributesBy = ldapSettings.getWrapAttributesBy();
		boolean showDecoratedValues = ldapSettings.isShowDecoratedValues();

		if (attributes != null) {
			for (Attribute attribute : attributes) {

				// FIXME - was added to filter out wrong attributes from table model, should be re-done to check local Editor settings instead !!
				/*
				// filter attributes which we shouldn't retrieve
				AttributeType type = SchemaUtils.getAttributeType(ldapNode.getLdapServer().getSchema(), attribute);
				if (type != null) {
					boolean shouldSkip = false;
					// remove if attribute name is not requested
					if (!ldapSettings.getFetchOnlyAttributes().isEmpty() && !ldapSettings.getFetchOnlyAttributes().contains(attribute.getId())) continue;
					// remove if category of attribute was not requested
					if (!type.isOperational() && !ldapSettings.isFetchUserAttributes()) shouldSkip = true;
					if (type.isOperational() && !ldapSettings.isFetchOperationAttributes()) shouldSkip = true;
					if (shouldSkip) continue;
				}
				*/

				// FIXME - for debug purpose
				if (!ldapNode.getLdapServer().getSchema().getAttributeTypes().containsKey(attribute.getUpId())) {
					log.info("Attribute not found in own registries: {}", attribute.getUpId());
				}

				for (Value value : attribute) {
					AttributeModelItem item = new AttributeModelItem(ldapNode, attribute, value);
					if (expandedItems.contains(attribute.getId()) || wrapAttributesBy == 0) {
						item.setCollapsed(false);
					} else if (!Objects.equals(attribute.get(), value) && !item.isCellObjectClass() && attribute.size() >= wrapAttributesBy) {
						item.setCollapsed(true);
					}
					if (!item.isCollapsible() && item.isCollapsed()) {
						break;
					} else {
						itemsMap.get(getIndex(item)).add(item);
					}
				}
				// We allow empty attributes in order to enable new entry creation
				if (attribute.size() == 0) {
					AttributeModelItem item = new AttributeModelItem(ldapNode, attribute, null);
					itemsMap.get(getIndex(item)).add(item);
				}

			}
		}

		boolean sortAscending = isSortAscending();
		Comparator<AttributeModelItem> comp = getComparator();
		if (!sortAscending) {
			comp = comp.reversed();
		}
		itemsMap.get(0).sort(comp);
		itemsMap.get(1).sort(comp);
		itemsMap.get(2).sort(comp);
		itemsMap.get(3).sort(comp);

		items.addAll(itemsMap.get(0));
		items.addAll(itemsMap.get(1));
		items.addAll(itemsMap.get(2));
		items.addAll(itemsMap.get(3));

		return items;
	}

	public List<AttributeModelItem> getItems() {
		return items;
	}

	public void clearItems() {
		items = new ArrayList<>();
	}

	@Override
	public int getRowCount() {
		return items.size();
	}

	@Override
	public int getColumnCount() {
		return COLUMN_NAMES.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return COLUMN_NAMES[columnIndex];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return COLUMN_CLASSES[columnIndex];
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {

		if (columnIndex != -1 && COLUMN_EDITABLE[columnIndex]) {
			// only second column cells are editable -> now check item itself
			return isRowEditable(rowIndex);
		}
		return false;

	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// we always retrieve whole AttributeModelItem
		if (rowIndex >= 0) {
			try {
				return items.get(rowIndex);
			} catch (IndexOutOfBoundsException ex) {
				// FIXME - should be error, but IDE bugs users to report it
				log.debug("Probable race-condition between UI and Model, somebody is asking for attribute on non-existing index.", ex);
				return null;
			}
		}
		return null;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		// we do not set values to the model this way - after value change we refresh from LDAP
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		listeners.add(l);
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		listeners.remove(l);
	}

	/**
	 * Determine if row Item (LDAP Attribute) is editable.
	 *
	 * @see JBTable#convertRowIndexToModel(int)
	 *
	 * @param rowIndex index of selected row (must be converted to model index !!)
	 * @return TRUE if Item is editable, FALSE otherwise
	 */
	public boolean isRowEditable(int rowIndex) {

		AttributeModelItem selectedItem = (rowIndex != -1) ? getItems().get(rowIndex) : null;
		if (selectedItem != null) {
			return (!selectedItem.isCellObjectClass() &&
					!selectedItem.isCellPartOfDN() &&
					!selectedItem.isReadOnly() &&
					!selectedItem.isRootNode() &&
					selectedItem.isUserModifiable()
			);
		}

		return false;

	}

	/**
	 * Determine if row Item (LDAP Attribute) is allowed to be removed.
	 *
	 * @see JBTable#convertRowIndexToModel(int)
	 *
	 * @param rowIndex index of selected row (must be converted to model index !!)
	 * @return TRUE if Item is editable, FALSE otherwise
	 */
	public boolean isRowRemovable(int rowIndex) {

		AttributeModelItem selectedItem = (rowIndex != -1) ? getItems().get(rowIndex) : null;
		if (selectedItem != null) {
			return (!selectedItem.isCellObjectClass() &&
					//!selectedItem.isCellUserPassword() && -- user password can be removed
					!selectedItem.isCellPartOfDN() &&
					!selectedItem.isReadOnly() &&
					!selectedItem.isRootNode() &&
					selectedItem.isUserModifiable() &&
					// either its MAY attribute or MUST and has >1 values
					(!selectedItem.isMustAttribute() || (selectedItem.getAttribute().size() > 1))
			);
		}

		return false;

	}

	/**
	 * Determine if row Item (LDAP Attribute) is editable for add action!
	 *
	 * @see JBTable#convertRowIndexToModel(int)
	 *
	 * @param rowIndex index of selected row (must be converted to model index !!)
	 * @return TRUE if Item is editable, FALSE otherwise
	 */
	public boolean isRowAddable(int rowIndex) {

		AttributeModelItem selectedItem = (rowIndex != -1) ? getItems().get(rowIndex) : null;
		if (selectedItem != null) {
			return (!selectedItem.isCellObjectClass() &&
					//!selectedItem.isCellUserPassword() && -- we can add user password
					!selectedItem.isCellPartOfDN() &&
					!selectedItem.isReadOnly() &&
					!selectedItem.isRootNode() &&
					selectedItem.isUserModifiable() &&
					// prevent adding value to single valued attrs if attribute contains it
					!(selectedItem.isSingleValued() &&
							selectedItem.getAttribute().size() >= 1)
			);
		}

		return false;

	}

	public void refresh() {
		items = createItems();
		for (TableModelListener listener : listeners) {
			listener.tableChanged(new TableModelEvent(this));
		}
	}

	public void refresh(int row) {
		items = createItems();
		for (TableModelListener listener : listeners) {
			listener.tableChanged(new TableModelEvent(this, row));
		}
	}

	/**
	 * Return default Comparator for attributes by PluginSettings, this order is used as default == UNSORTED on JBTable.
	 *
	 * @return comparator
	 */
	private Comparator<AttributeModelItem> getComparator() {

		if (isSortByAttributeType()) {
			return Comparator.comparing((AttributeModelItem item)->item.getAttribute().getUpId());
		} else {
			return Comparator.comparing((AttributeModelItem item)->item.getStringValue());
		}

	}

	private int getIndex(AttributeModelItem item) {

		boolean sortMustFirst = isSortMustAttributesFirst();
		boolean sortObjectClassFirst = isSortObjectClassFirst();
		boolean sortOperationalAttributesLast = isSortOperationalAttributesLast();

		if (sortObjectClassFirst && item.isCellObjectClass()) return 0;
		if (sortMustFirst && item.isMustAttribute()) return 1;
		if (sortOperationalAttributesLast && item.isOperationAttribute()) return 3;
		return 2;

	}

	public Set<String> getExpandedItems() {
		return expandedItems;
	}

	public boolean isSortObjectClassFirst() {
		return sortObjectClassFirst;
	}

	public void setSortObjectClassFirst(boolean sortObjectClassFirst) {
		this.sortObjectClassFirst = sortObjectClassFirst;
	}

	public boolean isSortMustAttributesFirst() {
		return sortMustAttributesFirst;
	}

	public void setSortMustAttributesFirst(boolean sortMustAttributesFirst) {
		this.sortMustAttributesFirst = sortMustAttributesFirst;
	}

	public boolean isSortOperationalAttributesLast() {
		return sortOperationalAttributesLast;
	}

	public void setSortOperationalAttributesLast(boolean sortOperationalAttributesLast) {
		this.sortOperationalAttributesLast = sortOperationalAttributesLast;
	}

	public boolean isSortByAttributeType() {
		return sortByAttributeType;
	}

	public void setSortByAttributeType(boolean sortByAttributeType) {
		this.sortByAttributeType = sortByAttributeType;
	}

	public boolean isSortAscending() {
		return sortAscending;
	}

	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	public LdapEntryTreeNode getLdapNode() {
		return ldapNode;
	}
}
