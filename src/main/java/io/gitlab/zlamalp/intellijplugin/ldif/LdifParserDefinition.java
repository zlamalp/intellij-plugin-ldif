package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.lang.*;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.psi.tree.*;
import io.gitlab.zlamalp.intellijplugin.ldif.parser.LdifParser;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.*;
import org.jetbrains.annotations.NotNull;

/**
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifParserDefinition implements ParserDefinition {

	public static final TokenSet WHITE_SPACES = TokenSet.create(TokenType.WHITE_SPACE);
	public static final TokenSet COMMENTS = TokenSet.create(LdifTypes.COMMENT);

	public static final IFileElementType FILE =
			new IFileElementType(Language.<LdifLanguage>findInstance(LdifLanguage.class));

	@NotNull
	@Override
	public Lexer createLexer(Project project) {
		return new LdifLexerAdapter();
	}

	@NotNull
	public TokenSet getWhitespaceTokens() {
		return WHITE_SPACES;
	}

	@NotNull
	public TokenSet getCommentTokens() {
		return COMMENTS;
	}

	@NotNull
	public TokenSet getStringLiteralElements() {
		return TokenSet.EMPTY;
	}

	@NotNull
	public PsiParser createParser(final Project project) {
		return new LdifParser();
	}

	@Override
	public IFileElementType getFileNodeType() {
		return FILE;
	}

	public PsiFile createFile(FileViewProvider viewProvider) {
		return new LdifFile(viewProvider);
	}

	public SpaceRequirements spaceExistanceTypeBetweenTokens(ASTNode left, ASTNode right) {
		return SpaceRequirements.MAY;
	}

	@NotNull
	public PsiElement createElement(ASTNode node) {
		return LdifTypes.Factory.createElement(node);
	}

}
