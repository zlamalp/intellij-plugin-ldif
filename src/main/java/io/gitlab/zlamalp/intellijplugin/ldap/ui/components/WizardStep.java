package io.gitlab.zlamalp.intellijplugin.ldap.ui.components;

import com.intellij.openapi.ui.ValidationInfo;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Representation of single step of WizardDialog. You supposed to provide implementation of JComponent
 * to be displayed and optionally focused.
 *
 * You can initialize step content either in getComponent() or onChangeToStep(), but remember they can be called
 * multiple times. Step can modify WizardModel (data holder) in implementation of abstract methods.
 *
 * TODO: Simplify those event methods, validation
 *
 * @author Pavel Zlámal
 */
public abstract class WizardStep {

	private String name;
	private String description;
	private Icon icon;
	private String helpId;

	public WizardStep() {
	}

	public WizardStep(String name) {
		this.name = name;
	}

	public WizardStep(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public WizardStep(String name, String description, Icon icon) {
		this.name = name;
		this.description = description;
		this.icon = icon;
	}

	public WizardStep(String name, String description, Icon icon, String helpId) {
		this.name = name;
		this.description = description;
		this.icon = icon;
		this.helpId = helpId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Icon getIcon() {
		return icon;
	}

	public void setIcon(Icon icon) {
		this.icon = icon;
	}

	public String getHelpId() {
		return helpId;
	}

	public void setHelpId(String helpId) {
		this.helpId = helpId;
	}

	/**
	 * Returns UI JComponent associated with this WizardStep
	 *
	 * @return wizard step content
	 */
	@NotNull
	public abstract JComponent getComponent();

	/**
	 * Returns UI JComponent which should have focus
	 *
	 * @return focused component
	 */
	@Nullable
	public JComponent getPreferredFocusedComponent() {
		return null;
	}

	/**
	 * Called when this step is set as current step (is displayed within the dialog)
	 *
	 * @param model WizardModel backing WizardDialog data
	 */
	public abstract <T extends WizardModel> void onChangeToStep(T model);

	/**
	 * Called on current step when user clicks Previous button
	 * (just before current step is changed for the previous)
	 *
	 * @param model WizardModel backing WizardDialog data
	 */
	public abstract <T extends WizardModel> void onPrevious(T model);

	/**
	 * Called on current step when user clicks Next button
	 * (just before current step is changed for the next)
	 *
	 * @param model WizardModel backing WizardDialog data
	 */
	public abstract <T extends WizardModel> void onNext(T model);

	/**
	 * Called on current step when user clicks Finish button
	 * (just before WizardDialog is closed with success)
	 *
	 * @param model WizardModel backing WizardDialog data
	 */
	public abstract <T extends WizardModel> void onFinish(T model);

	/**
	 * Called on current step when user clicks Cancel button
	 * (just before WizardDialog is closed with fail)
	 *
	 * @param model WizardModel backing WizardDialog data
	 */
	public abstract <T extends WizardModel> void onCancel(T model);

	public List<ValidationInfo> doValidateAll() {
		return new ArrayList<>();
	}

}
