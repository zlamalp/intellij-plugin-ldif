package io.gitlab.zlamalp.intellijplugin.ldap.settings.connection;

import com.intellij.openapi.Disposable;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.ui.popup.PopupStep;
import com.intellij.openapi.ui.popup.util.BaseListPopupStep;
import com.intellij.openapi.util.registry.Registry;
import com.intellij.ui.ColorChooserService;
import com.intellij.ui.ColorPickerListener;
import com.intellij.ui.DocumentAdapter;
import com.intellij.ui.FileColorManager;
import com.intellij.ui.JBColor;
import com.intellij.ui.ScrollPaneFactory;
import com.intellij.ui.SimpleListCellRenderer;
import com.intellij.ui.components.JBCheckBox;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBPasswordField;
import com.intellij.ui.components.JBTabbedPane;
import com.intellij.ui.components.fields.ExpandableTextField;
import com.intellij.ui.components.fields.ExtendableTextComponent;
import com.intellij.ui.components.fields.ExtendableTextField;
import com.intellij.ui.components.labels.LinkLabel;
import com.intellij.ui.components.labels.LinkListener;
import com.intellij.util.EventDispatcher;
import com.intellij.util.IconUtil;
import com.intellij.util.ui.JBDimension;
import com.intellij.util.ui.JBUI;
import com.intellij.util.ui.StartupUiUtil;
import com.intellij.util.ui.UIUtil;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.components.CircleColorIcon;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.components.ColorNamePair;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Objects;
import java.util.List;

/**
 * Outer layout of settings form for each LdapServer connection.
 *
 * @see ManageLdapServersDialog (dialog for managing all conections)
 * @see LdapServerConfigForm (form with basic settings)
 * @see LdapServerConfigSettingsForm (form with advanced settings)
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapServerConfigTabbedPane {

	private JPanel wrapper;
	private ExtendableTextField nameField;
	private JBCheckBox isGlobalField;
	private JComboBox<String> ldapType;
	private ExpandableTextField commentField;
	private LinkLabel<LdapServer> resetLabel;
	JBTabbedPane tabbedPane;
	private JBLabel commentLabel;
	private ColorNamePair ldapColor = new ColorNamePair("No color", JBColor.WHITE);

	private LdapServer ldapServer;
	private Project project;
	private LdapServerConfigForm connectionForm;
	private LdapServerConfigSettingsForm connectionSettings;

	private final EventDispatcher<ChangeListener> myDispatcher = EventDispatcher.create(ChangeListener.class);
	private boolean loaded = false;
	private boolean isNew = false;
	private boolean isValid = true;
	private Disposable disposable;

	public LdapServerConfigTabbedPane(@NotNull Project project, @NotNull final LdapServer ldapServer, @NotNull Disposable disposable) {
		this.disposable = disposable;
		this.ldapServer = ldapServer;
		this.project = project;
		this.connectionForm = new LdapServerConfigForm(this);
		this.connectionSettings = new LdapServerConfigSettingsForm(this);

		// fake left border
		tabbedPane.setTabComponentInsets(JBUI.emptyInsets());
		tabbedPane.setMinimumSize(new JBDimension(550, 500));
		tabbedPane.setPreferredSize(new JBDimension(550, 500));
		tabbedPane.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				// to notify upper dialog about selected tab index
				setModified();
			}
		});

		commentLabel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));

		wrapper.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
		wrapper.setMinimumSize(new JBDimension(550, 500));
		wrapper.setPreferredSize(new JBDimension(550, 500));

		for (LdapServer.LdapType type : LdapServer.LdapType.values()) {
			ldapType.addItem(type.getName());
		}
		ldapType.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (loaded) setModified();
			}
		});
		ldapType.setRenderer(new SimpleListCellRenderer<String>() {
			@Override
			public void customize(@NotNull JList<? extends String> list, String value, int index, boolean selected, boolean hasFocus) {
				if (value.equals("ActiveDirectory")) {
					setText("Active Directory");
					setIcon(LdapIcons.LDAP_MSAD);
				} else if (value.equals("ApacheDirectory")) {
					setText("Apache Directory");
					setIcon(LdapIcons.LDAP_APACHE);
				} else if (value.equals("OpenLDAP")) {
					setText("Open LDAP");
					setIcon(LdapIcons.LDAP);
				} else {
					setText(value);
					setIcon(LdapIcons.LDAP);
				}
			}
		});

		addTab("General", connectionForm.getContent());
		addTab("Options", connectionSettings.getContent());

		// WE MUST HIDE THIS, its managed by moving LDAP server connection between project/global lists !!
		// we keep it like this in order to use "changed" resolution logic
		isGlobalField.setVisible(false);
		isGlobalField.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (loaded) setModified();
			}
		});

		addTextChangedDocumentAdapter(nameField);
		addTextChangedDocumentAdapter(commentField);

		resetLabel.setVisible(false);
		resetLabel.setIcon(null);
		resetLabel.setBorder(BorderFactory.createEmptyBorder(0,0,0,10));
		resetLabel.setListener(new LinkListener<LdapServer>() {
			@Override
			public void linkSelected(LinkLabel linkLabel, LdapServer o) {
				loadConnection(ldapServer);
				if (loaded) setModified();
			}
		}, ldapServer);

		// listener for custom color selection
		List<ColorPickerListener> listenerList = new ArrayList<>();
		listenerList.add(new ColorPickerListener() {
			@Override
			public void colorChanged(Color color) {
				ldapColor = new ColorNamePair("Custom..." , new JBColor(color, color));
				if (loaded) setModified();
				nameField.updateUI();
			}
			@Override
			public void closed(@Nullable Color color) {

			}
		});

		// clickable color selection in name input field
		nameField.setExtensions(new ExtendableTextComponent.Extension() {
			@Override
			public Icon getIcon(boolean hovered) {
				if (hovered) {
					return IconUtil.colorize(new CircleColorIcon(16, ldapColor.getJBColor(), true), JBColor.GRAY);
				}
				return new CircleColorIcon(16, ldapColor.getJBColor(), true);
			}
			@Override
			public Runnable getActionOnClick() {
				return new Runnable() {
					@Override
					public void run() {

						// get existing file colors
						FileColorManager colorManager = FileColorManager.getInstance(project);
						List<ColorNamePair> colors = new ArrayList<>();
						colors.add(new ColorNamePair("No color", null));
						for (String colorName : colorManager.getColorNames()) {
							colors.add(new ColorNamePair(colorName, (JBColor)colorManager.getColor(colorName)));
						}
						colors.add(new ColorNamePair("Custom...", null));

						// create popup selection
						BaseListPopupStep<ColorNamePair> popupStep = new BaseListPopupStep<ColorNamePair>(null, colors){
							@Nullable
							@Override
							public PopupStep<ColorNamePair> onChosen(ColorNamePair selectedValue, boolean finalChoice) {
								if (selectedValue.getName().equals("Custom...")) {
									EventQueue.invokeLater(new Runnable() {
										@Override
										public void run() {
											ColorChooserService.getInstance().showDialog(project, LdapServerConfigTabbedPane.this.getContent(),  "Select Connection Color", ldapColor.getJBColor(), false , listenerList, false);
										}
									});
									return null;
								} else {
									ldapColor = selectedValue;
								}
								if (loaded) setModified();
								nameField.updateUI();
								return null;
							}

							@Override
							public Icon getIconFor(ColorNamePair value) {
								return value.getIcon();
							}

							@NotNull
							@Override
							public String getTextFor(ColorNamePair value) {
								// fix default color name for dark theme
								return StartupUiUtil.isUnderDarcula() && value.getName().equals("Yellow") ? "Brown" : value.getName();
							}

						};
						// FIXME - can we find better way to determine popup location ?
						Point location = MouseInfo.getPointerInfo().getLocation();
						location.setLocation(location.x-10, location.y+10);
						JBPopupFactory.getInstance().createListPopup(popupStep).showInScreenCoordinates(nameField, location);

					}
				};
			}
		});

	}

	/**
	 * Load LdapServerConnection details into form
	 *
	 * @param ldapServer Connection to load
	 */
	public void loadConnection(LdapServer ldapServer) {

		this.loaded = false;

		nameField.setText(ldapServer.getName());
		isGlobalField.setSelected(ldapServer.isGlobal());
		ldapType.setSelectedItem(ldapServer.getLdapType().getName());
		commentField.setText(ldapServer.getDescription());
		ldapColor = ldapServer.getColor();
		nameField.updateUI();

		// load global pane
		connectionForm.loadConnection(ldapServer);
		connectionSettings.loadConnection(ldapServer);

		this.loaded = true;

	}

	/**
	 * Save details from this form into LdapServerConnection instance
	 *
	 * @param ldapServer Connection to save
	 */
	public void saveConnection(LdapServer ldapServer) {

		ldapServer.setName(nameField.getText());
		ldapServer.setLdapType(LdapServer.LdapType.getLdapType((String)ldapType.getSelectedItem()));
		ldapServer.setDescription(getValueOrNull(commentField));
		ldapServer.setGlobal(isGlobalField.isSelected());
		ldapServer.setColor(ldapColor);
		// store global pane
		connectionForm.saveConnection(ldapServer);
		connectionSettings.saveConnection(ldapServer);

	}

	private String getValueOrNull(@NotNull JTextField textField) {
		String val = null;
		if (textField instanceof JBPasswordField) {
			if (((JBPasswordField) textField).getPassword().length != 0) {
				val = new String(((JBPasswordField) textField).getPassword());
			}
		} else {
			val = (textField.getText().trim().isEmpty()) ? null : textField.getText();
		}
		return val;
	}

	/**
	 * Return TRUE if form is modified against original LdapServer
	 *
	 * @param ldapServer Original LdapServer to check against
	 * @return TRUE = modified / FALSE = same
	 */
	public boolean isModified(LdapServer ldapServer) {

		if (!loaded) return false;
		if (isNew) return true;
		if (!Objects.equals(ldapServer.getName(), nameField.getText())) return true;
		if (!Objects.equals(ldapServer.getLdapType().getName(), (String)ldapType.getSelectedItem())) return true;
		if (!Objects.equals(ldapServer.getDescription(), getValueOrNull(commentField))) return true;
		if (!Objects.equals(ldapServer.isGlobal(), isGlobalField.isSelected())) return true;
		if (!Objects.equals(ldapServer.getColor(), ldapColor)) return true;

		if (connectionForm.isModified(ldapServer)) return true;
		if (connectionSettings.isNew() || connectionSettings.isModified(ldapServer.getSettings())) return true;

		return false;

	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean aNew) {
		isNew = aNew;
		connectionForm.setNew(aNew);
		connectionSettings.setNew(aNew);
	}

	public void setModified() {
		fireStateChanged();
	}

	public void addChangeListener(ChangeListener listener) {
		myDispatcher.addListener(listener);
		connectionForm.addChangeListener(listener);
		connectionSettings.addChangeListener(listener);
	}

	public void removeChangeListener(ChangeListener listener) {
		myDispatcher.removeListener(listener);
		connectionForm.removeChangeListener(listener);
		connectionSettings.removeChangeListener(listener);
	}

	public void fireStateChanged() {
		myDispatcher.getMulticaster().stateChanged(new ChangeEvent(this));
	}

	public String getDisplayName() {
		return getValueOrNull(nameField);
	}

	/**
	 * Returns Component representing form
	 *
	 * @return form component
	 */
	public JComponent getContent() {
		return wrapper;
	}

	public LdapServer getLdapServer() {
		return ldapServer;
	}

	/**
	 * Creates text changed listener for form field expecting string value
	 *
	 * @param textField text field to attach listener
	 */
	private void addTextChangedDocumentAdapter(@NotNull JTextField textField) {
		textField.getDocument().addDocumentListener(new DocumentAdapter() {
			@Override
			protected void textChanged(DocumentEvent documentEvent) {
				connectionForm.clearConnectedLabel();
				if (loaded) setModified();
			}
		});
	}

	/**
	 * Switch "Reset" link visibility.
	 * If ldapServer is "new" (previously unsaved), link is always hidden.
	 *
	 * @param show TRUE to show the link
	 */
	public void showReset(boolean show) {
		// respect NEW state -> making it false
		this.resetLabel.setVisible(!isNew && show);
	}

	/**
	 * Trigger decision, if "Reset to IDE default" should be visible or not
	 */
	public void setShowResetToIDEdefaults() {
		if (loaded) {
			this.connectionSettings.setShowResetToIDEdefaults();
		}
	}

	/**
	 * Add TAB to the tabbed pane wrapping it in scroller
	 *
	 * @param name Tab name displayed in the tab switcher
	 * @param component Inner tab component
	 */
	private void addTab(@NotNull String name, @NotNull Component component) {

		JScrollPane myScroller = ScrollPaneFactory.createScrollPane(component, true);
		myScroller.setViewportBorder(BorderFactory.createEmptyBorder(10, 10, 10, 15));
		myScroller.setWheelScrollingEnabled(true);
		// disable horizontal scrollbar to wrap panel content !!
		myScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		if (!Registry.is("ide.scroll.background.auto")) {
			myScroller.setBackground(UIUtil.SIDE_PANEL_BACKGROUND);
			myScroller.getViewport().setBackground(UIUtil.SIDE_PANEL_BACKGROUND);
			myScroller.getVerticalScrollBar().setBackground(UIUtil.SIDE_PANEL_BACKGROUND);
		}
		myScroller.setPreferredSize(new JBDimension(550, 500));
		myScroller.setMinimumSize(new JBDimension(550,500));
		tabbedPane.addTab(name, myScroller);

	}

	/**
	 * Select active "tab" by index
	 *
	 * @param index tab index
	 */
	public void setSelectedSubTabIndex(int index) {
		tabbedPane.setSelectedIndex(index);
	}

	@NotNull
	public Project getProject() {
		return project;
	}

	@NotNull
	public Disposable getDisposable() {
		return this.disposable;
	}

	/**
	 * Updates "global" flag in LdapServer from outside (when user moves LdapServer between lists)
	 *
	 * @param global TRUE = IDE wide / FALSE = Project wide
	 */
	public void setIsGlobal(boolean global) {
		isGlobalField.setSelected(global);
	}

	/**
	 * Validate and return all validation results for this form.
	 *
	 * @return List of validation results
	 */
	public List<ValidationInfo> doValidateAll() {

		List<ValidationInfo> result = new ArrayList<>();
		result.addAll(connectionForm.doValidateAll());
		result.addAll(connectionSettings.doValidateAll());
		updateValid(result);
		return result;

	}

	private void updateValid(List<ValidationInfo> validationInfo) {

		for (ValidationInfo info : validationInfo) {
			if (!info.okEnabled) {
				this.isValid = false;
				return;
			}
		}
		this.isValid = true;

	}

	public boolean isValid() {
		return this.isValid;
	}

}
