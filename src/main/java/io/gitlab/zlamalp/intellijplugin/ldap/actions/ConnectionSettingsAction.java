package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.project.DumbAware;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapServerTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.connection.ManageLdapServersDialog;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

/**
 * Open dialog window with LdapServerConnection settings. If some connection is selected in LdapTreePanel,
 * then it's pre-selected in a dialog window.
 *
 * @see LdapTreePanel
 * @see LdapServer
 * @see ManageLdapServersDialog
 * @see io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapPluginSettingsService
 * @see LdapServerTreeNode
 * @see LdapEntryTreeNode
 *
 * @author Attila Majoros
 * @author Pavel Zlámal
 */
public class ConnectionSettingsAction extends AnAction implements DumbAware {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.manageConnections";
	private static Logger log = LoggerFactory.getLogger(ConnectionSettingsAction.class);

	public ConnectionSettingsAction() {
	}

	public ConnectionSettingsAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		if (LdapActionUtils.isFromTree(e)) {

			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapTreeNode.class, null);
			if (selectedTreeNodes.length > 0) {
				new ManageLdapServersDialog(e.getProject(), treePanel, selectedTreeNodes[0].getLdapServer()).show();
			} else {
				new ManageLdapServersDialog(e.getProject(), treePanel, null).show();
			}

		} else if (LdapActionUtils.isFromEditor(e)) {

			FileEditor fileEditor = PlatformDataKeys.FILE_EDITOR.getData(e.getDataContext());
			if (fileEditor instanceof LdapEntryEditor) {
				if (((LdapEntryEditor)fileEditor).getLdapNode().getLdapServer() != null) {
					new ManageLdapServersDialog(e.getProject(), fileEditor.getComponent(), ((LdapEntryEditor)fileEditor).getLdapNode().getLdapServer()).show();
				}
			}

		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if ConnectionSettings action should be active.
	 *
	 * @param e triggered event
	 * @return TRUE if action should be active
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromTree(e)) {
			return true;
		} else if (LdapActionUtils.isFromEditor(e)) {
			// FIXME / TODO - add condition for !editor.isLoading();
			return (PlatformDataKeys.FILE_EDITOR.getData(e.getDataContext()) instanceof LdapEntryEditor);
		}
		return false;

	}

}
