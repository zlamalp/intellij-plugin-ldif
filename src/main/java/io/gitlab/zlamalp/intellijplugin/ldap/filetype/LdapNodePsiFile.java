package io.gitlab.zlamalp.intellijplugin.ldap.filetype;

import com.intellij.ide.navigationToolbar.NavBarPanel;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiFile;
import com.intellij.psi.impl.PsiManagerImpl;
import com.intellij.psi.impl.file.PsiBinaryFileImpl;
import com.intellij.ui.treeStructure.Tree;
import com.intellij.util.ui.tree.TreeUtil;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapServerTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeRootNode;
import org.jetbrains.annotations.NotNull;

import static io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel.LDAP_TOO_WINDOW_NAME;

/**
 * Implementation of {@link PsiFile} for our {@link LdapNodeVirtualFile}.
 * They are connect to each other by the means of {@link LdapNodeFileViewProvider} component.
 *
 * Sole purpose of it is to override {@link #navigate(boolean)} method to support custom navigation
 * triggered from {@link NavBarPanel}, which is PSI centric and by default opens Project tool window
 * and searches for file system / project content.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapNodePsiFile extends PsiBinaryFileImpl {

	public LdapNodePsiFile(@NotNull PsiManagerImpl manager, @NotNull FileViewProvider viewProvider) {
		super(manager, viewProvider);
	}

	@Override
	public void navigate(boolean requestFocus) {

		VirtualFile virtualFile = getVirtualFile();
		if (virtualFile instanceof LdapNodeVirtualFile) {

			LdapTreeNode node = ((LdapNodeVirtualFile)virtualFile).getLdapTreeNode();

			if (node instanceof LdapEntryTreeNode) {
				FileEditorManager.getInstance(getProject()).openFile(virtualFile, requestFocus, true);
				ToolWindowManager.getInstance(getProject()).getToolWindow(LDAP_TOO_WINDOW_NAME).show(new Runnable() {
					@Override
					public void run() {
						Tree tree = LdapTreePanel.getInstance(getProject()).getTree();
						if (tree != null) {
							TreeUtil.selectNode(tree, node);
						}
					}
				});
			} else if (node instanceof LdapServerTreeNode) {
				// FIXME - probably not needed, we consider server node to be a directory
				ToolWindowManager.getInstance(getProject()).getToolWindow(LDAP_TOO_WINDOW_NAME).show(new Runnable() {
					@Override
					public void run() {
						Tree tree = LdapTreePanel.getInstance(getProject()).getTree();
						if (tree != null) {
							TreeUtil.selectNode(tree, node);
						}
					}
				});
			} else if (node instanceof LdapTreeRootNode) {
				// FIXME - probably not needed, we consider tree root to be a directory
				ToolWindowManager.getInstance(getProject()).getToolWindow(LDAP_TOO_WINDOW_NAME).show(new Runnable() {
					@Override
					public void run() {

					}
				});
			}
		}
	}

	@Override
	public boolean canNavigate() {
		return true;
	}

	@Override
	public boolean canNavigateToSource() {
		return true;
	}

}
