package io.gitlab.zlamalp.intellijplugin.ldap.tree.node;

import io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * TreeNode representing Custom Search Result. Its expected to be below LdapTreeNodeSearch.
 * Children are expected to be standard LdapTreeNodes.
 *
 * @see LdapSearchTreeNode
 * @see LdapEntryTreeNode
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapSearchResultTreeNode extends DefaultMutableTreeNode implements LdapTreeNode, Comparable<LdapSearchResultTreeNode> {

	private static Logger log = LoggerFactory.getLogger(LdapSearchResultTreeNode.class);

	// LDAP server ldapServer associated with this node
	private LdapServer ldapServer;

	private String base;
	private String filter;
	private SearchScope scope;
	private Set<String> requestedAttributes;
	private String uuid;

	/**
	 * Create new TreeNode for LdapTreePanel matching single LDAP Entry.
	 *
	 * @param ldapServer LDAP server ldapServer
	 */
	public LdapSearchResultTreeNode(@NotNull LdapServer ldapServer, String uuid, String customSearchName, String base, String filter, SearchScope scope, Set<String> requestedAttributes) {

		super(customSearchName);
		this.uuid = uuid;
		this.ldapServer = ldapServer;

		this.base = base;
		this.filter = filter;
		this.scope = scope;
		this.requestedAttributes = requestedAttributes;

	}

	public String getUuid() {
		return uuid;
	}

	public String getName() {
		return toString();
	}

	public String getBase() {
		return base;
	}

	public String getFilter() {
		return filter;
	}

	public SearchScope getScope() {
		return scope;
	}

	public Set<String> getRequestedAttributes() {
		return requestedAttributes;
	}

	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	/**
	 * Get all child LdapTreeNodes for this parent (LdapTreeNodeSearchResult) or empty list if no child is present.
	 * Value is taken from local tree structure, no actual ldap search is performed.
	 *
	 * If child nodes are not of type LdapTreeNode, they are ignored. This wrapper node is expected to
	 * contain only them !!
	 *
	 * @return list of child LdapTreeNodes
	 */
	public @NotNull List<LdapEntryTreeNode> getChildren() {
		List<LdapEntryTreeNode> children = new ArrayList<>();
		for (int i=0; i<getChildCount(); i++) {
			TreeNode node = getChildAt(i);
			if (node instanceof LdapEntryTreeNode) {
				children.add(((LdapEntryTreeNode) node));
			}
		}
		return children;
	}

	/**
	 * Get LdapServerConnection related to this TreeNode
	 *
	 * @return LdapServerConnection related to this TreeNode
	 */
	public @NotNull
	LdapServer getLdapServer() {
		return ldapServer;
	}

	@NotNull
	@Override
	public String getTreeNodePresentation() {
		return toString();
	}

	@Nullable
	public Icon getIcon() {
		return LdapIcons.SEARCH;
	}

	@Nullable
	@Override
	public LdapNodeVirtualFile getVirtualFile() {
		return null;
	}

	@Override
	public String toString() {
		return getUserObject().toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		LdapSearchResultTreeNode that = (LdapSearchResultTreeNode) o;
		return Objects.equals(ldapServer, that.ldapServer) && Objects.equals(getUuid(), that.getUuid());
	}

	@Override
	public int hashCode() {
		return Objects.hash(ldapServer, uuid);
	}

	@Override
	public int compareTo(@NotNull LdapSearchResultTreeNode o) {

		if (toString() == null && o.toString() != null) return -1;
		else if (o.toString() == null && toString() != null) return 1;
		else if (toString() == null && o.toString() == null) return 0;
		else return toString().compareToIgnoreCase(o.toString());

	}

}
